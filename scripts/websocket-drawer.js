
window.onbeforeunload = closingCode;
function closingCode(){
   // do something...
   drawer.closeTheWebsocket();
   drawer = null;
   return null;
}

function Drawer(chatID, callback) {
  var socket_url = 'websocket.network';//'abdulmirza2.ddns.net'
  var port = '443';//'8086';
   let webSocket;

   this.join = function(){
     openSocket();
     return false;
   }

   this.closeTheWebsocket = function(){
    // alert("closing");
     webSocket.close();
     return false;
   }

   function openSocket() {
     if (webSocket !== undefined && webSocket.readyState !== WebSocket.CLOSED) {
         return;
     }

     webSocket = new WebSocket("wss://" + socket_url + ":" + port
         + "/StudyChatDrawer/draw?id=" + chatID.toString());

     webSocket.onopen = function (event) {
       callback(this);
     };

     webSocket.onmessage = function (event) {
         parse(event.data);
     };

     webSocket.onclose = function (event) {
     };
   }

   this.send = function (x,y,action) {
      sendLineToServer(x,y,action);
   }

   this.sendImage = function (image) {
      sendImageToServer("addImage",image);
   }

   function parse(message) {
       let jObj = $.parseJSON(message);
       if('x' in jObj) {
          paintBrush.drawFromServer(jObj.x, jObj.y, jObj.action);
       } else if('image' in jObj) {
          paintBrush.drawFromServer(0, 0, jObj.action, jObj.image);
       }
   }

   function sendLineToServer(x,y,action) {
     var json = '{""}';
     var myObject = new Object();
     myObject.x = x;
     myObject.y = y;
     myObject.action = action;
     json = JSON.stringify(myObject);

    // console.log("JSN",json);

     webSocket.send(json);
   }

   function sendImageToServer(action,image) {
     var json = '{""}';
     var myObject = new Object();
     myObject.action = action;
     myObject.image = image;
     json = JSON.stringify(myObject);

     webSocket.send(json);
   }

   function closeSocket() {
      webSocket.close();
   }
}
