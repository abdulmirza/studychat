var class_id = null;
var url = null;

Dropzone.autoDiscover = false;

$(document).on({
    ajaxStart: function() {$(".loader").show();},
     ajaxStop: function() {$(".loader").hide();}
});

$(document).ready(function () {
  const urlParams = new URLSearchParams(window.location.search);
  console.log(urlParams.get('CLASS_ID'));
  if(urlParams.get('CLASS_ID') != null) {

    class_id = parseInt(urlParams.get('CLASS_ID'));
    if(class_id != null) {
      getClassesFiles(class_id);
      getSchoolId(class_id).then(function(data) {
        console.log(data);
        data = JSON.parse(data);

        if(data["RESULT"] == 1) {
          document.cookie = "SCHOOL_ID = "+data["MESSAGE"]+";";
          console.log("IN");
          getClasses(data["MESSAGE"]);
        }

      });
    }
  } else if(getCookie("SCHOOL_ID") != "") {
    let id = getCookie("SCHOOL_ID");
    getClasses(id);
    $("#share_link").val("https://studychat.online/filesView.php?CLASS_ID="+class_id);
  } else {
    $("#selectSchoolModal").modal("toggle");

     $('#selectSchoolModal').on('hidden.bs.modal', function (e) {
      // $("#joinChatModal").modal("toggle");
     });
  }


 $("#classSelector")
 .dropdown({ maxSelections: 10});

 $('.schoolSelect')
   .dropdown({
     action: 'hide',
     onChange: function(value, text, $selectedItem) {
       document.cookie = "SCHOOL_ID="+value+"; expires=Thu, 18 Dec 2022 12:00:00 UTC; path=/";
       $('.schoolSelect').dropdown('set selected', value);
       // $('#selected_school').text(text);
       $("#alert_select_school").alert('close');
       getClasses(value);
       $("#settingsModal").modal("hide");
    }
  });

  $("#class_container").on("click",".class_card", function () {
    class_id = $(this).data("class-id");
    getClassesFiles(class_id);
    $("#class_name").text($(this).find("p").text());
        window.history.pushState("n/a", "Title", "/filesView.php?CLASS_ID="+class_id);
  });

  var myAwesomeDropzone = new Dropzone("form#my-awesome-dropzone", {
     autoProcessQueue: false,
     uploadMultiple: false,
     parallelUploads: 1,
     maxFiles: 100,

     init: function () {
         var myDropzone = this;

         this.element.querySelector('button[type=submit]').addEventListener("click", function (e) {
           console.log(class_id);
           e.preventDefault();
           e.stopPropagation();

           if(class_id != null) {

             myDropzone.processQueue();
             $("#uploadModal .alert").hide();
           } else {
             alert("Please connect to a class.");
           }
         });
          this.on("sending", function(file, xhr, formData){
              formData.append("CLASS_ID", class_id);
          });
          this.on("success", function(file, responseText) {
             console.log(responseText);
             let result = JSON.parse(responseText);
             url = result["URL"];
             $("#uploadModal .alert-success").show();
             $("#append_file").addClass("pulse");
             $( "#message_footer .input_text" ).attr("placeholder","Sending file and message...")
          });
         this.on("sendingmultiple", function () {
         });
         this.on("successmultiple", function (files, response) {
         });
         this.on("errormultiple", function (files, response) {
           $("#uploadModal .alert-danger").show();
         });
     }
  });

});

function getSchoolId(class_id)  {
  return $.get("php/website/getSchoolFromClassId.php", {CLASS_ID : class_id})
  .done(function(data) {
  });
}

function getClasses(schoolId)  {
 console.log("SCHOOL_ID", schoolId);
  return $.get("php/website/getClasses.php", {SCHOOL_ID : schoolId, ACCID: -1})
  .done(function(data) {
      console.log(data);
      data = JSON.parse(data);
      let p = $("#class_container");

      if(data.length > 0) {
        p.empty();
              //     p.append('<option value="-1">Class</option>');
        $.each(data, function( i, v ) {
          console.log(v.CLASS_ID, class_id);
           let section = "";
           if(v.CLASS_SECTION != null) {
             section = v.CLASS_SECTION;
           }
           if(v.CLASS_ID == class_id) {
             $("#class_name").text(v.CLASS_NAME);
           }
            let str = '<div class="row class_card" data-class-id="'+v.CLASS_ID+'">\
                  <div class="col-12">\
                    <p class="class_name">\
                      '+v.CLASS_NAME+'\
                      - '+section+'\
                    </p>\
                  </div>\
                </div>';//'<option value="'+v.CLASS_ID+'">'+v.CLASS_NAME + ' ' + section +'</option>';
            p.append(str);
        });
      } else {
        $("#no_classes_for_school").show();
      }
  });
}

function getClassesFiles(class_id){
  $.get("php/website/getClassesFiles.php", {CLASS_ID : class_id})
  .done(function(data) {
      console.log(data);
      data = JSON.parse(data);
      console.log(data);
      $("#share_link").val("https://studychat.online/filesView.php?CLASS_ID="+class_id);
      let p = $("#files_container");
      p.empty();

      if(data.length > 0) {
        $("#no_files").hide();
      $.each(data, function( i, v ) {
         let str = '<div class="col-12 col-sm-12 col-md-4 col-lg-4 padding">\
                    <a href="'+v.PATH+'" download class="url">\
                     <div class="file_container">\
                       <div class="row">\
                         <div class="col-3">\
                           <img src="css/img/folder.svg" alt="folder" class="file_image">\
                         </div>\
                         <div class="col-9 padding_left">\
                           <p class="file_name">\
                             '+v.NAME+'\
                           </p>\
                           <span class="file_uploader"> '+v.UPLOADER+'</span>\
                         </div>\
                       </div>\
                       <div class="row margin_top_5">\
                         <div class="col">\
                           <p class="file_time_stamp">\
                             '+v.DATE+'\
                           </p>\
                         </div>\
                       </div>\
                       <div class="row">\
                         <div class="col">\
                               <span class="file_size">'+Math.ceil((v.SIZE/1000)/1000)+'MB</span>\
                         </div>\
                       </div>\
                     </div>\
                   </a></div>';
          p.append(str);
      });
    } else {
        $("#no_files").show();
    }
  });
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1);
          if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
  }
  return "";
}

function mobileCheck() {
    var check = false;
    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
}
