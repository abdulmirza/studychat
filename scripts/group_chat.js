//var distance = 0;
var chatArray = [];
var num = 0;
//first = true;
 function ChatG(ad_id, ulMessage,inputMessage) {
                this.adG= parseInt(ad_id);
                var ad = this.adG;
                var lMessage = ulMessage;
                var inpMessage = inputMessage;
                // to keep the session id
                var sessionId = '';

                // name of the client
                var name = '';
                var id = 0;
                // socket connection url and port
                var socket_url = 'localhost';
                var port = '8083';

                var webSocket;

                /**
                * Connecting to socket
                */
                this.join = function () {
                    openSocket();
                    return false;
                }

                /**
                * Will open the socket connection
                */

                function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
            }
            return "";
        }

     function openSocket() {
                    // Ensures only one connection is open at a time
                    if (webSocket !== undefined && webSocket.readyState !== WebSocket.CLOSED) {
                        return;
                    }
                     var myCookie = getCookie("user");
                     var chat_id = "";
                    // if (myCookie != "") {
                         var split = myCookie.split("%40");
                         chat_id = split[0] + ad.toString();
                    webSocket = new WebSocket("ws://" + socket_url + ":" + port
                        + "/GC/groupchat?name=yo" + "&id=" + ad.toString());

                    /**
                    * Binds functions to the listeners for the websocket.
                    */
                    webSocket.onopen = function (event) {
                    };

                    webSocket.onmessage = function (event) {
                        // parsing the json data
                        parseMessage(event.data);
                    };

                    webSocket.onclose = function (event) {
                        alert('Error! Connection is closed. Try connecting again.');
                    };
                }

                /**
                * Sending the chat message to server
                */
                this.send = function (mes) {
                    var message = mes;
                    if (message.trim().length > 0) {
                        var from_name = 'Buyer';

                        var li = '<li><span class="name">' + from_name + '</span> '
                             + message + '</li>';
                        sendMessageToServer('message', message);
                        return li;
                    } else {
                        alert('Please enter message to send!');

                    }

                }

                /**
                * Closing the socket connection
                */
                function closeSocket() {
                     webSocket.close();
                }

                /**
                * Parsing the json message. The type of message is identified by 'flag' node
                * value flag can be self, new, message, exit
                */
                function parseMessage(message) {
                    var jObj = $.parseJSON(message);

                    // if the flag is 'self' message contains the session id
                    if (jObj.flag == 'self') {

                        sessionId = jObj.sessionId;

                    } else if (jObj.flag == 'new') {
                        // if the flag is 'new', a client joined the chat room
                        var new_name = 'Buyer';

                        // number of people online
                        var online_count = jObj.onlineCount;

                        if (jObj.sessionId != sessionId) {
                            new_name = "Seller";
                        }

                        var li = '<li class="new"><span class="name">' + new_name + '</span> '
                            + jObj.message + '</li>';
                    } else if (jObj.flag == 'message') {
                        // if the json flag is 'message', it means somebody sent the chat
                        // message
                        var from_name = 'Seller';

                        if (jObj.sessionId != sessionId) {
                            from_name = jObj.name;
                        }
                          appendToUl(jObj.message, from_name);


                    } else if (jObj.flag == 'exit') {
                        // if the json flag is 'exit', it means somebody left the chat room
                        var li = '<li class="exit"><span class="name red">' + jObj.name
                            + '</span> ' + jObj.message + '</li>';
                        var online_count = jObj.onlineCount;
                        appendChatMessage(li);
                    }
                }
                /**
                * Sending message to socket server message will be in json format
                */
                function sendMessageToServer(flag, message) {
                    var json = '{""}';

                    // preparing json object
                    var myObject = new Object();
                    myObject.sessionId = sessionId;
                    myObject.message = message;
                    myObject.flag = flag;
                    json = JSON.stringify(myObject);

                    // sending message to server
                    webSocket.send(json);
                }

                function appendToUl(message,name){
                    var ul = document.getElementById(lMessage);
                    var li = document.createElement("li");
                    var span = document.createElement("span");
                    var p = document.createElement("p");
                    span.appendChild(document.createTextNode(name));
                     p.appendChild(document.createTextNode(message));
                    li.appendChild(span);
                    li.appendChild(p);
                    ul.appendChild(li);
                    ul.scrollTop = ul.scrollHeight;
                }
            }

            function getCookie(cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1);
                        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
                }
                return "";
            }

            function keyExists(id){
                // alert("in");
                for (var x = 0;x < chatArray.length;x++){
                    // alert(id + chatArray[x].adG);
                    if(chatArray[x]["adG"] == id){
                      //  alert("in 3");
                        return true;
                    }
                }
                return false;
            }
            $(document).ready(function () {

                $(".dropdown").on("click", ".dropdown-content a", function (e) {
                    var myCookie = getCookie("user");

                    if (myCookie != "") {
                        if (num < 7) {
                            var title = $(this).parent().find('a').text().toLowerCase().substring(0, 35);
                            var ad_id = $(this).parent().find('input[type=hidden]').val();
                             alert("AD_ID: " + ad_id + " Title: " + title);
                            if (!keyExists(ad_id)) {
                                //$("#chatees").append("<a><div class='widget'><p>" + title + "</p><input type='hidden' value='" + ad_id + "'/></div></a>");
                                var mess = 'messages' + ad_id;
                                var inmess = 'input_message' + ad_id;
                                var chat = new ChatG(ad_id, mess, inmess);
                                //   alert(mess + " : " + inmess);
                                chatArray.push(chat);
                                chat.join();
                                $("#chats").append(" <div class='chat-box'><input type='checkbox' />  " +
                                                        "<label data-expanded='Close " + title + "' data-collapsed='Open " + title + "'></label>" +
                                                        "<div class='chat-box-content'>" +
                                                            "<ul class='ul' id='messages" + ad_id + "'></ul>" +
                                                            "<div id='input_message_container'>" +
                                                                "<input type='text' name='message' id='input_message" + ad_id + "' placeholder='Type your message' />" +
                                                                "<button id='btn_send' class=' btn btn-info'>Send</button><input type='hidden' value='" + num + "'>" +
                                                                "<div class='clear'></div>" +
                                                              "</div></div>");
                                num++;
                            } else {
                                alert("You have already started a chat for this book.");
                            }
                        } else {
                            alert("Unfortunately, You already have 7 chats open on this page. Please see the FAQ for more information");
                        }

                    } else {
                        alert("Please Sign In before chatting");
                    }
                });

                $("#chats").on("click", ".chat-box button", function (e) {
                    var ul = "#" + $(this).parent().parent().find("ul").attr("id");
                    var inputValue = $(this).parent().find("input[type=text]").val();
                    var num = $(this).parent().find("input[type=hidden]").val();
                    var li = chatArray[num].send(inputValue);

                    $(ul).append(li);
                    $(ul).scrollTop($(ul).prop("scrollHeight"));
                    $(this).parent().parent().find("input[type=text]").val("");
                });
            });
