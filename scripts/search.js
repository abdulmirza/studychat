

$(document).ready(function () {

    $('#search').keyup(function () {
        var filter_array = new Array();
        var filter = this.value.toLowerCase();  // no need to call jQuery here

        filter_array = filter.split(' '); // split the user input at the spaces

        var arrayLength = filter_array.length; // Get the length of the filter array

        $('.ad').each(function () {
            /* cache a reference to the current .media (you're using it twice) */
            var _this = $(this);
            var title = _this.find('h4').text().toLowerCase();

            /* 
            title and filter are normalized in lowerCase letters
            for a case insensitive search
            */

            var hidden = 0; // Set a flag to see if a div was hidden

            // Loop through all the words in the array and hide the div if found
            for (var i = 0; i < arrayLength; i++) {
                if (title.indexOf(filter_array[i]) < 0) {
                    _this.hide();
                    hidden = 1;
                }
            }
            // If the flag hasn't been tripped show the div
            if (hidden == 0) {
                _this.show();
            }
        });
    });

    $('#low').click(function () {
        sortRows($('#container'), 'asc');
        sortRows2($('#container'), 'asc');
    });


    function sortRows($container, sortOrder /* "asc" or "desc" */) {
        var rowsArray = $.makeArray($container.find('.ad.pro'));
        rowsArray = rowsArray.sort(function (a, b) {
            var result = (Number(a.getAttribute('data-price') || 0) - Number(b.getAttribute('data-price') || 0)); // Using native getAttribute because a and b are DOM elements, not jQuery elements, and converting them is a wasted cycle
            return (result * (sortOrder === 'desc' ? -1 : 1)); // Flip if sort-order is desc
        });
        $container.append(rowsArray); // Put the elements in, reordered.
    }

        function sortRows2($container, sortOrder /* "asc" or "desc" */) {
        var rowsArray = $.makeArray($container.find('.ad').not(".pro"));
        rowsArray = rowsArray.sort(function (a, b) {
            var result = (Number(a.getAttribute('data-price') || 0) - Number(b.getAttribute('data-price') || 0)); // Using native getAttribute because a and b are DOM elements, not jQuery elements, and converting them is a wasted cycle
            return (result * (sortOrder === 'desc' ? -1 : 1)); // Flip if sort-order is desc
        });
        $container.append(rowsArray); // Put the elements in, reordered.
    }
    $('#cs').keyup(function () {
        //alert("ca;;ed");
        var filter_array = new Array();
        var filter = this.value.toLowerCase();  // no need to call jQuery here

        filter_array = filter.split(' '); // split the user input at the spaces

        var arrayLength = filter_array.length; // Get the length of the filter array

        $('.ad').each(function () {
            /* cache a reference to the current .media (you're using it twice) */
            var _this = $(this);
            var title = _this.find('.type').text().toLowerCase();

            /* 
            title and filter are normalized in lowerCase letters
            for a case insensitive search
            */

            var hidden = 0; // Set a flag to see if a div was hidden

            // Loop through all the words in the array and hide the div if found
            for (var i = 0; i < arrayLength; i++) {
                if (title.indexOf(filter_array[i]) < 0) {
                    _this.hide();
                    hidden = 1;
                }
            }
            // If the flag hasn't been tripped show the div
            if (hidden == 0) {
                _this.show();
            }
        });
    });

});

