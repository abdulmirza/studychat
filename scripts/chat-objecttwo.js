
            var distance = 0;
            var chatArray = [];
 function Chat(ad_id, ulMessage,chat_id,type) {
                // alert(ad_id);
                var ad_id = parseInt(ad_id);
                var lMessage = ulMessage;
                var cha = chat_id;
                var ty = type;
                // to keep the session id
                var sessionId = '';

                // name of the client
                var name = '';
                var id = 0;
                // socket connection url and port
                var socket_url = '192.168.0.101';
                var port = '8082';

                var webSocket;

                /**
                * Connecting to socket
                */
                this.join = function () {

                    openSocket();
                    return false;
                }

                /**
                * Will open the socket connection
                */
     function openSocket() {
                    // Ensures only one connection is open at a time
                    if (webSocket !== undefined && webSocket.readyState !== WebSocket.CLOSED) {
                        return;
                    }
                        
                    // Create a new instance of the websocket
                    webSocket = new WebSocket("ws://" + socket_url + ":" + port
                        + "/SellBooksChat/chat?name="+ty + "&id=" + ad_id.toString() +"&chat_id="+ cha);

                    /**
                    * Binds functions to the listeners for the websocket.
                    */
                    webSocket.onopen = function (event) {
                    };

                    webSocket.onmessage = function (event) {

                        // parsing the json data
                        parseMessage(event.data);
                       // alert("here");
                    };

                    webSocket.onclose = function (event) {
                        alert('Error! Connection is closed. Try connecting again.');
                    };
                }

                /**
                * Sending the chat message to server
                */
                this.send = function (mes) {
                    var message = mes;
                    // alert(message);
                    if (message.trim().length > 0) {
                        

                        var li = '<li><span class="name">' + ty+ '</span> '
                             + message + '</li>';

                        // appending the chat message to list
                        sendMessageToServer('message', message);
                        return li;

                    } else {
                        alert('Please enter message to send!');

                    }

                }

                /**
                * Closing the socket connection
                */
                this.closeSocket = function () {
                    //alert("Closing");
                     webSocket.close();
                }

                /**
                * Parsing the json message. The type of message is identified by 'flag' node
                * value flag can be self, new, message, exit
                */
                function parseMessage(message) {
                    var jObj = $.parseJSON(message);

                    // if the flag is 'self' message contains the session id
                    if (jObj.flag == 'self') {

                        sessionId = jObj.sessionId;

                    } else if (jObj.flag == 'new') {
                        // if the flag is 'new', a client joined the chat room
                        var new_name = 'You';

                        // number of people online
                        var online_count = jObj.onlineCount;

                        if (jObj.sessionId != sessionId) {
                            new_name = "Seller";
                        }

                        var li = '<li class="new"><span class="name">' + new_name + '</span> '
                            + jObj.message + '</li>';

                    } else if (jObj.flag == 'message') {
                        // if the json flag is 'message', it means somebody sent the chat
                        // message
                        var from_name = 'Buyer';

                        if (jObj.sessionId != sessionId) {
                            from_name = jObj.name;
                        }
                          appendToUl(jObj.message, from_name, lMessage);

                    } else if (jObj.flag == 'exit') {
                        // if the json flag is 'exit', it means somebody left the chat room
                        var li = '<li class="exit"><span class="name red">' + jObj.name
                            + '</span> ' + jObj.message + '</li>';

                        var online_count = jObj.onlineCount;
                        appendChatMessage(li);
                    }
                }

                /**
                * Sending message to socket server message will be in json format
                */
                function sendMessageToServer(flag, message) {
                    var json = '{""}';

                    // preparing json object
                    var myObject = new Object();
                    myObject.sessionId = sessionId;
                    myObject.message = message;
                    myObject.flag = flag;
                    json = JSON.stringify(myObject);
                    webSocket.send(json);
                }

                function appendToUl(message,name,id){
                    var ul = document.getElementById(id);
                    var li = document.createElement("li");
                    var span = document.createElement("span");
                    var p = document.createElement("p");
                    span.appendChild(document.createTextNode(name));
                     p.appendChild(document.createTextNode(message));
                    li.appendChild(span);
                    li.appendChild(p);
                    ul.appendChild(li);
                    ul.scrollTop = ul.scrollHeight;
                }
            }

            function fillArray(cnt){
                for(var i = 0;i < cnt; i++){
                    chatArray[i] = "None";
                }
            }

            $(document).ready(function () {
                (function () {
                    var n = $(".chat-box").length;
                    fillArray(n);
                })();
                $(".chat-box").each(function () {
                    $(this).on("click", "input[type=checkbox]", function (e) {
                        var chat_id = $(this).parent().find('.chatid').val();
                        var type = $(this).parent().find('.type').val();
                        var ad_id = $(this).parent().find('.adid').val();
                        var mess = $(this).parent().find("ul").attr("id");
                        var chat = new Chat(ad_id, mess, chat_id, type);
                        var num = $(this).parent().find(".num").val();
                        // chatArray.splice(num, 1, chat);
                        chatArray[num] = chat;
                        // chatArray.push(chat);
                        chat.join();
                    });

                    $(this).on("click", "button", function (e) {
                        var ul = "#" + $(this).parent().parent().find("ul").attr("id");
                        var inputValue = $(this).parent().parent().find("input[type=text]").val();
                        var num = $(this).parent().find(".num").val();
                        console.log("num: " + num + ": Input " + inputValue);
                        console.log(chatArray);
                        var li = chatArray[num].send(inputValue);

                        $(ul).append(li);
                        $(ul).scrollTop($(ul).prop("scrollHeight"));
                        $(this).parent().parent().find("input[type=text]").val("");
                    });
                });
            });