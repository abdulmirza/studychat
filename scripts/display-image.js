function imageIsLoaded(a) {
    document.getElementById("img").naturalWidth > 1024 ||
    document.getElementById("img").naturalHeight > 1024 ?
        (alert("Image resolution is too high. Please limit images to 1024*1024 resolution"), $("#img").attr("src", ""),
        $("#imgName").html("")) : $("#img").attr("src", a.target.result)
}
function removeImage() {
    $("#img").attr("src", "")
}

$(function() {
    $("#fimg").change(function() {
        var a = document.getElementById("fimg").files[0].size;
        a /= 1e6;
        var b = this.files[0].name.split(".")[this.files[0].name.split(".").length - 1].toLowerCase();
        if ("png" == b || "jpg" == b || "jpeg" == b)
            if (a <= 1.5) {
                if (this.files && this.files[0]) {
                    $("#imgName").html(this.files[0].name);
                    var c = new FileReader;
                    c.onload = imageIsLoaded, c.readAsDataURL(this.files[0])
                }
            } else alert("Please, select a smaller image size. The restriction is 1.5 MBs");
        else alert("Sorry, " + this.files[0].name + " is invalid, allowed extensions are: png, jpeg, jpg")
    });
});
