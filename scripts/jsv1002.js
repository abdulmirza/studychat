var messaging;
if (firebase.messaging.isSupported()) {
  messaging = firebase.messaging();
  messaging.usePublicVapidKey("BHnDH7puKJQEP2esC7aJ8cxf8gHNnpA659_MGyfNvSJ7ZGCqGmtX1HNHh5Tqo7BXX0OGYBuR98neusPFHEPhB_k");
} else {

}

var scheduler;
var secondPress = false;
var arr = [];
var currentChat;
var courseId = null;
var url = null;
var paintBrush = null;

Dropzone.autoDiscover = false;

$(document).ready(function () {
  if(getParameterByName('COURSE_ID') != null && getParameterByName('COURSE_ID') != "") {

    courseId = getParameterByName('COURSE_ID');

    console.log("CLASS", courseId);
    let name = getCookie("name");
    let id = getCookie("ACCID");

    let chat = new Chat(id, name, courseId, "messages" );

    $("#share_link").empty();
    $("#share_link").val('https://studychat.online/signin.php?CLASS='+courseId);
    currentChat = chat;

    let requestForOlderMessages = currentChat.getOlderMessages(courseId);

    requestForOlderMessages.then(currentChat.handleOlderMessages).then(function () {
      currentChat.join();

      getEnrolledStudents(courseId);
      getClassesFiles(courseId);
    });
  }

  if(mobileCheck()){
    $("#mobile").modal("toggle");
  } else if(getCookie("SCHOOL_ID") == "") {
    $("#selectSchoolModal").modal("toggle");
  }

  if(getCookie("SCHOOL_ID") != "") {
    let id = getCookie("SCHOOL_ID");
    getClasses(id);
    $(".school_link").attr('href', 'https://sellbooks.today/loadBooks.php?school='+id);
    $("#alert_select_school").alert('close');
  }

  getStudentsClasses();

  if(!mobileCheck()) {
    if(getCookie("PUSH_NOTI_GRANTED") != "" && getCookie("PUSH_NOTI_GRANTED") == "1") {
      $("#alert_grant_notification_access").hide();
    }
  } else {
    $("#alert_grant_notification_access").hide();
  }

 $('#sidebarCollapse').on('click', function () {
     $('#sidebar').toggleClass('active');
     $("#sidebarCollapse").toggleClass("center_vertical_justify");
 });

// $(document).click(function () {
//       $('#sidebar').addClass('active');
// });

 $("#classSelector")
 .dropdown({clearable: true,maxSelections: 10});

 $('.schoolSelect')
   .dropdown({
     action: 'hide',
     onChange: function(value, text, $selectedItem) {
       document.cookie = "SCHOOL_ID="+value+"; expires=Thu, 18 Dec 2022 12:00:00 UTC; path=/";
       $('.schoolSelect').dropdown('set selected', value);
       // $('#selected_school').text(text);
       $("#alert_select_school").alert('close');
       getClasses(value);
       $("#settingsModal").modal("hide");
    }
   })
 ;

$("#idontsee").click(function () {
    if(secondPress) {
            $("#cities").val("0");
            $("#city_dropdown").hide();
        $("#main_city").show();
    } else {
        secondPress = true
        $("#cities").prepend($("<option selected>Not a suburb</option>").val("0"));
        $("#city").show();
        $("#city_p_tag").show();
        $(this).text("I dont see the main city");
    }
});

$(".hamburger").click(function () {
  $(".hamburger").show();
  $(this).hide();
});

$("#change_chat_ok").click(function () {
  let input = $('input[name=selected_class]:checked');
  courseId = input.val();
  let className = input.parent().find("label").text();
  //console.log(input, input.parent().find("label"));
  $("#class_name").text(className);
  $("#uploadsModal .header").text("Files for "+className);

  console.log("CLASS", courseId);
  let name = getCookie("name");
  let id = getCookie("ACCID");

  if (name != "") {
    if (currentChat != null) {
        currentChat.closeTheWebsocket();
        currentChat = null;
        if(paintBrush != null) {
          paintBrush.closeTheWebsocket();
          paintBrush = null;
        }
    }

    let ul = "#messages";
    $(ul).empty();
    $("#imgHolder").empty();

    let title = $(this).parent().find('.dropbtn').text().substring(0, 35).toUpperCase();
    $("#cn").html(title);

    let chat = new Chat(id, name, courseId, "messages" );

    $("#share_link").empty();
    $("#share_link").val('https://studychat.online/signin.php?CLASS='+courseId);
    currentChat = chat;

    let requestForOlderMessages = currentChat.getOlderMessages(courseId);

    requestForOlderMessages.then(currentChat.handleOlderMessages).then(function () {
      currentChat.join();

      getEnrolledStudents(courseId);
      getClassesFiles(courseId);
    });
    //$("#freehand_drawing_msg").hide();


  } else {
    location.reload();
  }

  $("#changeChatModal").modal("toggle");
});

$("#create_chat_ok").click(function () {
  console.log(getCookie("SCHOOL_ID"));
   if(getCookie("SCHOOL_ID") != "") {
      submitClass();
   }
});

$("#join_chat").click(function () {
  if(getCookie("SCHOOL_ID") == "") {
      $("#selectSchoolModal").modal("toggle");

       $('#selectSchoolModal').on('hidden.bs.modal', function (e) {
         $("#joinChatModal").modal("toggle");
       });
  } else {
    $("#joinChatModal").modal("toggle");
  }
});

$("#joinChatModal .btn-primary").click(function () {
   let values = $("#classSelector").find("input[type=hidden]").val();
   values = values.split(",");
   console.log(values);

   submitClasses(values);
});


$("#send").click( function (e) {
  if(getCookie("user") != "") {
      sendMessage(url);
   } else {
     alert("Please Sign In before sending a message");
     window.location.replace("php/website/logout.php");
   }
});


$( "#message_footer .input_text" ).keypress(function(e) {
   if(e.keyCode === 13){
       e.preventDefault(); // Ensure it is only this code that rusn
       sendMessage(url);
       //alert("Enter was pressed was presses");
   }
});


var myAwesomeDropzone = new Dropzone("form#my-awesome-dropzone", {
   autoProcessQueue: false,
   uploadMultiple: false,
   parallelUploads: 10,
   maxFiles: 10,
   maxFilesize: 100,

   init: function () {
       var myDropzone = this;

       this.element.querySelector('button[type=submit]').addEventListener("click", function (e) {
         console.log(courseId);
         e.preventDefault();
         e.stopPropagation();

         if(courseId != null) {

           myDropzone.processQueue();
           $("#uploadModal .alert").hide();
         } else {
           alert("Please connect to a class.");
         }
       });
        this.on("sending", function(file, xhr, formData){
            formData.append("COURSE_ID", courseId);
            $(".loader").show();
        });
        this.on("success", function(file, responseText) {
          $(".loader").hide();
           console.log(responseText);
           let result = JSON.parse(responseText);
           url = result["URL"];
           $("#uploadModal .alert-success").show();
           $("#append_file").addClass("pulse");
           $( "#message_footer .input_text" ).attr("placeholder","Sending file and message...");
           getClassesFiles(courseId);
           setTimeout(function() {
             $("#uploadModal").modal("toggle");
           }, 2000);
             this.removeAllFiles(true);
        });
       this.on("sendingmultiple", function () {
         $(".loader").show();
       });
       this.on("successmultiple", function (files, response) {
         $(".loader").hide();
       });
       this.on("errormultiple", function (files, response) {
         $("#uploadModal .alert-danger").show();
         $(".loader").hide();
       });
   }
});

  $('#scheduleModal').on('shown.bs.modal', function (e) {
    if(scheduler != null){
      scheduler.destroy();
      scheduler = null;
    }
    scheduler = new armScheduler("schedule-container",1,4,null,
                             "./php/website/getScheduledTimes.php",
                             getCookie("ACCID"),
                             5
                             );
  });

  $('#freeHandDrawerModal').on('shown.bs.modal', function (e) {
    setupCanvas($(this));

    let drawer = new Drawer(classId, startPaintBrush);
    drawer.join();
  });
});

function startPaintBrush(drawer) {
    paintBrush = $('#canvas-display').paintBrush([],drawer);
}

function setupCanvas(parent) {
  let canvasContainer = $("#canvas_container");
  let canvas = document.getElementById("canvas-display");
  let controlsButton = $("#controls");

  let width = canvasContainer.innerWidth();
  let height = canvasContainer.innerHeight();
  canvas.width = width;
  canvas.height = height;

  let offsetTop = $(canvas).offset().top; //+ parent.find(".modal-header").height();
  let offsetLeft = $(canvas).offset().left;

  $(canvas).data("offset_top", offsetTop);
  $(canvas).data("offset_left", offsetLeft);

  //console.log("called", offsetTop, offsetLeft);
}

function getClasses(schoolId)  {
//  let schoolId = getCookie("SCHOOL_ID");
  let accid = $("#accid").val();
  $.get("php/website/getClasses.php", {SCHOOL_ID : schoolId, ACCID: accid})
  .done(function(data) {
      console.log(data);
      data = JSON.parse(data);
      let p = $("#classSelector .menu");
      $("#joinChatModal .alert").hide();
      if(data.length > 0) {
        p.empty();
              //     p.append('<option value="-1">Class</option>');
        $.each(data, function( i, v ) {
           let section = "";
           if(v.CLASS_SECTION != null) {
             section = v.CLASS_SECTION;
           }
            let str = "<div class='item' data-value='" + v.CLASS_ID + "'>" +v .CLASS_NAME + " " + section + "</div>";//'<option value="'+v.classId+'">'+v.CLASS_NAME + ' ' + section +'</option>';
            p.append(str);
        });
      } else {
        $("#no_classes_for_school").show();
      }
  });
}

function getStudentsClasses(){
  let accid = $("#accid").val();
   $.get("php/website/getClassesAndGroups.php", {ACCID : accid})
   .done(function(data) {
    //  console.log(data);
      data = JSON.parse(data);

      if(data.length > 0) {
         let p = $("#chats_holder");
         p.empty();
         $.each(data, function( i, v ) {

             let str =  '<div class="custom-control custom-radio" data-class-id="'+v.CLASS_ID+'">\
                           <input type="radio" value="'+v.CLASS_ID+'" class="custom-control-input" id="defaultUnchecked'+v.CLASS_ID+'" name="selected_class">\
                           <label class="custom-control-label class_el" for="defaultUnchecked'+v.CLASS_ID+'">'+v.CLASS_NAME+'</label>\
                         </div>';
             p.append(str);
         });
         $("#alert_first_time").alert('close');
      }
   });
}

function getEnrolledStudents(classId){
  $.get("php/website/getStudentsEnrolledInClass.php",{CLASS_ID: classId})
  .done(function(data) {
     console.log(data);
     data = JSON.parse(data);

     let p = $("#members_holder");
     p.empty();
     $.each(data, function( i, v ) {
         let str =  '<div class="row" style="padding: 2%">\
                       <div class="col">\
                         <p class="student_name" style="margin-bottom: 2%;">\
                           '+v.USERNAME+'\
                         </p>\
                         <p class="student_status margin_0">\
                           offline\
                         </p>\
                       </div>\
                    </div>';
         p.append(str);
     });
  });
}

function getClassesFiles(classId){
  $.get("php/website/getClassesFiles.php", {CLASS_ID : classId})
  .done(function(data) {
      //console.log(data);
      data = JSON.parse(data);

      let p = $("#files_container");
      p.empty();
      $.each(data, function( i, v ) {
         let str = '<div class="col-12 col-sm-12 col-lg-3 padding">\
                    <a href="'+v.PATH+'" download class="url">\
                     <div class="file_container">\
                       <div class="row">\
                         <div class="col-3">\
                           <img src="css/img/folder.svg" alt="folder" class="file_image">\
                         </div>\
                         <div class="col-9 padding_left">\
                           <p class="file_name">\
                             '+v.NAME+'\
                           </p>\
                           <span class="file_uploader"> '+v.UPLOADER+'</span>\
                         </div>\
                       </div>\
                       <div class="row margin_top_5">\
                         <div class="col">\
                           <p class="file_time_stamp">\
                             '+v.DATE+'\
                           </p>\
                         </div>\
                       </div>\
                       <div class="row">\
                         <div class="col">\
                               <span class="file_size">'+Math.ceil((v.SIZE/1000)/1000)+'MB</span>\
                         </div>\
                       </div>\
                     </div>\
                   </a></div>';
          p.append(str);
      });
  });
}

function submitClasses(classes) {
 let accid = $("#accid").val();
 $.post("php/website/enrollClass.php", {ACCID : accid, CLASSES: classes})
 .done(function(data) {
  //  console.log(data);
    data = JSON.parse(data);

    $("#joinChatModal .alert-success, #joinChatModal .alert-danger").hide();

    if(data["SUCCESS"]) {
      $("#joinChatModal .alert-success").show();

      setTimeout(function() {
        $("#joinChatModal").modal("toggle");
        $("#joinChatModal .alert-success").hide();
      }, 2000);

      getStudentsClasses();

      $("#classSelector").dropdown('clear');
   }
    else
      $("#joinChatModal .alert-danger").show();
 });
}

function submitClass() {
  let schoolId = getCookie("SCHOOL_ID");
  let className = $("#className").val();
  let classSection = $("#classSection").val();
  let accid = $("#accid").val();

  if(className != "") {
     $.post("php/website/createClass.php", {ACCID : accid, SCHOOL_ID : schoolId, CLASS_NAME : className, CLASS_SECTION : classSection})
     .done(function(data) {
      //  console.log(data);
        data = JSON.parse(data);

        $("#createChatModal .alert-success, #createChatModal .alert-danger").hide();

        if(data["SUCCESS"]) {
          $("#createChatModal .alert-success").show();
          setTimeout(function() {
            $("#createChatModal").modal("toggle");
          }, 3000);

          $("#className").val("");
          $("#classSection").val("");

          getStudentsClasses();
        }
        else {
          $("#createChatModal .alert-danger").show();
        }
     });
   }
}

function sendMessage(url) {
  if(currentChat != null) {
  var ul = "#messages";
  var inputValue = $("#message_footer").find("input").val();
  if(inputValue.length <= 230){
      currentChat.send(inputValue, url);
    //  $(ul).append(li);
      $(ul).scrollTop($(ul).prop("scrollHeight"));
      $("#message_footer").find("input").val("");
      $("#append_file").removeClass("pulse");
   }else{
      alert("Messages are limited to 230 characters for now");
   }
 } else {
   $("#changeChatModal").modal("toggle");
 }
}

if(messaging != null) {
  messaging.onTokenRefresh(() => {
    messaging.getToken().then((refreshedToken) => {
      console.log('Token refreshed.');
      sendTokenToServer(refreshedToken);
      resetUI();
    }).catch((err) => {
      console.log('Unable to retrieve refreshed token ', err);
  //    showToken('Unable to retrieve refreshed token ', err);
    });
  });

  messaging.onMessage((payload) => {
    console.log('Message received. ', payload);
  });
}

function resetUI() {
  messaging.getToken().then((currentToken) => {
    if (currentToken) {
      sendTokenToServer(currentToken);

      $("#alert_grant_notification_access").hide();
    } else {
      console.log('No Instance ID token available. Request permission to generate one.');
    }
  }).catch((err) => {
    console.log('An error occurred while retrieving token. ', err);
  //  showToken('Error retrieving Instance ID token. ', err);
  });
}

function sendTokenToServer(currentToken) {
  console.log(currentToken);
   let accid = $("#accid").val();

  $.post("php/website/addFirebaseToken.php", {ACCID : accid, TOKEN : currentToken})
  .done(function(data) {
     console.log(data);
     data = JSON.parse(data);
     if(data["SUCCESS"]) {

     } else {

     }
  });
}

function requestPermission() {
  console.log('Requesting permission...');
  Notification.requestPermission().then((permission) => {
    if (permission === 'granted') {
      console.log('Notification permission granted.');
      resetUI();
    } else {
      console.log('Unable to get permission to notify.');
    }
  });
}

function appendMessage(payload) {
  console.log(payload);
  const messagesElement = document.querySelector('#messages');
  const dataHeaderELement = document.createElement('h5');
  const dataElement = document.createElement('pre');
  dataElement.style = 'overflow-x:hidden;';
  dataHeaderELement.textContent = 'Received message:';
  dataElement.textContent = JSON.stringify(payload, null, 2);
  messagesElement.appendChild(dataHeaderELement);
  messagesElement.appendChild(dataElement);
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function mobileCheck() {
    var check = false;
    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1);
          if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
  }
  return "";
}
