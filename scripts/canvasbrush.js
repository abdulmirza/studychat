(function($) {
      $.fn.paintBrush = function(options, webSocket) {
            var undoHistory = [];

            var settings = {
               'targetID' : 'canvas-display'
            },
            $this = $(this),
            o = {},
            ui = {},
            canvas,
            cntxt,
            draw = 0,
            cntxt,
            top = 0,
            left = 0,
            webSocket,

            core = {
                  init : function(options) {
                        let canvasId = "#"+settings["targetID"];
                        console.log(canvasId);
                        ui.$loadParentDiv = o.targetID;
                        core.draw();
                        core.controls();
                        webSocket = webSocket;
                        canvas = document.getElementById(settings["targetID"]);
                        cntxt = canvas.getContext("2d");
                        top =  $(canvasId).offset().top;
                        left = $(canvasId).offset().left;

                          console.log("OFFSETS",  $(canvasId).offset().top ,  $(canvasId).offset().left,$(canvasId).data("offset_top"), $(canvasId).data("offset_left"));
                  //core.toggleScripts();
                  },

                  canvasInit : function() {
                      context = document.getElementById("canvas-display").getContext("2d");
                      context.lineCap = "round";
                      //Fill it with white background
                      context.save();
                      context.fillStyle = '#ecf0f1';
                      context.fillRect(0, 0, context.canvas.width, context.canvas.height);
                      context.restore();
                  },

                  saveActions : function() {
                      var imgData = document.getElementById("canvas-display").toDataURL("image/png");
                      undoHistory.push(imgData);
                      $('#undo').removeAttr('disabled');

                  },

                  undoDraw : function() {
                        if(undoHistory.length > 0){
                            var undoImg = new Image();
                            $(undoImg).load(function(){
                                var context = document.getElementById("canvas-display").getContext("2d");
                                context.drawImage(undoImg, 0,0);
                            });
                            undoImg.src = undoHistory.pop();
                            if(undoHistory.length == 0)
                                $('#undo').attr('disabled','disabled');
                        }
                  },

                  draw : function() {
                      core.canvasInit();

                      //Drawing Code
                      $('body').mousedown(function(e){
                          if(e.button == 0){
                              draw = 1;
                              core.saveActions(); //Start The drawing flow. Save the state
                              cntxt.beginPath();
                              let x = e.pageX - left;
                              let y = e.pageY - top;
                              cntxt.moveTo(x, y);
                                    console.log("mouse down",x, y, top, left);
                              webSocket.send(x,y,"lineStart");
                          } else {
                              draw = 0;
                          }
                      })
                       .mouseup(function(e){
                              if(e.button != 0){
                                  draw = 1;
                              } else {
                                  draw = 0;
                                  let x = e.pageX-left;
                                  let y = e.pageY-top;
                                  cntxt.lineTo(x,y);
                                        console.log("mouse up",x, y);
                                  cntxt.stroke();
                                  cntxt.closePath();
                                  webSocket.send(x,y,"lineEnd");
                              }
                      })
                       .mousemove(function(e){
                              if(draw == 1){
                                    //console.log("drawing");
                                    let x = e.pageX-left;
                                    let y = e.pageY-top;
                                    console.log("mouse move",x, y);
                                    cntxt.lineTo(x, y);
                                    cntxt.stroke();

                                    webSocket.send(x,y,"lineMiddle");
                              }
                      });

                  },
                  drawFromServer : function(x,y, action,image) {
                        var canvas, cntxt, top, left, draw, draw = 0;
                        canvas = document.getElementById("canvas-display");
                        cntxt = canvas.getContext("2d");

                        let newX = x ;//* canvas.width ;
                        let newY = y ;//* canvas.height;
                        console.log("x,y",x,y);
                        if(action == "lineStart") {
                              core.saveActions();
                              cntxt.beginPath();
                              cntxt.moveTo(newX, newY);
                        } else if (action == "lineMiddle") {
                              cntxt.lineTo(newX,newY);
                              cntxt.stroke();
                        } else if (action == "lineEnd") {
                              cntxt.lineTo(newX,newY);
                              cntxt.stroke();
                              cntxt.closePath();
                        } else if(action == "addImage") {
                              console.log("IMAGE",image);
                              var img = new Image();
                              img.onload = function() {
                                  var c = document.getElementById("canvas-display");
                                  var ctx = c.getContext("2d");
                                  //img.width = c.width;
                                  //img.height = c.height;
                                  //ctx.drawImage(img,0,0);
                                  ctx.drawImage(img, 0, 0, img.width, img.height,     // source rectangle
                                     0, 0, c.width, c.height);
                              }
                              img.src = 'data:image/png;base64,'+image;
                        }
                  },
                   getDataUri :function(callback) {
                        var image = new Image();
                       image.onload = function () {
                         var canvas = document.createElement('canvas');
                         canvas.width = this.naturalWidth;
                         canvas.height = this.naturalHeight;
                         canvas.getContext('2d').drawImage(this, 0, 0);
                         canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, '');
                         callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));
                    };
                  },
                   closeTheWebsocket: function (){
                     webSocket.closeTheWebsocket();
                     console.log("closing");
                  },
                  controls : function() {
                      canvas = document.getElementById("canvas-display");
                      cntxt = canvas.getContext("2d");
                      $('#export').click(function(e){
                              // e.preventDefault();
                              //window.open(canvas.toDataURL(), 'Canvas Export','height=1000,width=1000');
                              $(this).attr('href', canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"));
                       });

                       $('#clear').on("click", function(e){
                              e.preventDefault();
                              canvas.width = canvas.width;
                              canvas.height = canvas.height;
                              core.canvasInit();
                              $('#colors li:first').click();
                              $('#brush_size').change();
                              //core.toggleScripts();
                              undoHistory = [];
                        });

                        $('#brush_size').change(function(e){
                                cntxt.lineWidth = $(this).val();
                                //core.toggleScripts();
                        });

                        $('#colors li').on("click", function(e){
                                e.preventDefault();
                              $('#colors li').removeClass('selected');
                              $(this).addClass('selected');
                              cntxt.strokeStyle = $(this).css('background-color');
                              //core.toggleScripts();
                        });

                                  //Undo Binding
                        $('#undo').on("click", function(e){
                                e.preventDefault();
                              core.undoDraw()
                              //core.toggleScripts();
                        });

                        //Init the brush and color
                        $('#colors li:first').click();
                        $('#brush_size').change();

                        $('#controls').click(function(){
                              core.toggleScripts();
                        });

                        $("#img_canvas").change(function(e) {
                           var reader = new FileReader();
                           reader.onload = function(event){
                                var img = new Image();
                                img.onload = function(){
                                    var c = document.getElementById("canvas-display");
                                    var ctx = c.getContext("2d");
                                    //img.width = c.width;
                                    //img.height = c.height;
                                    //ctx.drawImage(img,0,0);
                                    ctx.drawImage(img, 0, 0, img.width,    img.height,     // source rectangle
                                       0, 0, c.width, c.height);
                                    webSocket.sendImage(c.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));
                                }
                                img.src = event.target.result;

                           }
                           reader.readAsDataURL(e.target.files[0]);
                     });
                  },

                  toggleScripts : function() {
                       $('#colors').toggle();
                       $('#control-buttons').toggle();
                  }
            };

            $.extend(true, o, settings, options);

            core.init();

            return core;
      };
 })(jQuery);
