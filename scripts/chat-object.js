var paintBrush = null;
var count = 0;

window.onbeforeunload = closingCode;
function closingCode(){
   // do something...
   currentChat.closeTheWebsocket();
   currentChat = null;
   return null;
}

function Chat(id, name, courseId, ulMessage) {
   let userName = name;
   courseId = parseInt(courseId);
   let lMessage = ulMessage;
   let socket_url = 'websocket.network';
   let port = '443';
   // var socket_url = 'localhost';
   // var port = '8080';
   var webSocket;

   var connectedUsers = 0;

   this.join = function(){
     openSocket();
     return false;
   }

   this.closeTheWebsocket = function(){
    // alert("closing");
     webSocket.close();
     return false;
   }

   this.send = function (mes, url) {
     if (mes.trim().length > 0) {
        let json = convertToJsonString("message", mes, url);
        sendMessageToServer(json);

        let data = new Object();

        data.MESSAGE = json;
        data.SENT_BY = id;

        return getLi(data);
     } else {
         alert('Please enter message to send!');
     }
   }

   this.getOlderMessages = function(courseId) {
      return new Promise(function(resolve, reject) {
        $.get("php/messages/getMessages.php", {COURSE_ID : courseId})
        .done(function(data) {
            resolve(data);
        });
     });
   }

   this.handleOlderMessages = function (data) {
      data = JSON.parse(data);

      data.forEach(function(jObj, index) {
         appendToUl(jObj);
      });
   }

   // this.handleErrors = function (error) {
   //    console.error("Something went wrong!", error);
   // }

   function openSocket() {
     if (webSocket !== undefined && webSocket.readyState !== WebSocket.CLOSED) {
         return;
     }

      try {
         webSocket = new WebSocket("wss://" + socket_url + ":" + port
             + "/chat?id="+id+"&name="+name + "&course_id=" + courseId.toString());

         webSocket.onopen = function (event) {
         };

         webSocket.onmessage = function (event) {
            console.log(event.data);
             parseMessage(event.data);
         };

         webSocket.onclose = function (event) {

         };

         webSocket.onerror = function (event) {
            console.log(event);
            alert("Please turn off adblock. StudyChat cannot load messages. "+event.error);
         };
      } catch(e) {
         alert("Please turn off adblock. StudyChat cannot load messages.");
      }

   }

   function closeSocket() {
     webSocket.close();
   }

   function appendToUl(jObj) {
     let li = getLi(jObj);

     $("#"+lMessage).append(li);

     $("#"+lMessage).scrollTop($("#"+lMessage).prop("scrollHeight"));
   }

   function getLi(jObj) {
      console.log(jObj);
      let cssClassName = "";

      if(id == jObj.SENT_BY)
        cssClassName = "me";
      else
        cssClassName = "you";

      let container = "";

      let max_width = $("#"+lMessage).width() * 0.40;
      //console.log("MAX_WIDTH", max_width);
      let msg = JSON.parse(jObj.MESSAGE);
      let name = msg.name != null ? msg.name : userName;

      if(msg.url != null || msg.image != null) {
         let url = msg.url || msg.image;
         // container = '<div class="width_100 '+cssClassName+'">\
         //              <div class="clearfix"><p class="sender">' + nameFrom +'</p><div><p class="message">'+ mes + '</p>'+
         //              '<a href="'+url+'" class="url" download>Download File</a></div></div></div>';
         container = '<div class="width_100 '+cssClassName+'">\
                            <span class="sender">' + name +'</span>\
                            <div class="both" style="width: '+max_width+'px">\
                               <span class="message">'+ msg.message + '</span>\
                               <a href="'+url+'" class="url" download><img src="'+url+'" class="width_100"/></a>\
                            </div>\
                      </div>';
      } else {
        container = '<div class="width_100 '+cssClassName+'">\
                           <span class="sender">' + name +'</span>\
                           <div class="both">\
                              <span class="message">'+ msg.message + '</span>\
                           </div>\
                     </div>';

      }

      console.log(url);


      return container;
   }

   function parseMessage(strjObj) {
    //console.log("m,essage",message);
     let jObj = JSON.parse(strjObj);

    if (jObj.MESSAGE_ID != null) {
       appendToUl(jObj);
    } else if (jObj.flag == 'new') {
        connectedUsers++;
        $("#num_of_students_online").val(connectedUsers);
     } else if (jObj.flag == 'exit') {
        connectedUsers--;
         $("#num_of_students_online").val(connectedUsers);
         //console.log("Clsoing: "+jObj.name);
     }
   }

   function sendMessageToServer(json) {
    console.log(json);
     if(webSocket.readyState !== webSocket.CLOSED){
         webSocket.send(json);
         url = null;
     } else {
        alert("You've been disconnected from the chat. Please connect again");
     }
   }

   function convertToJsonString(flag, message, url) {
      var json = '{""}';
      var msgObject = new Object();
      msgObject.message = message;
      msgObject.flag = flag;
     // myObject.url = url;

      if(url != null){
           msgObject.url = url;
      }

      json = JSON.stringify(msgObject);

      return json;
   }
}
