<div class="modal " id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background-color: transparent; border: none;box-shadow: none; overflow: visible">

    <div class="modal-header" style="    background-color: transparent;
    border: none;
    box-shadow: none;">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white; opacity: 1">
      <span aria-hidden="true">&times;</span>
    </button>
    </div>
      <div class="modal-body">
        <div class="row ">
            <div class="col">

              <div class="ui fluid search selection dropdown input_text schoolSelect">
               <!-- <input name="tags" type="hidden">-->
                <input type="hidden" required name="school" maxlength="200"/>
                <i class="dropdown icon"></i>
                <div class="default text">Select school</div>
                <div class="menu">
                    <?php require("./php/website/getSchools.php");?>
                </div>
              </div>

            </div>
        </div>
        <p class="center_text margin_top_10" style="color: #fff">
          OR
        </p>
        <div class="row margin_top_10">
          <div class="col padding">
            <button class="btn btn-lg btn-orange width_100" data-toggle="modal" data-target="#addSchoolModal">Add new school</button>
          </div>
          <div class="col padding">
            <button class="btn btn-lg btn-black width_100" data-toggle="modal" data-target="#joinChatModal">Join a chat</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
