<div class="modal fade" id="selectSchoolModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="overflow: visible">
      <div class="modal-header">
        <h5 class="modal-title">Select school</h5>
      </div>
      <div class="modal-body">
        <div class="row ">
            <div class="col">
              <p>
                Please select the school you attend.
              </p>
              <div class="ui fluid search selection dropdown input_text schoolSelect">
               <!-- <input name="tags" type="hidden">-->
                <input type="hidden" required name="school" maxlength="200"/>
                <i class="dropdown icon"></i>
                <div class="default text">Select school</div>
                <div class="menu">
                    <?php require("./php/website/getSchools.php");?>
                </div>
              </div>

            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close <?php echo file_get_contents("css/img/cross.svg"); ?></button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK  <?php echo file_get_contents("css/img/checkmark.svg"); ?></button>
      </div>
    </div>
  </div>
</div>
