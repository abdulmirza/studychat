<div class="modal fade" id="joinChatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Join a Chat</h5>
      </div>
      <div class="modal-body" style="height: 325px">
        <div class="alert alert-danger hide" role="alert" id="no_classes_for_school">
          <strong>Oh No!</strong> No classes for your school have been added yet! Be the first by going to the sidebar and clicking "Create a Chat".
        </div>
          <div class="row margin_top_2_5">
              <div class="col-12">
                <div class="ui fluid multiple search selection dropdown input_text empty" id="classSelector">
                 <!-- <input name="tags" type="hidden">-->
                  <input type="hidden" required name="class_name" maxlength="200"/>
                  <i class="dropdown icon"></i>
                  <div class="default text">Join a Chat</div>
                  <div class="menu">
                  </div>
                </div>
              </div>
          </div>

          <div class="alert alert-success hide" role="alert">
            <strong>Successfully Enrolled!</strong> You have Successfully enrolled.
          </div>
          <div class="alert alert-danger hide" role="alert">
            <strong>Oh No!</strong> We weren't able to enroll you in (some) of the classes you wannted. Please try submitting again!
          </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close <?php echo file_get_contents("css/img/cross.svg"); ?></button>
        <button type="button" class="btn btn-primary">OK  <?php echo file_get_contents("css/img/checkmark.svg"); ?></button>
      </div>
    </div>
  </div>
</div>
