<div class="modal fade" id="changeChatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="overflow: auto">
      <div class="modal-header">

        <h5 class="modal-title"><?php echo file_get_contents("css/img/change_chat.svg"); ?>Change Chat</h5>
      </div>
      <div class="modal-body">
        <div id="chats_holder">

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close <?php echo file_get_contents("css/img/cross.svg"); ?></button>
        <button type="button" class="btn btn-primary" id="change_chat_ok">OK  <?php echo file_get_contents("css/img/checkmark.svg"); ?></button>
      </div>
    </div>
  </div>
</div>
