<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 700px;">
    <div class="modal-content" >
      <div class="modal-header">
        <h5 class="modal-title"><?php echo file_get_contents("css/img/upload.svg");?>Upload files to group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
      </div>
      <div class="modal-body">
            <div class="loader hide"></div>
          <div class="row" style="height:95%;" id="file_select">
              <div class="col-12 center_text">
                <div class="alert alert-success hide" role="alert">
                  <strong>File uploaded successfully</strong> Your file has been uploaded successfully! Send a message and we will automatically inlude the link to download it in your message
                </div>
                <div class="alert alert-danger hide" role="alert">
                  <strong>Oh No!</strong> Your file didn't upload. Please try again.
                </div>

              </div>
          </div>
          <form action="php/website/fileUpload.php" class="dropzone" id="my-awesome-dropzone">
             <button type="submit" class="btn btn-lg btn_main_view float_right pulse" id="upload_file"><?php  echo file_get_contents("css/img/SEND.svg"); ?></button>
          </form>
          <!-- <div style="border:3px solid #007aff;padding:3%;border-radius:1000px; display: inline-block">

             <input id="fileupload" type="file" name="files[]" data-url="php/website/initUpload.php" multiple class="inputfile" />
            <label for="fileupload" id="upload_button" class="btn btn-lg btn_main_view"><?php // echo file_get_contents("css/img/upload.svg"); ?></label>
          </div> -->
          <!-- <div class="row margin_top_5 hide" style="height:95%;" id="file_upload">
              <div class="col-12 center_text">
                <div style="border:3px solid #007aff;padding:3%;border-radius:1000px; display: inline-block">
                  <div class="width_100">
                    <span id="progress_percent" class="float_left">0%</span>
                      <span id="progress_data" class="float_left">0MB</span>
                  </div>

                  <div id="progress" class="width_100 margin_top_5">
                    <div class="bar" style="width: 0%;" data-dz-uploadprogress></div>
                  </div>
                </div>

              </div>
          </div> -->

        </div>
    </div>
  </div>
</div>
