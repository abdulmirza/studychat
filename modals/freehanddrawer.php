<div class="modal" id="freeHandDrawerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <?php echo file_get_contents("css/img/freehand_drawing2.svg"); ?>
        <h5 class="modal-title">Freehand Drawing</h5>
      </div>
      <div class="modal-body" id="canvas_container" style="padding: 0">
        <canvas id="canvas-display" width="400" height="400" ></canvas>

        </div>
      <div class="modal-footer">
        <div style="width: 100%;display: inline-flex">
          <div style="width: 15%;display: flex;
justify-content: space-between;">
            <button type="button" class="btn btn-lg btn_main_view"><?php echo file_get_contents("css/img/pencil_light.svg"); ?></button>
            <button type="button" class="btn btn-lg btn_main_view"> <?php echo file_get_contents("css/img/pencil_regular.svg"); ?></button>
                      <button type="button" class="btn btn-lg btn_main_view"> <?php echo file_get_contents("css/img/pencil_bold.svg"); ?></button>
          </div>

          <div style="width: 75%">
            <div style="width: 50%; margin: 0 auto; display: flex;justify-content: space-between;">
              <button type="button" class="btn btn-lg btn_main_view"><?php echo file_get_contents("css/img/ellipse_black.svg"); ?></button>
              <button type="button" class="btn btn-lg btn_main_view"> <?php echo file_get_contents("css/img/ellipse_blue.svg"); ?></button>
              <button type="button" class="btn btn-lg btn_main_view"> <?php echo file_get_contents("css/img/ellipse_red.svg"); ?></button>
              <button type="button" class="btn btn-lg btn_main_view"> <?php echo file_get_contents("css/img/ellipse_yellow.svg"); ?></button>
              <button type="button" class="btn btn-lg btn_main_view"> <?php echo file_get_contents("css/img/ellipse_green.svg"); ?></button>
              <button type="button" class="btn btn-lg btn_main_view"> <?php echo file_get_contents("css/img/ellipse_orange.svg"); ?></button>
              <button type="button" class="btn btn-lg btn_main_view"> <?php echo file_get_contents("css/img/ellipse_purple.svg"); ?></button>
            </div>
          </div>

          <div id="freehanddrawer_user_btns_container">
            <button type="button" class="btn btn-lg btn_main_view" data-dismiss="modal"><?php echo file_get_contents("css/img/download.svg"); ?></button>
            <button type="button" class="btn btn-lg btn_main_view"> <?php echo file_get_contents("css/img/SEND.svg"); ?></button>
          </div>
        </div>



      </div>
    </div>
  </div>
</div>
