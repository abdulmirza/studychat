<div class="modal fade" id="addSchoolModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add your school</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
              <input type="text" name="school" placeholder="School name" class="input_text"/>
          </div>
        </div>
          <div class="row margin_top_2_5">
              <div class="col-lg-12">
                  <input type="text" name="address" placeholder="School address" class="input_text"/>
              </div>
          </div>
          <div class="row margin_top_2_5 hide" id="city">
              <div class="col-lg-12">
                  <input type="text" name="city_suburb" placeholder="City" class="input_text"/>
              </div>
          </div>
          <div class="row margin_top_2_5" id="city_dropdown">
              <div class="col-lg-12">
                      <p id="city_p_tag" class="details_text hide">
                          Is it a suburb of one these cities?
                      </p>
                      <select id="cities" name="city" class="input_text">
                          <?php
                             require("php/website/getSuburbs.php");
                           ?>
                      </select>
                  <a id="idontsee" style="float: right;display: block">I don't see my city</a>
              </div>
          </div>
          <div class="row margin_top_2_5 hide" id="main_city">
              <div class="col-lg-12">
                  <input type="text" name="city_main" placeholder="Main city" class="input_text"/>
              </div>
          </div>
          <div class="row margin_top_2_5">
              <div class="col-lg-12">
                  <input type="number" placeholder="Postal code" class="input_text"/>
              </div>
          </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close <?php echo file_get_contents("css/img/cross.svg"); ?></button>
        <button type="button" class="btn btn-primary">OK  <?php echo file_get_contents("css/img/checkmark.svg"); ?></button>
      </div>
    </div>
  </div>
</div>
