<div class="modal fade" id="firsttimeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">WELCOME TO STUDYCHAT!</h5>
      </div>
      <div class="modal-body">
          <div class="row margin_top_2_5">
              <div class="col-12">

                <p>
                  Looks like this is your first time using StudyChat! To get started in 3 easy steps:
                </p>

                <ol class="padding_0">
                  <li>
                     go to settings (gear icon) and select a school (if you haven't selected it yet).
                  </li>
                  <li class="margin_top_2_5">
                     Click the "Join a chat" button and enroll into any class you want!
                  </li>
                  <li class="margin_top_2_5">
                     Dismiss the modal and click "Change chat" in top left to begin chatting!
                  </li>
                </ol>

              </div>
          </div>


        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close <?php echo file_get_contents("css/img/cross.svg"); ?></button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK  <?php echo file_get_contents("css/img/checkmark.svg"); ?></button>
      </div>
    </div>
  </div>
</div>
