<div class="modal fade" id="createChatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="overflow: visible">
      <div class="modal-header">
        <h5 class="modal-title">Create a chat</h5>
      </div>
      <div class="modal-body">
        <div class="alert alert-success hide" role="alert">
          <strong>Class successfully created!</strong> You have Successfully created and enrolled into your class!
        </div>
        <div class="alert alert-danger hide" role="alert">
          <strong>Oh No!</strong> We weren't able to create your class! Please try submitting again.
        </div>
        <div class="row">
          <div class="col">
            <div class="ui fluid search selection dropdown input_text schoolSelect">


             <!-- <input name="tags" type="hidden">-->
              <input type="hidden" required name="school" maxlength="200"/>
              <i class="dropdown icon"></i>
              <div class="default text">Select a school...</div>
              <div class="menu">
                  <?php require("./php/website/getSchools.php");?>
              </div>
            </div>
          </div>
        </div>
        <div class="row margin_top_2_5">
          <div class="col">
            <input type="text" class="input_text" id="className" placeholder="Class name, Club name, etc..."/>
          </div>
        </div>
        <div class="row margin_top_2_5">
          <div class="col">
            <input type="text" class="input_text" id="classSection" placeholder="Class section/Group number (Optional)"/>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close <?php echo file_get_contents("css/img/cross.svg"); ?></button>
        <button type="button" class="btn btn-primary" id="create_chat_ok">OK  <?php echo file_get_contents("css/img/checkmark.svg"); ?></button>
      </div>
    </div>
  </div>
</div>
