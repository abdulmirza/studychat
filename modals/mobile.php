<div class="modal fade" id="mobile" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Mobile Support!</h5>
      </div>
      <div class="modal-body">
          <div class="row margin_top_2_5">
              <div class="col-12">

                <p>
                  Hey, We see you're on mobile! Although studychat works perfectly fine on mobile consider using this website on a pc. We have to remove some content on mobile due to
                  handheld devices screens being too small.
                </p>


              </div>
          </div>


        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close <?php echo file_get_contents("css/img/cross.svg"); ?></button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK  <?php echo file_get_contents("css/img/checkmark.svg"); ?></button>
      </div>
    </div>
  </div>
</div>
