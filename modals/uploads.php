<div class="modal" id="uploadsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col">
            <span class="header">Files for Class</span>
              <button id="upload_file" class="btn btn-info btn_solid_main_view" data-toggle="modal" data-target="#uploadModal">upload files</button>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>

          </div>
        </div>

        <div class="row margin_top_5" id="files_container">

        </div>

      </div>
    </div>
  </div>
</div>
