<?php

$classes = $_GET["CLASS"] ?? array();
$queryString = "?";

foreach((array) $classes as $id){
  $queryString .= "CLASS[]=" . $id;
}

if (isset($_COOKIE["user"]) && isset($_COOKIE["loggedIn"]) && isset($_COOKIE["ACCID"])) {
		//echo 1

    header("Location: studychat.php". $queryString);
}

if(isset($_COOKIE["ACCID"])) {
          header("Location: studychat.php");
}

$userCredentialsDontMatch= "none";
$showInputLeftBlank = "none";
$someThinghasGoneWrong = "none";
$accountMade = "none";
$wrongPassOrEmail = "none";
$duplicateAccount = "none";

if(isset($_GET["RESULT"])){

    $result = $_GET["RESULT"];
    //echo "Result".$result;
    if(strcmp($result , "5") == 0){
        $wrongPassOrEmail = "block";
    }
}
?>
<?php
  $cssFiles = array('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">',
    '<link href=css/cssv1001.css rel=stylesheet>',
    '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">');
  require "shared/header.php"
?>

<div class="container">
  <div class="row margin_top_10">
    <div class="col">
      <!-- <img src="css/img/studychat-itunes.png" style="width:100px;"/> -->
      <p class="studychat center_text">
        StudyChat
      </p>
    </div>
  </div>

  <div class="row margin_top_5">
      <div class="alert alert-danger" style="display: <?php echo $wrongPassOrEmail;?>"> <strong>Wrong Email or Password!</strong> Please turn off caps lock and try again </div>
      <div class="alert alert-warning">
            <strong>You may already have an account!</strong> If you've have an account on SellBooks that same account will work here! The same email and password will work.
      </div>
    <div class="col-lg-3 col-12">
      <form action="php/website/signin.php" method="POST">


      <div class="row">
        <div class="col">
          <p class="header_text text_align_left"><?php echo $document[0]->nodeValue ?></p>
        </div>
      </div>

      <div class="row margin_top_5">
          <div class="col-lg-12">
              <input type="text" name="email-login" placeholder="Username or email" maxlength="50" class="input_text entry"/>
          </div>
      </div>
      <div class="row margin_top_5">
          <div class="col-lg-12">
              <input type="password" name="pass-login" placeholder="<?php echo $document[4]->nodeValue ?>" required class="input_text entry"/>
          </div>
      </div>
      <?php
         foreach((array) $classes as $id){
            echo '<input type="hidden" name="CLASS[]" value="'.$id.'">';
         }
      ?>
      <!-- <div class="row margin_top_5">
          <div class="col-lg-12">
            <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
  <input type="checkbox" class="custom-control-input">
  <span class="custom-control-indicator"></span>
  <span class="custom-control-description">Remember my preference</span>
</label>
          </div>
      </div> -->
      <div class="row margin_top_5">
          <div class="col-12">
              <input type="submit" value="Sign in" class="btn btn-primary sign_btn"/>
          </div>
      </div>
      <div class="row margin_top_5">
          <div class="col-12 text-md-left text-center">
              <a href="signup.php<?php echo $queryString?>"> Dont have an account? Sign up!</a>
          </div>
      </div>
      <div class="row margin_top_5">
          <div class="col-12 text-md-left text-center">
              <a href="forgotPassword.php<?php echo $queryString?>">Can't sign in? Reset your password!</a>
          </div>
      </div>
      </form>
    </div>

    <div class="col-lg-6 col-12 offset-md-3 d-none d-sm-none d-md-none d-lg-block">
        <?php echo file_get_contents("css/img/undraw_group_chat_v059.svg"); ?>
    </div>
  </div>
</div>
