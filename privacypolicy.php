<?php
  $cssFiles = array('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">',
    '<link href=css/cssv1001.css rel=stylesheet>',
    '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">');
  require "shared/header.php";
?>

<div>
  <p>End user license agreement:
This is an agreement between StudyChat – Chat with classmates (“StudyChat”) and you,  a user of StudyChat service. If you do not agree to thses terms, you may not use the StudyChat service. By continuing to use the StudyChat service you hereby declare that you accept these terms.</p>
<br>
<p>
1.	Advertising ID -
StudyChat does not save nor store any devices Adevertising Id. StudyChat does not track a user based on Advertising Id. StudyChat does not/cannot determine a user from the current nor previous Advertising Id. StudChat does limit
</p>
<br />
<p>
1.	Ability to Accept Terms of License -
You affirm that you have the ability to enter this license and conform to its terms.
</p>
<br>
<p>
2.	Changes to this license -
StudyChat reserves the right to modify, update, change and amend the terms of this license. It is the users’ obligation to review this license for any changes. Your continued use of the service will signify your acceptance to these terms.
</p>
<br>
<p>
3.	Acknowledgement -
The end user hereby agrees that this agreement is concluded between StudyChat and its representatives and the end user. Apple nor its affiliates have any part in this agreement
</p>
<br>
<p>
4.	Scope of License -
This license, once accepted, cannot be transferred nor transmitted in any way, shape or form to anyone. This license is only valid on devices owned by any end user who has accepted this policy.
</p>
<br>
<p>
5.	Maintenance and Support -
All maintenance and support will be carried out by StudyChat and its representatives. Apple has no obligation to further the maintenance and support of this service.
</p>
<br>
<p>
6.	Warranty -
StudyChat and its representatives are solely responsible for the warranty of this service. Apple has no obligation to fulfill this warranty.
</p>
<br>
<p>
7.	Product Claims -
All product claims are to be addressed by StudyChat and its representatives. Apple is not responsible for any product claims pertaining to StudyChat
</p>
<br>
<p>
8.	Intellectual Property Rights -
If in the event a copyright is infringed, StudyChat, Not Apple, takes sole responsibility of the investigation, defense, settlement and discharge of any such infringement.
</p>
<br>
<p>
9.	Legal Compliance - The end-user must represent and warrant that (i) he/she is not located in a country that is subject to a U.S. Government embargo, or that has been designated by the U.S. Government as a “terrorist supporting” country; and (ii) he/she is not listed on any U.S. Government list of prohibited or restricted parties.
</p>
<br>
<p>
10.	Developer Name and Address -
All complaints and questions can be sent to:
Name: Abdul-Rahman Mirza
Address: 60 vermont Longueuil, Qc, Canada
Contact info: abdul.rahman.mirza@hotmail.com
</p>
<br>
<p>
11.	Third Party Beneficiary - You and the end-user must acknowledge and agree that Apple, and Apple’s subsidiaries, are third party beneficiaries of the EULA, and that, upon the end-user’s acceptance of the terms and conditions of the EULA, Apple will have the right (and will be deemed to have accepted the right) to enforce the EULA against the end-user as a third party beneficiary thereof.
</p>
</div>

            <!--<div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse30">Why are the buttons for promoting grayed out on the website?</a>
                    </h4>
                </div>
                <div id="collapse30" class="panel-collapse collapse">
                    <div class="panel-body">
                        Currently, We are still finishing this feature on the website version but on the App, they work just fine.
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">What does "what type" mean?</a>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse">
                    <div class="panel-body">
                        When creating an ad you must select a "Type of Book". This allows users to know if the book your selling is for the class they are in.
                        It takes the format
                        Type of class: Name of Class.
                        Example: "English: Introduction to College English"

                    </div>
                </div>
            </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">What should I write for the "Name of book" text field?</a>
                    </h4>
                </div>
                <div id="collapse5" class="panel-collapse collapse">
                    <div class="panel-body">
                        For this textfield please right ONLY the name of the book. Extra information such as edition, version, volume should be written into the description.

                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">How Can I delete an Ad?</a>
                    </h4>
                </div>
                <div id="collapse10" class="panel-collapse collapse">
                    <div class="panel-body">
                        To delete an ad simply click the delete button situated right next to the image of the Ad. If on the SellBooks app simply long hold (Android) or swipe to delete (iOS) on the ads you want to delete and then click the delete button in the top right corner.

                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">How Can I edit an Ad?</a>
                    </h4>
                </div>
                <div id="collapse6" class="panel-collapse collapse">
                    <div class="panel-body">
                        Go to "My account" click on the section where ever your ad might be (Ads: for unpromoted, Payed Ads for promoted) and simply click on the ad you wish to edit.

                    </div>
                </div>
            </div>

             <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">I clicked the Delete button on my Ad?</a>
                    </h4>
                </div>
                <div id="collapse7" class="panel-collapse collapse">
                    <div class="panel-body">
                        Good News, your ad has not be deleted. Simply refresh the page to get it back. As long as you dont press the Big Red Delete Button your ad is not permanently deleted.
                    </div>
                </div>
            </div>

             <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">What does the "What book are you looking for" page do?</a>
                    </h4>
                </div>
                <div id="collapse8" class="panel-collapse collapse">
                    <div class="panel-body">
                        By telling us what book you are looking for, we can save any book that is being sold at your school for you so that when you visit your account overview tab "What book are you looking for" we can display all books that you need.
                    </div>
                </div>
            </div>-->
        </div>
    </div>

    <?php
    $jsFiles = null;
    require "shared/footer.php";?>
