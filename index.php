<?php
  $cssFiles = array('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">',
    '<link href=css/cssv1001.css rel=stylesheet>',
    '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">');
  require "shared/header.php";
?>


  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <a class="navbar-brand js-scroll-trigger" href="#page-top">StudyChat</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
          <a class=" nav-link index_page_button btn btn-outline-light nav-link js-scroll-trigger ml-auto" href="filesView.php">Launch Courses File Viewer</a>
      </li>
      <li class="nav-item">
          <a class=" nav-link index_page_button btn btn-outline-light nav-link js-scroll-trigger ml-auto" href="signin.php">Launch web app</a>
      </li>
    </ul>
  </div>
</nav>

<div class="jumbotron jumbotron-fluid">
  <div>
    <h1 id="jumbotron_header"> Communicate with your classmates!</h1>
    <p id="jumbotron_subtext" class="">A direct communication line with your whole class!
Find answers to your questions easily!</p>

    <div id="jumbotron_btns">
      <div class="row">
        <div class="col center_text">
          <a style="display: inline-block" class="index_page_button_inv btn btn-outline-light nav-link js-scroll-trigger margin_top_2_5" href="signin.php">Launch web app</a>
        </div>
        <!-- <div class="col">

        </div> -->
      </div>
    </div>
  </div>

</div>

<section id="facts">

        <div class="row">
            <div class="col-lg-12 center_text">
                <p class="header_text">
                    <?php echo $document[5]->nodeValue ?>
                </p>
            </div>
        </div>
        <div class="row row_spacing">
            <div class="col-lg-4 col-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="facts_img_bg center_margin center_text vertical-center">
                                <?php echo file_get_contents("css/img/question_bubble.svg"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="facts_sub_header">
                                <?php echo $document[6]->nodeValue ?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="facts_text">
                                <?php echo $document[7]->nodeValue ?>
                            </p>
                        </div>
                    </div>
            </div>
            <div class="col-lg-4 col-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="facts_img_bg center_margin center_text vertical-center">
                                <?php echo file_get_contents("css/img/calendar_clock.svg"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="facts_sub_header">
                                <?php echo $document[8]->nodeValue ?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="facts_text">
                                <?php echo $document[9]->nodeValue ?>
                            </p>
                        </div>
                    </div>
            </div>
            <div class="col-lg-4 col-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="facts_img_bg center_margin center_text vertical-center">
                                <?php echo file_get_contents("css/img/stick-man.svg"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="facts_sub_header">
                                <?php echo $document[10]->nodeValue ?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="facts_text">
                                <?php echo $document[11]->nodeValue ?>
                            </p>
                        </div>
                    </div>
            </div>

        </div>
</section>

<section class="section_bg">
    <div class="row">
      <div class="col padding">
        <p class="how_to_header_text">
          <?php echo $document[12]->nodeValue ?>
        </p>
        <p class="how_to_text">
          <?php echo $document[13]->nodeValue ?>
        </p>
      </div>
      <div class="col padding">
        <img src="css/img/index_page_screens/desktop/select_school.png" class="how_to_img"/>
      </div>
    </div>
</section>

<section>
    <div class="row">
      <div class="col padding">
        <img src="css/img/index_page_screens/desktop/create_a_chat.png" class="how_to_img"/>
      </div>
      <div class="col padding">
        <p class="how_to_header_text">
          <?php echo $document[14]->nodeValue ?>
        </p>
        <p class="how_to_text">
          <?php echo $document[15]->nodeValue ?>
        </p>
      </div>

    </div>
</section>

<section class="section_bg">
    <div class="row">
      <div class="col padding">
        <p class="how_to_header_text">
          <?php echo $document[16]->nodeValue ?>
        </p>
        <p class="how_to_text">
          <?php echo $document[17]->nodeValue ?>
        </p>
      </div>
      <div class="col padding">
        <img src="css/img/index_page_screens/desktop/share.png" class="how_to_img"/>
      </div>
    </div>
</section>

<!--<section>
  <div class="container">
    <div class="row">
      <div class="col">
        <p class="how_to_header_text">
          <?php //echo $document[11]->nodeValue ?>
        </p>
        <p class="how_to_text">
          <?php //echo $document[11]->nodeValue ?>
        </p>
      </div>
      <div class="col">
        <img src="css/img/communicate.png" class="how_to_img"/>
      </div>
    </div>
  </div>
</section>-->

<section style="height: 400px; display: flex;justify-content:center;align-content:center;flex-direction:column; ">
    <div class="row" >
      <div class="col center_text">
        <p class="how_to_header_text">
          <?php echo $document[18]->nodeValue ?>
        </p>
        <a class="btn btn-lg launch_web_app margin_top_2_5" href="signin.php">Launch web app</a>
      </div>
    </div>
</section>

<footer id="footeR_bg">
    <div class="row center_text">
      <div class="col">
        <p class="footer_text">
          <?php echo $footer[0]->nodeValue ?>
        </p>
      </div>
      <div class="col">
        <p class="footer_text">
          <?php echo $footer[1]->nodeValue ?>
        </p>
      </div>
      <div class="col">
        <p class="footer_text">
          <?php echo $footer[2]->nodeValue ?>
        </p>
      </div>
    </div>
    <hr style="background-color: #fff;margin: 5% 0"/>
    <div class="row">
      <div class="col">
        <p class="center_text" style=" color: #b2bec3;opacity:0.7">
          Copyright 2018 <?php echo date("Y.m.d") ?>
        </p>
      </div>
    </div>
</footer>

<?php
$jsFiles = null;
require "shared/footer.php";?>
