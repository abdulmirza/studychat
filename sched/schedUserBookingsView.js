function armSchedulerForUserBookings(container, maxWeeks, amtOfInts,disabledTimes,phpFile,phpFileDisabled,userID){

var events = Create3DArray(maxWeeks,7);
var last = false;
var heightOfIntervals = 0;
var mY = 0;
var currEvent = null;
var editing = false;
var week = 0;
var amt;
var orderedDays = [];

createScheduler();

$('#pop a').click(function (e) {
  e.preventDefault();
    $('#pop').find('.info').remove();
  $('#pop').css({"display":"none"});
  $('.sel').find('.ctv').show();
  $('.sel').removeClass('sel');
});

$( '.timings-container' ).on( 'click', '.cancel', function () {
  removeBooking($(this).parent().parent().parent());
});

$( '#pop' ).on( 'click', '.cancel', function (e) {
  e.stopPropagation();
  removeBooking($(".sel"));
    $(this).parent().parent().parent().hide();

});

$('.timings-container').on('click', '.overlay', function() {
  if(tooSmall(this)){
    $('#pop').find('.info').remove();
    var posX = positionBox(this);
    var posY =  $(this).position().top;
    if($(this).position().top+200 > $( document ).height()){
      posY = $(this).position().top-100;
    }
    $('#pop').css({"display": "block", "top":posY,"left":posX});
    $(this).children().show();
    $('#pop').append($(this).children().clone());
    $('#pop').find(".ctv").hide();
    $(this).children().hide();
    $('#pop').find('.btn-cont').css({position: 'static'});
    $(this).addClass("sel");
  }
});

$('body').mousemove(function(e) {
    if (e.pageY < mY) {
    //    console.log('From Bottom');
        increaseTime = false;
    // moving downward
    } else {
      increaseTime = true;
    }
    mY = e.pageY;
});

$( ".interval" ).hover(
      function() {
      var e = getInterval(this);
        if (e == undefined || !e.disabled && currEvent != null && increaseTime && !editing){
          if(e.x == currEvent.x){
              $( this ).addClass("booked");
            var destination = $(this).position();
            e.top = destination.top;
            e.start = getStartTime(e.y,e.x,0,this);
              e.type = 5;
              e.edited = true;
              setInterval(e);
              currEvent = e;

            }
        }
      }, function() {
        var e = getInterval(this);
        if(typeof e !== "undefined"){
        if(typeof currEvent != "undefined" && !increaseTime && currEvent != null &&e.x == currEvent.x && (e.y == (currEvent.y - 1) || e.y == currEvent.y) && !editing){
          $( this ).removeClass("booked");
            currEvent = jQuery.extend(true, {}, e);
          removeInterval(e);
        }
      }
      }
);

$( '.timings-container' ).on( 'click', '.editbtn', function () {
       $('.sel').show();
       $(this).parent().hide();
       $('.sel').find('.ctv').show();

       $('#pop').find('.info').remove();
            $('#pop').find('.ctv').remove();
            $('#pop').hide();
            var days = convertDaysArrayToAssocArray($(".day"));
            var i = currEvent == null ? $(".sel") : $($(days[currEvent.x+"o"])).find(".interval");

            currEvent = new Object();
            currEvent.x = $(".sel").data("day");
            $('.overlay.sel').removeClass('sel');
            buildOverlay( i);

            editing = false;
});

$('.'+container).on( 'click', '.next', function () {
        week+=7;
        if(week / 7 == maxWeeks -1){
          $(this).hide();
        }

        $(".prev").show();
        $($($( ".weekdays" )).children()).remove();
        $( ".weekdays" ).append("<li>Time</li>");
        var today = new Date();
        today.setDate(today.getDate()+week);
        for(var x = 0;x < 7;x++){
          figureOutDates(today,x);
        }
        showBookings();
});

$('.'+container).on( 'click', '.prev', function () {

      week-=7;
      if(week == 0){
      $(this).hide();
    }
      $(".next").show();
        $($($( ".weekdays" )).children()).remove();
        $( ".weekdays" ).append("<li>Time</li>");
        var today = new Date();
        today.setDate(today.getDate()+week);
        for(var x = 0;x < 7;x++){
          figureOutDates(today,x);
        }

        showBookings();
});

function createScheduler(){
    amt = 18 * amtOfInts;
    var size = 100 / amt;
    $("."+container).append('<div class="month"><ul><li class="prev">&#10094;</li><li class="next">&#10095;</li><li style="text-align:center" ><p style="font-size:24px;margin:0px" id="month"></p><p style="font-size:18px;margin:0px" id="year"></p></li></ul></div><ul class="weekdays"><li>Time</li></ul><div class="sched-container"><div class="t-container"></div><div class="timings-container"><div id="pop"><a href="#">close</a></div><div id="ent" class="circleBase"><button class="editbtn"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></button></div></div></div>');
  var today = new Date();
  $(".prev").hide();
  if(maxWeeks == 1)
    $(".next").hide();

    var date = new Date();
    var d = date.getDay();
    var ds = 0;
  while(ds < 7){
    var day = "<div class='day'>";

    for(var sec = 0; sec < amt; sec++){
      var dotted = "";
      if((sec+1) % amtOfInts == 0)
        dotted = "border-bottom:1px dashed #26A69A";
      else
      if(sec % amtOfInts == 0)
        dotted = "border-top:1px dashed #26A69A";

      day += "<div class='interval' data-x='"+d+"' data-y='"+sec+"' style='height:"+size+"%;"+dotted+"'></div>";
      orderedDays[d+"o"] = ds;
    }
    day += "</div>";

    ds++;
    d++;
    if(d > 6){
      d = 0;
    }
    $(".timings-container").append(day);
  }

  $("#month").html(getMonthName(today.getMonth()));
  $("#year").html(today.getFullYear());

  for(var x = 0;x < 7;x++){
    var td = today.getDay();
    figureOutDates(today,x);
  }

  for(var x = 6;x < 24;x++){
    var z = "am";
    var h = x;
    if(x > 12){
      h -= 12;
      z = "pm";
    }
    $(".t-container").append("<div class='time'><p>"+h+z+"</p></div>");
  }
  heightOfIntervals = $(".interval").outerHeight();

  getTutorBookings();
}

function figureOutDates(today,diff){
  var day = new Date();
  day.setDate(today.getDate() + diff);
  setWeekDays(day);
}

function setWeekDays(day){
  var days = ['Su','Mo','Tu','We','Th','Fr','Sa'];
  $( ".weekdays" ).append("<li>"+days[day.getDay()]+ " "+ day.getDate()+"</li>");
}

function getMonthName(month){
  var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
  ];
  return monthNames[month];
}

function Create3DArray(weeks,rows) {
  var w = [];
    for(var x=0;x<weeks;x++){
        var d = [];
      for (var i=0;i<rows;i++) {
        d[i] = [];
      }
      w[x] = d;
    }
  return w;
}

function tooSmall(o){
  var s = $( o ).data( "start" );
  var e = $( o ).data( "end" );
  return (e - s) < amtOfInts * 2;
}

function formatAMPM(date, addT) {
  if(addT){
    var d = new Date(date.getTime());
    d.setMinutes(d.getMinutes() + (60 / amtOfInts));
    date = d;
  }
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = ('0'+minutes).slice(-2);
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

function getStartTime(y,x,add15Mins,c){
  var hour = y / amtOfInts;
  var interval = hour % 1;
  var time = interval * 60;
  var today = new Date();
  var diff;
  var i = $(".day").index($(c).parent());
  var td = today.getDate();

  diff = td + i+week;

  var d = new Date();
  d.setDate(diff);
  d.setHours(Math.floor(hour+6));
  d.setMinutes(time+add15Mins);
  d.setSeconds(0);
  return d;
}

function removeBooking(o){
  var day = o.data( "day" );
  var start = o.data( "start" );
  var end = o.data( "end" );

  var days = convertDaysArrayToAssocArray($(".day"));

  var ints = $($(days[day+"o"])).find(".interval");
  var es = convertDaysArrayToAssocArray(events[week / 7]);
  var id = null;
  for(var i = start; i <= end;i++){
    //console.log("ED",es[day+"o"][i]);
    $(ints[i]).removeClass("booked");
    if(es[day+"o"][i].hasOwnProperty("id"))
      id = es[day+"o"][i].id;
    es[day+"o"][i] = undefined;
  }

  if(id != null){
    $.post( "./php/bookingmanipulator/deleteBooking.php", { booking_id: id,account_id:Cookies.get("ACCID")})
    .done(function( data ) {
      console.log("DATA",data);
      if(!data){
        alert("Booking refund has not been processed. Error ID: " + id);
      }
    });
  }

  o.remove();
}

function disableTimesForBookings(disabledTimes){
 var seg = 60 / amtOfInts;
  if(disabledTimes != null){
    es = disabledTimes;
    var interval,time;
    $(".day").find(".booking").remove();
    $(".day").find(".overlay.before").remove();
    $(".day").find(".booked.before").removeClass("booked before");
    for(var x = 0;x < es.length;x++){
      var css = "booked before";
      if(es[x].type == 2)
        css = "u-booked";
      else if(es[x].type == 3)
        css = "c-booked";

      var d = es[x].start.getDay();
      var s = es[x].start;
      var e = es[x].end;
      var start = s.getHours() * amtOfInts - (6*amtOfInts);

      interval = s.getMinutes();

      time = (Math.round(interval/seg)*seg) / seg;
      start += time;
      var st = new Object();
      st.x = d;
      st.y = start;
      st.start = es[x].start;
      st.start.setMinutes(Math.round(interval/seg)*seg);

      var end = e.getHours() * amtOfInts - (6*amtOfInts);

      interval = e.getMinutes();

      time = (Math.round(interval/seg)*seg) / seg;
      end += time;

      var days = convertDaysArrayToAssocArray($(".day"));

      var ints = $($(days[d+"o"])).find(".interval");

      var totalSquaresBooked = end-start;

      var weekdays = convertDaysArrayToAssocArray(events[week]);
      for(var i = start; i < end;i++){
          var en = new Object();
          en.x = d;
          en.y = i;
          en.start = getStartTime(i,d,0,$(ints[i]));
          if(i == end-1) {en.top = $(ints[i]).position().top;}

          en.type = es[x].type;

          $(ints[i]).addClass(css);

          en.top =  $(ints[i]).position().top;

          en.id = es[x].id;
          weekdays[d+"o"][i] = en;
      }
      configOverlay(st,en,totalSquaresBooked,$(ints).parent(),d,es[x].type)
    }
  }
}

function getInterval(o){
  var xy = getXY(o);
  var o = new Object();
  o.x =xy.x;
  o.y = xy.y;

  var weekdays = convertDaysArrayToAssocArray(events[week / 7]);
  if(weekdays[xy.x+"o"][xy.y] != null && weekdays[xy.x+"o"][xy.y].disabled){
    o.disabled = true;
  }else{
    o.disabled = false;
  }
  return o;
}

function setInterval(o){
  var weekdays = convertDaysArrayToAssocArray(events[week / 7]);
  weekdays[o.x+"o"][o.y] = o;
}

function getXY(o){
  var x = $( o ).data( "x" );
  var y = $( o ).data( "y" );
  var xy = new Object();
  xy.x = x;
  xy.y = y;
  return xy;
}

function positionBox(o){
  if($(o).position().left+400 < $( document ).width()){
    return $(o).position().left+100;
  }
  return $(o).position().left-200;
}

function removeInterval(o){
  var weekdays = convertDaysArrayToAssocArray(events[week / 7]);
  weekdays[o.x+"o"][o.y] = undefined;
}

function buildOverlay(o){
 //console.log(o);
  var par = $(o).parent();
  var weekdays = convertDaysArrayToAssocArray(events[week / 7]);

  par.find(".overlay").remove();
  var start=null;
  var end = null;
  var totalSquaresBooked = 0;

  for(var z = 0;z < amt+1;z++){
    var e = weekdays[currEvent.x+"o"][z];
    if( e != null && e.hasOwnProperty("edited")){
      if(weekdays[currEvent.x+"o"][z+1] != null)
        editBookings(weekdays,e,1);
      if(weekdays[currEvent.x+"o"][z-1] != null)
        editBookings(weekdays,e,-1);
    }
    if(isDifferent(weekdays[currEvent.x+"o"][z-1],e)){
      if(totalSquaresBooked > 0){
            end = weekdays[currEvent.x+"o"][z-1];
            configOverlay(start,end,totalSquaresBooked+1,par,currEvent.x,end.type);
            totalSquaresBooked = 0;
            end = null;
            start = null;
      }
      if(start == null){
        start = e;
      }
    }else{
      totalSquaresBooked += 1;
    }
  }
  currEvent = null;
  $("#ent").hide();
}

function configOverlay(start,end,totalSquaresBooked,par,x,type){
  console.log("END",end,start,totalSquaresBooked);
  var x = currEvent == null ? x : currEvent.x;
  var btn = "<div class='btn-cont'><button class='btn cancel btn-book' style='width:100%'>Cancel</button></div>";
  var c = "overlay booking";
  var v = end.start;
  if (type == 3) {btn= ""; c = "overlay disabled";} else
  if (type == 2) {btn= "";} else
  if (type == 4) {c = "overlay before"} else
  if (type == 1) {c = "overlay passed";btn=""} else
  if (type == 5) {c = "overlay may"}
  if(end.end != null){v= end.end;}
  var height = totalSquaresBooked * heightOfIntervals;
  var ol = "<div data-day='"+x+"' data-start='"+start.y+"' data-end='"+end.y+"' class='"+c+"'><div class='info'><p class='start'></p><p class='end'></p>"+btn+"</div>";
  var object = $($.parseHTML(ol));
  object.find("p:first").html("Start Time - " + formatAMPM(start.start));
  object.find("p:last").html("End Time - " + formatAMPM(v,true));
  par.append($(object).css({"top": (end.top+heightOfIntervals) - height, height: height + "px"}));

  if(tooSmall(object)){
    $(object).children().hide();
    var size = (end.y - start.y) / 1.5;
    var f = 1 -size;
    object.append("<p class='ctv' style='font-size:"+(f == 1 ? .5 : f)+"em'>Click to View</p>");
  }
  //console.log("totalSquaresBooked THISA",start,end,totalSquaresBooked,par,x,type,$($.parseHTML(ol)));
console.log("Evenbts",events);
}

function showBookings(){
  $($(".schedule-container").children()).find(".overlay").remove();
  $(".c-booked").removeClass("c-booked");
  $(".pass-booked").removeClass("pass-booked");
  $(".u-booked").removeClass("u-booked");
  var days = convertDaysArrayToAssocArray($(".day"));
  var weekdays = convertDaysArrayToAssocArray(events[week / 7]);
  for(var x = 0;x< 7;x++){
      var d = $($(days[x+"o"]).children()).removeClass("booked");
      var i = $(d).get(0);
      currEvent = new Object();
      currEvent.x = x;
      recreateBooking(weekdays,$(days[x+"o"]).children(),x);
      buildOverlay(i);
  }
}

function recreateBooking(weekdays,is,x){
  for(var y = 0;y < is.length;y++){
      var e = weekdays[x+"o"][y];
      if(e != null && e.type == 1)
        $($(is).get(y)).addClass("pass-booked");
      else if(e != null && e.type == 2)
        $($(is).get(y)).addClass("u-booked");
      else if(e != null && e.type == 3)
        $($(is).get(y)).addClass("c-booked");
      else if(e != null && e.type == (4 || 5))
        $($(is).get(y)).addClass("booked");
  }
}

function isDifferent(prev,curr){
  var diff = false;
  if(typeof curr === "undefined" || typeof prev === "undefined"){
    diff = true;
  }else if(prev.type != curr.type && (prev.type != 5 && prev.type != 4)){
    diff = true;
  }//else if()
  return diff;
}

function convertDaysArrayToAssocArray(days){
  var day = new Date();
  var d = day.getDay();
  var i = [];
  for(var x = 0;x < days.length;x++){
    i[d.toString()+"o"] = days[x];
    d++;
    if(d > 6){
      d = 0;
    }
  }
  return i;
}

function getTutorBookings(){
  $.get( phpFile, { time:moment().format('M/D/YYYY, h:mm:ss A'), ACCID: userID})
    .done(function( data ) {
     console.log("DDD",data);
     data = JSON.parse(data);
      var es = [];
      for(var i = 0;i < data.length;i++){
        var e = new Object();
        e.start = new Date(data[i].booking_start);
        e.end = new Date(data[i].booking_end);
        e.id = data[i].id;
        if(data[i].bookerid == userID || data[i].tutorid == userID){
          e.disabled = false;
          e.type=4;
        }
        else{
          e.disabled = true;
          e.type=2;
        }
        es.push(e);
      }
      console.log("ED",es);
      disableTimesForBookings(es);
    });
}

armSchedulerForUserBookings.prototype.destroy = function () {
  $("."+container).empty();
};
}
