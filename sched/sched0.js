function armScheduler(container, maxWeeks, amtOfInts,disabledTimes,phpFile,userID,classId){

  var events = Create3DArray(maxWeeks,7);
  var last = false;
  var heightOfIntervals = 0;
  var mY = 0;
  var currEvent = null;
  var editing = false;
  var week = 0;
  var amt;
  var orderedDays = [];

  var fromPOP = false;
  var refreshDisabledPassedTimesInterval = null;
  createScheduler();

  $('#pop a').click(function (e) {
    e.preventDefault();
    $('#pop').find('.info').remove();
    $('#pop').css({"display":"none"});
    $('.sel').find('.ctv').show();
    $('.sel').removeClass('sel');
    fromPOP = true;
  });

  $('.interval').on('click', function() {
    var color = $( this ).css("background-color");

    var e = getInterval(this);
    if (e == null || !e.disabled){
      if(color == "rgba(0, 0, 0, 0)"){
        $(this).addClass("booked");
        var destination = $(this).position();
        e.top = destination.top;
        e.start = getStartTime(e.y,e.x,0,this);
        e.type = 5;
        e.edited = true;
        currEvent = e;
        setInterval(e);
      }else if(color == "rgb(0, 126, 101)" && currEvent != null && !editing){
        var destination = $(this).position();
        e.top = destination.top;
        var add = 0;
        e.start = getStartTime(e.y,e.x,add,this);
        e.type = 5;
        e.edited = true;
        setInterval(e);
        currEvent = e;
        buildOverlay(this);

      }else{
        var weekdays = convertDaysArrayToAssocArray(events[week]);
        $(this).removeClass("booked");
        currEvent = jQuery.extend(true, {}, e);
        weekdays[e.x+"o"][e.y]  = undefined;
        removeIdsFromBookings(weekdays,currEvent);
        editBookings(weekdays,currEvent, -1);
        editing = true;
      }
    }
  });

  $( '.timings-container' ).on( 'click', '.cancel', function () {
    removeBooking($(this).parent().parent().parent());
  });

  $( '.timings-container' ).on( 'click', '.edit', function () {
    let par = $(this).parent().parent().parent();
    $(".sel").hide();
    $('.circleBase').css({display:"block","top":par.position().top,"left":par.position().left+10});
    par.hide();
    par.addClass("sel");
  });

  $( '#pop' ).on( 'click', '.cancel', function (e) {
    e.stopPropagation();
    removeBooking($(".sel"));
    $(this).parent().parent().parent().hide();

  });

  $('.timings-container').on('click', '.overlay', function() {
    if(tooSmall(this)){
      $('#pop').find('.info').remove();
      var posX = positionBox(this);
      var posY =  $(this).position().top;
      if($(this).position().top+200 > $( document ).height()){
        posY = $(this).position().top-100;
      }
      $('#pop').css({"display": "block", "top":posY,"left":posX});
      $(this).children().show();
      $('#pop').append($(this).children().clone());
      $('#pop').find(".ctv").hide();
      $(this).children().hide();
      $('#pop').find('.btn-cont').css({position: 'static'});
      $(this).addClass("sel");
    }
  });

  $('body').mousemove(function(e) {
    if (e.pageY < mY) {
      //    console.log('From Bottom');
      increaseTime = false;
      // moving downward
    } else {
      increaseTime = true;
    }
    mY = e.pageY;
  });

  $( ".interval" ).hover(
    function() {
      var e = getInterval(this);
      if (e == undefined || !e.disabled && currEvent != null && increaseTime && !editing){
        if(e.x == currEvent.x){
          $( this ).addClass("booked");
          var destination = $(this).position();
          e.top = destination.top;
          e.start = getStartTime(e.y,e.x,0,this);
          e.type = 5;
          e.edited = true;
          setInterval(e);
          currEvent = e;

        }
      }
    }, function() {
      var e = getInterval(this);
      if(typeof e !== "undefined"){
        if(typeof currEvent != "undefined" && !increaseTime && currEvent != null &&e.x == currEvent.x && (e.y == (currEvent.y - 1) || e.y == currEvent.y) && !editing){
          $( this ).removeClass("booked");
          currEvent = jQuery.extend(true, {}, e);
          removeInterval(e);
        }
      }
    }
  );

  $( '.timings-container' ).on( 'click', '.editbtn', function () {
    $('.sel').show();
    $(this).parent().hide();
    $('.sel').find('.ctv').show();

    $('#pop').find('.info').remove();
    $('#pop').find('.ctv').remove();
    $('#pop').hide();
    var days = convertDaysArrayToAssocArray($(".day"));
    var i = currEvent == null ? $(".overlay.sel") : $($(days[currEvent.x+"o"])).find(".interval");
    //console.log("I",$(".overlay.sel").data(),currEvent);
    currEvent = new Object();
    currEvent.x = $(".overlay.sel").data("day");
    $('.sel').removeClass('sel');
    buildOverlay(i);

    editing = false;
  });

  $( '.schedule-container' ).on( 'click', '.next', function () {

    week++;
    if(week == maxWeeks -1){
      $(this).hide();
      $( ".maxTimePassed" ).fadeIn( 1000).delay(1500).fadeOut(1000);
    }

    $(".prev").show();
    $($($( ".weekdays" )).children()).remove();
    $( ".weekdays" ).append("<li></li>");
    //var today = new Date();
    //today.setDate(today.getDate()+week);
    for(var x = 0;x < 7;x++){
    //  console.log("WEEK",week,x);
      figureOutDates(week,x);
    }
    showBookings();
  });

  $( '.schedule-container' ).on( 'click', '.prev', function () {
    week--;
    if(week == 0){
      $(this).hide();
      disablePassedTimes();
    }
    $(".next").show();
    $($($( ".weekdays" )).children()).remove();
    $( ".weekdays" ).append("<li></li>");
  //  var today = new Date();
    //today.setDate(today.getDate()+week);
    for(var x = 0;x < 7;x++){
    //  console.log("WEEK",week,x);
      figureOutDates(week,x);
    }

    showBookings();
  });

  function createScheduler(){
    amt = 18 * amtOfInts;
    var size = 100 / amt;
    $("."+container).append('<div class="month"><ul><li class="prev">&#10094;</li><li class="next">&#10095;</li><li style="text-align:center" ><p style="font-size:24px;margin:0px" id="month"></p></p></li></ul></div><ul class="weekdays"><li></li></ul><div class="sched-container"><div class="t-container"></div><div class="timings-container"><div id="pop"><a href="#">close</a></div><div id="ent" class="circleBase"><button class="editbtn"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></button></div></div></div><div class="maxTimePassed"><p>Bookings can only be made up to '+maxWeeks+' weeks in advance</p></div>');
    var today = new Date();
    $(".prev").hide();
    if(maxWeeks == 1)
    $(".next").hide();

    var date = new Date();
    var d = date.getDay();
    var ds = 0;
    while(ds < 7){
      var day = "<div class='day'>";

      for(var sec = 0; sec < amt; sec++){
        var dotted = "";
        if((sec+1) % amtOfInts == 0)
          dotted = "border-bottom:1px dashed #FF7B55";
        else if(sec % amtOfInts == 0)
          dotted = "border-top:1px dashed #FF7B55";

        day += "<div class='interval' data-x='"+d+"' data-y='"+sec+"' style='height:"+size+"%;"+dotted+"'></div>";
        orderedDays[d+"o"] = ds;
      }
      day += "</div>";

      ds++;
      d++;
      if(d > 6){
        d = 0;
      }
      $(".timings-container").append(day);
    }

    $("#month").html(getMonthName(today.getMonth()) + " " + today.getFullYear());
  //  $("#year").html(today.getFullYear());

    for(var x = 0;x < 7;x++){
      figureOutDates(0,x);
    }

    let z = "am";
    for(var x = 6;x < 24;x++){
      var h = x;
      if(x > 12){
        h -= 12;
      }else if(x == 12){
        z = "pm";
      }
      $(".t-container").append("<div class='time'><p>"+h+z+"</p></div>");
    }
    heightOfIntervals = $(".interval").outerHeight();

    getScheduledTimes();
  }

  function figureOutDates(week,diff){
    let day = new Date();
  //  console.log("TODAY",day);
    let add = 0;
    if(week > 0)
      add = 6 * week;
    day.setDate(day.getDate()+week+add+diff);
    //  console.log("TODAY2",day,week,diff);
  //  day.setDate(day.getDate() + diff);
    //  console.log("TODAY3",day);
    setWeekDays(day);
  }

  function setWeekDays(day){
    let days = ['Su','Mo','Tu','We','Th','Fr','Sa'];
    $( ".weekdays" ).append("<li>"+days[day.getDay()]+" "+ day.getDate()+"</li>");
  //  console.log("week days",day,day.getDay(),days[day.getDay()],day.getDate());
  }

  function getMonthName(month){
    var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  return monthNames[month];
}


function Create3DArray(weeks,rows) {
  var w = [];
  for(var x=0;x<weeks;x++){
    var d = [];
    for (var i=0;i<rows;i++) {
      d[i] = [];
    }
    w[x] = d;
  }
  return w;
}

function tooSmall(o){
  var s = $(o).data("start");
  var e = $(o).data("end");
  return (e - s) < amtOfInts * 2;
}

function formatAMPM(date, addT) {
  if(addT){
    var d = new Date(date.getTime());
    d.setMinutes(d.getMinutes() + (60 / amtOfInts));
    date = d;
  }
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = ('0'+minutes).slice(-2);
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

function getStartTime(y,x,add15Mins,c){
  var hour = y / amtOfInts;
  var interval = hour % 1;
  var time = interval * 60;
  var today = new Date();
  var diff;
  var i = $(".day").index($(c).parent());
  var td = today.getDate();

  diff = td + i+week*7;

  var d = new Date();
  d.setDate(diff);
  d.setHours(Math.floor(hour+6));
  d.setMinutes(time+add15Mins);
  d.setSeconds(0);
  return d;
}

function removeBooking(o){
  var day = o.data( "day" );
  var start = o.data( "start" );
  var end = o.data( "end" );

  var days = convertDaysArrayToAssocArray($(".day"));

  var ints = $($(days[day+"o"])).find(".interval");
  var es = convertDaysArrayToAssocArray(events[week / 7]);
  var id = null;
  for(var i = start; i <= end;i++){
    //console.log("ED",es[day+"o"][i]);
    $(ints[i]).removeClass("booked");
    if(es[day+"o"][i].hasOwnProperty("id"))
      id = es[day+"o"][i].id;
    es[day+"o"][i] = undefined;
  }

  if(id != null){
    $.post( "./php/bookingmanipulator/deleteBooking.php", { booking_id: id,account_id:Cookies.get("ACCID"),time:moment().format('M/D/YYYY, h:mm:ss A')})
    .done(function( data ) {
      console.log("DATA",data);
      data = JSON.parse(data);
      if(!data["SUCCESS"]){
        alert("Booking refund has not been processed. Error Message: "+data["MESSAGE"] +" Error ID: " + id);
      }
    });
  }

  o.remove();
}

function disableTimesForWeekDays(disabledTimes){
  var seg = 60 / amtOfInts;

  if(disabledTimes != null){
    es = disabledTimes;
    let interval,time;

    $(".day").find(".booking").remove();
    $(".day").find(".overlay.disabled").remove();
    $(".day").find(".c-booked").removeClass("c-booked");

    let days = convertDaysArrayToAssocArray($(".day"));
              let css = "c-booked";
    for(let w = 0;w < maxWeeks;w++){
      let weekdays = convertDaysArrayToAssocArray(events[w]);
      for(let d = 0;d < 7;d++){
        let ints = $($(days[d+"o"])).find(".interval");
        for(let i = 0; i < (18*amtOfInts);i++){
          let en = new Object();
          en.x = d;
          en.y = i;
          en.start = getStartTime(i,d,0,$(ints[i]));
          en.type = 3;//es[x].type;
          heightOfIntervals = $(ints[i]).outerHeight();
          $(ints[i]).addClass(css);
          en.top =  $(ints[i]).position().top;
          weekdays[d+"o"][i] = en;
        }
      }
    }

    for(var x = 0;x < es.length;x++){

      var d = es[x].start.getDay();
      var s = es[x].start;
      var e = es[x].end;
      var start = s.getHours() * amtOfInts - (6*amtOfInts);

      interval = s.getMinutes();

      time = (Math.round(interval/seg)*seg) / seg;
      start += time;
      let st = new Object();
      st.x = d;
      st.y = start;
      st.start = es[x].start;
      st.start.setMinutes(Math.round(interval/seg)*seg);

      let end = e.getHours() * amtOfInts - (6*amtOfInts);

      interval = e.getMinutes();

      time = (Math.round(interval/seg)*seg) / seg;
      end += time;
      let ints = $($(days[d+"o"])).find(".interval");

      let totalSquaresBooked = end-start;

      for(var w = 0;w < maxWeeks;w++){
        let weekdays = convertDaysArrayToAssocArray(events[w]);
        for(var i = start; i < end;i++){

          $(ints[i]).removeClass(css);
          weekdays[d+"o"][i] = undefined;
        }
      }

    //  configOverlay(st,en,totalSquaresBooked,$(ints).parent(),d,es[x].type)
  }
  showBookings();
  }
}

function disableTimesForWeekDaysTemp(disabledTimes){
  var seg = 60 / amtOfInts;

  if(disabledTimes != null){
    es = disabledTimes;
    let interval,time;

    $(".day").find(".booking").remove();
    $(".day").find(".overlay.disabled").remove();
    $(".day").find(".c-booked").removeClass("c-booked");

    let days = convertDaysArrayToAssocArray($(".day"));
    let css = "c-booked";
      let weekdays = convertDaysArrayToAssocArray(events[0]);
      for(let d = 0;d < 7;d++){
        let ints = $($(days[d+"o"])).find(".interval");
        for(let i = 0; i < (18*amtOfInts);i++){
          let en = new Object();
          en.x = d;
          en.y = i;
          en.start = getStartTime(i,d,0,$(ints[i]));
          en.type = 3;//es[x].type;
          heightOfIntervals = $(ints[i]).outerHeight();
          $(ints[i]).addClass(css);
          en.top =  $(ints[i]).position().top;
          weekdays[d+"o"][i] = en;
        }
      }

    for(var x = 0;x < es.length;x++){
      let d = es[x].start.getDay();
      if(d == new Date().getDay()){

      var s = es[x].start;
      var e = es[x].end;
      var start = s.getHours() * amtOfInts - (6*amtOfInts);

      interval = s.getMinutes();

      time = (Math.round(interval/seg)*seg) / seg;
      start += time;
      let st = new Object();
      st.x = d;
      st.y = start;
      st.start = es[x].start;
      st.start.setMinutes(Math.round(interval/seg)*seg);

      let end = e.getHours() * amtOfInts - (6*amtOfInts);

      interval = e.getMinutes();

      time = (Math.round(interval/seg)*seg) / seg;
      end += time;
      let ints = $($(days[d+"o"])).find(".interval");

      let totalSquaresBooked = end-start;

      for(var w = 0;w < maxWeeks;w++){
        let weekdays = convertDaysArrayToAssocArray(events[w]);
        for(var i = start; i < end;i++){

          $(ints[i]).removeClass(css);
          weekdays[d+"o"][i] = undefined;
        }
      }

    }
  }
  showBookings();
  }
}

function configOverlay(start,end,totalSquaresBooked,par,x,type){
//  console.log("CALLED",start,end,totalSquaresBooked);
    var x = currEvent == null ? x : currEvent.x;
    var btn = "<div class='btn-cont'><button class='btn cancel btn-book'>Cancel</button><button class='btn edit btn-book'>Edit</button></div>";
    var c = "overlay booking";
    var v = end.start;
    if (type == 3) {btn= ""; c = "overlay disabled";} else
    if (type == 2) {btn= "";} else
    if (type == 4) {c = "overlay before"} else
    if (type == 1) {c = "overlay passed";btn=""} else
    if (type == 5) {c = "overlay may"}
    if(end.end != null){v= end.end;}
    var height = (totalSquaresBooked * heightOfIntervals);



    var ol = "<div data-day='"+x+"' data-start='"+start.y+"' data-end='"+end.y+"' class='"+c+"'><div class='info'><p class='start'></p><p class='end'></p>"+btn+"</div>";
    var object = $($.parseHTML(ol));
    object.find("p:first").html("Start Time - " + formatAMPM(start.start));
    object.find("p:last").html("End Time - " + formatAMPM(v,true));
    $(object).css({"top": (end.top+heightOfIntervals) - height, height: height + "px"});

    par.append(object);

    if(tooSmall(object)){
      $(object).children().hide();
      var size = (end.y - start.y) / 1.5;
      var f = 1 -size;
      object.append("<p class='ctv' style='font-size:"+(f == 1 ? .5 : f)+"em'>Click to View</p>");
    }
    //console.log("Evenbts",events);
  }


  function disableTimesForBookings(disabledTimes){
    var seg = 60 / amtOfInts;
    if(disabledTimes != null){
      es = disabledTimes;
      var interval,time;
      $(".day").find(".booking").remove();
      $(".day").find(".overlay.before").remove();
      $(".day").find(".booked.before").removeClass("booked before");
      var today  = new Date();
      for(var x = 0;x < es.length;x++){
        var css = "booked before";
        if(es[x].type == 2)
        css = "u-booked";
        else if(es[x].type == 3)
        css = "c-booked";

        var d = es[x].start.getDay();
        var s = es[x].start;
        var e = es[x].end;
        var start = s.getHours() * amtOfInts - (6*amtOfInts);

        interval = s.getMinutes();

        time = (Math.round(interval/seg)*seg) / seg;
        start += time;
        var st = new Object();
        st.x = d;
        st.y = start;
        st.start = es[x].start;
        st.start.setMinutes(Math.round(interval/seg)*seg);

        var end = e.getHours() * amtOfInts - (6*amtOfInts);

        interval = e.getMinutes();

        time = (Math.round(interval/seg)*seg) / seg;
        end += time;

        var days = convertDaysArrayToAssocArray($(".day"));

        var ints = $($(days[d+"o"])).find(".interval");

        var totalSquaresBooked = end-start;

        var nextWeek = new Date();
        nextWeek.setDate(nextWeek.getDate()+6);

        var w = 0;
        if(s >= nextWeek){
          w++;
        }

        var weekdays = convertDaysArrayToAssocArray(events[w]);

        for(var i = start; i < end;i++){
          var en = new Object();
          en.x = d;
          en.y = i;
          en.start = getStartTime(i,d,0,$(ints[i]));
          //if(i == end-1) {en.top = $(ints[i]).position().top;}

          en.type = es[x].type;
          //heightOfIntervals = $(ints[i]).outerHeight();

          $(ints[i]).addClass(css);

          en.top =  $(ints[i]).position().top;

          en.id = es[x].id;
          weekdays[d+"o"][i] = en;
        }
        showBookings();
        //configOverlay(st,en,totalSquaresBooked,$(ints).parent(),d,es[x].type)
      }
    }
  }

  function getInterval(o){
    var xy = getXY(o);
    var o = new Object();
    o.x =xy.x;
    o.y = xy.y;

    var weekdays = convertDaysArrayToAssocArray(events[week]);
    if(weekdays[xy.x+"o"][xy.y] != null && weekdays[xy.x+"o"][xy.y].disabled){
      o.disabled = true;
    }else{
      o.disabled = false;
    }
    return o;
  }

  function setInterval(o){
    var weekdays = convertDaysArrayToAssocArray(events[week]);
    weekdays[o.x+"o"][o.y] = o;
  }

  function getXY(o){
    var x = $( o ).data( "x" );
    var y = $( o ).data( "y" );
    var xy = new Object();
    xy.x = x;
    xy.y = y;
    return xy;
  }

  function positionBox(o){
    if($(o).position().left+400 < $( document ).width()){
      return $(o).position().left+100;
    }
    return $(o).position().left-200;
  }

  function removeInterval(o){
    var weekdays = convertDaysArrayToAssocArray(events[week]);
    weekdays[o.x+"o"][o.y] = undefined;
  }


function showBookings(){
  $($(".schedule-container").children()).find(".overlay").remove();
  $(".c-booked").removeClass("c-booked");
  $(".pass-booked").removeClass("pass-booked");
  $(".u-booked").removeClass("u-booked");
  var days = convertDaysArrayToAssocArray($(".day"));
  var weekdays = convertDaysArrayToAssocArray(events[week]);

  for(let x = 0;x< 7;x++){
    let d = $($(days[x+"o"]).children()).removeClass("booked");
    let i = $(d).get(0);
    currEvent = new Object();
    currEvent.x = x;
    recreateBooking(weekdays,$(days[x+"o"]).children(),x);
    buildOverlay(i);
  }
}


function buildOverlay(o){
    //console.log(o,currEvent);
    var par = $(o).parent();
    var weekdays = convertDaysArrayToAssocArray(events[week]);

    par.find(".overlay").remove();
    var start=null;
    var end = null;
    var totalSquaresBooked = 0;
    let oneIntBooking = false;

    for(var z = 0;z < amt+1;z++){
      var e = weekdays[currEvent.x+"o"][z];
      let i = z;
      if( e != null && e.hasOwnProperty("edited")){
        if(weekdays[currEvent.x+"o"][z+1] != null)
          editBookings(weekdays,e,1);
        if(weekdays[currEvent.x+"o"][z-1] != null)
          editBookings(weekdays,e,-1);
      }

      if(isDifferent(weekdays[currEvent.x+"o"][i-1],e)){

        if(totalSquaresBooked > 0 || oneIntBooking){
          end = weekdays[currEvent.x+"o"][i-1];

          configOverlay(start,end,totalSquaresBooked+1,par,currEvent.x,end.type);
          totalSquaresBooked = 0;
          end = null;
          start = null;
          oneIntBooking = false;
        }

        if(isDifferent(weekdays[currEvent.x+"o"][i-1],e) && isDifferent(weekdays[currEvent.x+"o"][i+1],e) && e !== undefined){
          oneIntBooking = true;
        }

        if(start == null){
          start = e;
        }
      }else{
        totalSquaresBooked += 1;
      }
    }
  currEvent = null;
  $("#ent").hide();
}

function recreateBooking(weekdays,is,x){
  for(var y = 0;y < is.length;y++){
    var e = weekdays[x+"o"][y];
    if(e != null && e.type == 1)
    $($(is).get(y)).addClass("pass-booked");
    else if(e != null && e.type == 2)
    $($(is).get(y)).addClass("u-booked");
    else if(e != null && e.type == 3)
    $($(is).get(y)).addClass("c-booked");
    else if(e != null && (e.type == 4 || e.type == 5))
    $($(is).get(y)).addClass("booked");
  }
}

function isDifferent(prev,curr){
  var diff = false;
  if(typeof curr === "undefined" || typeof prev === "undefined"){
    diff = true;
  }else if(prev.type != curr.type /*&& (prev.type != 5 && prev.type != 4)*/){
    if((prev.type == 5 || prev.type == 4) && (curr.type == 5 || curr.type == 4)){
      diff = false;
    }else{
      diff = true;
    }
  }
  return diff;
}

function convertDaysArrayToAssocArray(days){
  var day = new Date();
  var d = day.getDay();
  var i = [];
  for(var x = 0;x < days.length;x++){
    i[d.toString()+"o"] = days[x];
    d++;
    if(d > 6){
      d = 0;
    }
  }
  return i;
}

function getScheduledTimes() {
  return $.get( phpFile, { CLASS_ID: classId})
  .done(function( data ) {
    console.log("DATS",data);
    data = JSON.parse(data);
    var es = [];
    for(var i = 0;i < data.length;i++){
      var e = new Object();
      e.start = new Date(data[i].booking_start);
      e.end = new Date(data[i].booking_end);
      e.id = data[i].id;

      if(data[i].booker_id == userID){
        e.disabled = false;
        e.type=4;
      }
      else{
        e.disabled = true;
        e.type=2;
      }
      es.push(e);
    }
    disableTimesForBookings(es);
  });
}

function resetScheduler(){
  $(".booked").removeClass("booked");
  $(".u-booked").removeClass("u-booked");
  $(".c-booked").removeClass("c-booked");
  $(".overlay").remove();
}

function removeIdsFromBookings(weekdays,e){
  var i = e.y+1;
  if(typeof weekdays[e.x+"o"][i - 2] !== 'undefined'){
    while(typeof weekdays[e.x+"o"][i] !== 'undefined'){
      delete weekdays[e.x+"o"][i].id;
      weekdays[e.x+"o"][i].edited = true;
      i++;
    }
  }
}

function editBookings(weekdays,e,direction){
  var i = e.y+(1*direction);
    while(weekdays[e.x+"o"][i] != null && (weekdays[e.x+"o"][i].type == 4 || weekdays[e.x+"o"][i].type == 5)){
        weekdays[e.x+"o"][i].edited = true;
      i = i+(1*direction);
    }
}

armScheduler.prototype.getBookings = function () {
  var totalSquaresBooked = 0;
  var start = null;
  var end = null;
  var bookings = [];
  for(var w = 0;w < maxWeeks;w++){
    for(var x = 0;x < 7;x++){
      var id = null;
      var mergeID = [];
      for(var y = 0;y < amt+1;y++){
        var e = events[w][x][y];
        if((typeof e === "undefined" || (e.type != 5 && e.type != 4)) && totalSquaresBooked > 0){
          end = events[w][x][y-1];
          var b = new Object();

          b.start = start.start.toLocaleString();
          var add = ((60 / amtOfInts)*60000);
          if(y == amt){
            add = ((60 / amtOfInts)*59999);
          }

          var newEnd = new Date(end.start.getTime() + add);

          b.end = newEnd.toLocaleString();
          b.id = id;
          b.mergeID = mergeID;

          var delta = Math.round((newEnd.getTime() - start.start.getTime()) / 1000);
          console.log("D",delta,Math.floor(newEnd.getTime()),Math.floor(start.start.getTime()));

          var hours = Math.floor(delta / 3600);
          delta -= hours * 3600;

          var minutes = Math.floor(delta / 60);

          var duration = hours + (minutes != 0 ? (minutes / 60) : 0);

          console.log("M", duration, hours, minutes);
          b.duration = duration;
          b.totalSquaresBooked = totalSquaresBooked;
          bookings.push(b);
          id = null;
          totalSquaresBooked = 0;
          end = null;
          start = null;
        }else if(typeof e !== "undefined" && (e.type == 5 || e.type == 4) && e.hasOwnProperty("edited")){
          console.log(e);
          if(start == null){
            start = e;
          }

          if(e.hasOwnProperty('id')){
            if(id == null)
            id = e.id;
            else if(id != e.id && mergeID.indexOf(e.id) == -1)
            mergeID.push(e.id);
          }

          totalSquaresBooked += 1;
        }
      }
    }
  }
  return bookings;
};

  armScheduler.prototype.removeBookings = function () {
    var o = $(".booking");
    for(var i =0; i < o.length;i++){
      removeBooking($(o.get(i)));
    }
    // getDisabledTimes();
    // getTutorBookings();
    $.when(getDisabledTimes()).then(getTutorBookings);
  };

  armScheduler.prototype.getAmountOfIntervals = function () {
    return amtOfInts;
  };

  armScheduler.prototype.destroy = function () {
    $("."+container).empty();
  };
}
