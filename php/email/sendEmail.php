<?php
require '../vendor/autoload.php';

require('Mailin.php');
$apikey = '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';

putenv("SENDGRID=$apikey");


function sendEmailToTutorForBookings($email, $name, $bookings, $intervalCost) {
    // echo $email;
    $html = file_get_contents('../email/templates/booking-made-for-tutor.html');
    //echo $html;
    $text = "";
    $total = 0;
    foreach($bookings as $booking) {
        $pay = $booking["totalSquaresBooked"] * $intervalCost;
        $duration = $booking["duration"];

        $timeUnit = "hours";//$duration < 1 ? "minutes" : "hours";

        $text .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
         <tbody>
            <tr>
               <td class="Uber18_text_p1" valign="top" align="left" style="color: #2c3e50; font-family: "Uber18-text-Regular",
                    "HelveticaNeue-Light", "Helvetica Neue Light", Helvetica, Arial, sans-serif; font-size: 16px; line-height: 28px; padding-bottom: 5px; padding-right: 12px;
                    padding-top: 5px; direction: ltr; text-align: left;"> ' . $booking["start"] . ", " . $booking["end"] . ", " . $duration ." ". $timeUnit. '
                </td>
               <td class="Uber18_text_p1" valign="top" align="right" style="color: #2c3e50; font-family: "Uber18-text-Regular",
                    "HelveticaNeue-Light", "Helvetica Neue Light", Helvetica, Arial, sans-serif; font-size: 16px; line-height: 28px; padding-bottom: 5px; padding-right: 0px;
                        padding-top: 5px; text-align: right; direction: ltr;">
                        $' . $pay . '
                </td>
            </tr>
         </tbody>
        </table>';

        $total += $pay;
    }

    $html = str_replace("#COST#","$".$total,$html);
    $html = str_replace("#TUTOR_NAME#",$name, $html);
    $html = str_replace("#BOOKINGS#",$text,$html);

    sendEmail($email, $name/*"test-b1cx6@mail-tester.com"*/, "New Bookings Made!", $html);
}

function sendEmailToBookerForBookings($email, $name, $bookings, $intervalCost) {
    //echo $email;
    $html = file_get_contents('../email/templates/booking-made-for-user.html');
    //echo $html;
    $text = "";
    $total = 0;
    foreach($bookings as $booking) {
        $pay = $booking["totalSquaresBooked"] *$intervalCost;;
        $duration = $booking["duration"];

        $timeUnit = $duration < 1 ? "minutes" : "hours";

        $text .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
         <tbody>
            <tr>
               <td class="Uber18_text_p1" valign="top" align="left" style="color: #2c3e50; font-family: "Uber18-text-Regular",
                    "HelveticaNeue-Light", "Helvetica Neue Light", Helvetica, Arial, sans-serif; font-size: 16px; line-height: 28px; padding-bottom: 5px; padding-right: 12px;
                    padding-top: 5px; direction: ltr; text-align: left;"> ' . $booking["start"] . ", " . $booking["end"] . ", " . $duration ." ". $timeUnit. '
                </td>
               <td class="Uber18_text_p1" valign="top" align="right" style="color: #2c3e50; font-family: "Uber18-text-Regular",
                    "HelveticaNeue-Light", "Helvetica Neue Light", Helvetica, Arial, sans-serif; font-size: 16px; line-height: 28px; padding-bottom: 5px; padding-right: 0px;
                        padding-top: 5px; text-align: right; direction: ltr;">
                        $' . $pay . '
                </td>
            </tr>
         </tbody>
        </table>';

        $total += $pay;
    }

    $html = str_replace("#COST#","$".$total,$html);
    $html = str_replace("#BOOKER_NAME#",$name, $html);
    $html = str_replace("#BOOKINGS#",$text,$html);

    sendEmail($email, $name/*"test-b1cx6@mail-tester.com"*/, "New Bookings Made!", $html);
}

function sendConfirmationEmail($email, $name) {
    $html = file_get_contents('templates/confirm-email.html');

    sendEmail($name, $email/*"test-b1cx6@mail-tester.com"*/, "Confirm your email!", $html);
}

function sendWelcomeEmail($email, $name) {
    $html = file_get_contents('templates/welcome.html');

    sendEmail($name, $email/*"test-b1cx6@mail-tester.com"*/, "Welcome to Stutor!", $html);
}

function sendsupportEmail($email, $name, $subject) {
    $html = file_get_contents('../email/templates/contact-support.html');

    sendEmail($email, $name/*"test-b1cx6@mail-tester.com"*/, $subject, $html);
}

function sendEmailToUserForBoughtCrashCourse($email, $name, $crashCourse, $tutorName) {
    // echo $email;
    $html = file_get_contents('../email/templates/crash-course-bought-for-user.html');
    //echo $html;
    $text = "";

    $text .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
     <tbody>
        <tr>
           <td class="Uber18_text_p1" valign="top" align="left" style="color: #2c3e50; font-family: "Uber18-text-Regular",
                "HelveticaNeue-Light", "Helvetica Neue Light", Helvetica, Arial, sans-serif; font-size: 16px; line-height: 28px; padding-bottom: 5px; padding-right: 12px;
                padding-top: 5px; direction: ltr; text-align: left;"> ' . $crashCourse["NAME"] . ", " . $tutorName . '
            </td>
           <td class="Uber18_text_p1" valign="top" align="right" style="color: #2c3e50; font-family: "Uber18-text-Regular",
                "HelveticaNeue-Light", "Helvetica Neue Light", Helvetica, Arial, sans-serif; font-size: 16px; line-height: 28px; padding-bottom: 5px; padding-right: 0px;
                    padding-top: 5px; text-align: right; direction: ltr;">
                    $' . $crashCourse["COST"] . '
            </td>
        </tr>
     </tbody>
    </table>';



    $html = str_replace("#COST#","$".$crashCourse["COST"] ,$html);
    $html = str_replace("#TUTOR_NAME#",$name, $html);
    $html = str_replace("#BOOKINGS#",$text,$html);

    sendEmail($email, $name/*"test-b1cx6@mail-tester.com"*/, "Crash Course Bought!", $html);
}

function sendPasswordResetEmail($email, $name, $rand, $message) {
        $html = file_get_contents('../email/templates/account-password-reset.php');

        $html = str_replace("#MESSAGE#", $message ,$html);
        $html = str_replace("#RAND#", $rand, $html);

        sendEmail($email, $name/*"test-b1cx6@mail-tester.com"*/, "Password Reset", $html);
return 1;
}

function sendEmail($email,$name, $subject, $html) {

    $mailinThing = new Mailin("https://api.sendinblue.com/v2.0","1p38HfvnMsATkwWm");
    $data = array( "to" => array($email=>$name),
        "from" => array("admin@studychat.online", "StudyChat Support"),
        "subject" => $subject,
        "html" => $html//,
        //"attachment" => array("https://domain.com/path-to-file/filename1.pdf", "https://domain.com/path-to-file/filename2.jpg")
	);
    $obj = $mailinThing->send_email($data);
}

/*
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
 <tbody>
    <tr>
       <td class="Uber18_text_p1" valign="top" align="left" style="color: rgb(0, 0, 0); font-family: 'Uber18-text-Regular', 'HelveticaNeue-Light', 'Helvetica Neue Light', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 28px; padding-bottom: 5px; padding-right: 12px; padding-top: 5px; direction: ltr; text-align: left;"> Tolls, Surcharges, and Fees </td>
       <td class="Uber18_text_p1" valign="top" align="right" style="color: rgb(0, 0, 0); font-family: 'Uber18-text-Regular', 'HelveticaNeue-Light', 'Helvetica Neue Light', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 28px; padding-bottom: 5px; padding-right: 0px; padding-top: 5px; text-align: right; direction: ltr;"> #BOOKING_COST# </td>
    </tr>
 </tbody>
</table>
*/
?>
