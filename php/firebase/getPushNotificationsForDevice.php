<?php
require '../connect.php';

$device_id = 3;
$class_id = $_POST["CLASS_ID"];

$sql = "select distinct push_notification_id AS PUSH_NOTI_ID, class_name
        from app_push_token as APT
                join enrolled_students as ES on ES.account_id = APT.account_id
                join class as C on C.class_id = ES.class_id
        where
                app_id = 2 AND
                C.class_id = ? AND
                device_id = 3";

// if(isset($_POST["token"])) {
//         $tokens = $_POST["token"];
//         $sql .= " AND push_notification_id not in ('" . implode( "', '" , $tokens ) . "' )";
// }

//echo $sql;
$stmt = $conn->prepare($sql);

$stmt->bind_param("i",$class_id);

$stmt->execute();

$result = $stmt->get_result();

$tokens = [];

while ($row = $result->fetch_assoc()) {
        $tokens[] = $row;
}

$conn->close();

echo json_encode($tokens);
