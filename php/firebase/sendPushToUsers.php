<?php
require '../vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

require '../connect.php';
require 'firebase.php';

if(! file_exists(__DIR__.'/healthy-system-152300-firebase-adminsdk-uegrp-62b2f22cef.json')) {die('config absent.');}

$serviceAccount = ServiceAccount::fromJsonFile('healthy-system-152300-firebase-adminsdk-uegrp-62b2f22cef.json');

$firebase = (new Factory)
->withServiceAccount($serviceAccount)
->withDatabaseUri('https://healthy-system-152300.firebaseio.com')
->create();
$messaging = $firebase->getMessaging();


$app_id = 2;

$name = $_POST["name"];
$message = $_POST["message"];
$class_id = $_POST["class_id"];

$sql = "select distinct push_notification_id, class_name
        from app_push_token as APT
                join enrolled_students as ES on ES.account_id = APT.account_id
                join class as C on C.class_id = ES.class_id
        where
                app_id = 2 AND
                C.class_id = ?";

if(isset($_POST["token"])) {
        $tokens = $_POST["token"];
        $sql .= " AND push_notification_id not in ('" . implode( "', '" , $tokens ) . "' )";
}

//echo $sql;
$stmt = $conn->prepare($sql);

$stmt->bind_param("i",$class_id);

$stmt->execute();

$result = $stmt->get_result();

$firebase = new Firebase();

$body = $message;
$data = array("CLASS_ID"=>$class_id);

while ($row = $result->fetch_assoc()) {
        $className = $row["class_name"];
        $title = $name . " messaged ". $className;
        $notification = array("body" => $message, "title"=>$title);
        $token = $row["push_notification_id"];

        $message = CloudMessage::fromArray([
          'token' => $token,
          'notification' => $notification, // optional
          'data' => $data, // optional
        ]);

        try{
                $messaging->send($message);
        } catch(Exception $e){
                echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

}

$conn->close();

echo 1;
?>
