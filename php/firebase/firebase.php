<?php
require '../vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
/**
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class Firebase {

    // sending push message to single user by firebase reg id
    public function send($to, $content, $data) {
        return $this->sendPushNotification($to,$content, $data);
    }


    // Sending message to a topic by topic name
    public function sendToTopic($to, $message) {
        $fields = array(
            'to' => '/topics/' . $to,
            'data' => $message,
        );
        return $this->sendPushNotification($fields);
    }

    // sending push message to multiple users by firebase registration ids
    public function sendMultiple($registration_ids, $message, $data) {
        $fields = array(
            'registration_ids' => $registration_ids,
            'notification' => $message,

           'priority' => 'high',
           'data' => $data
        );

        return $this->sendPushNotification($fields);
    }

    // function makes curl request to firebase servers
    private function sendPushNotification($to, $content, $data) {

      if(! file_exists(__DIR__.'/healthy-system-152300-firebase-adminsdk-uegrp-62b2f22cef.json')) {die('config absent.');}

              // This assumes that you have placed the Firebase credentials in the same directory
              // as this PHP file.

              $serviceAccount = ServiceAccount::fromJsonFile('healthy-system-152300-firebase-adminsdk-uegrp-62b2f22cef.json');

        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        //  ->withServiceAccountAndApiKey($serviceAccount, $apiKey)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://healthy-system-152300.firebaseio.com')
        ->create();
        //echo "1 .<br>";
        $messaging = $firebase->getMessaging();

        $deviceToken = $to;
        //$notification = Notification::create("test", "body");
        // $data = [
        // 'first_key' => 'First Value',
        // 'second_key' => 'Second Value',
        // ];
        //echo "2 .<br>";
        /*$message = CloudMessage::withTarget('token', $deviceToken)
         ->withNotification($notification) // optional
         ->withData($data) // optional
        ;*/

        //echo "3 .<br>";
        $message = CloudMessage::fromArray([
          'token' => $deviceToken,
          'notification' => $content, // optional
          'data' => $data, // optional
        ]);

        print_r( $messaging->send($message));

         }
    }
?>
