<?php
require_once 'connect.php';

    $result = $conn->query("Select city_Name as CITY, CITY_ID FROM city ORDER BY city_NAME ASC");

    $cities = [];
    while ($row = $result->fetch_assoc()) {
        $cities[] = $row;
    }

    mysqli_close($conn);
    echo json_encode($cities);
?>
