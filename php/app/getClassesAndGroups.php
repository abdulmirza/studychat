<?php
require 'connect.php';
$stid = "";
$name ="";
// $result = 0;
$name = "";
$classarray =  array();
$grouparray = array();

$sql = 'select CLASS_ID,CLASS_NAME, CLASS_SECTION from class
        join enrolled_students as ES using (class_id)
        where ES.account_id = ? order by class_name';

$stmt = $conn->prepare($sql);

$account_id = $_GET["ACCID"];

$stmt->bind_param("i",$account_id);

$stmt->execute();

$result = $stmt->get_result();

while ($row = $result->fetch_assoc()) {
   // echo $row["CLASS_NAME"];
    $classarray[] = $row;

}

$conn->close();

echo json_encode($classarray);
?>
