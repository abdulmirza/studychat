<?php
require('connect.php');

 $sql = "select CLASS_NAME, CLASS_SECTION, CLASS_ID, IF(ENROLLED_CLASS_ID IS NOT NULL, 1, 0) IS_ENROLLED_IN_CLASS from class as C
left join (select class_id AS ENROLLED_CLASS_ID from enrolled_students where account_id = ?) AS ES ON ES.ENROLLED_CLASS_ID = C.class_id
where
	  C.school_id = ? AND C.CLASS_NAME NOT LIKE '%abdul%'
order by class_name";
//AND C.CLASS_NAME NOT LIKE '%abdul%'
$stmt = $conn->prepare($sql);

$account_id = $_GET["ACCID"] ?? 0;
$school_id = $_GET["SCHOOL_ID"];

$stmt->bind_param("ii",$account_id, $school_id);

$stmt->execute();

$result = $stmt->get_result();

$classarray =  array();
while ($row = $result->fetch_assoc()) {

     $classarray[] = $row;
//  echo $row['SCHOOL_NAME'];
}

$conn->close();

echo json_encode($classarray);
?>
