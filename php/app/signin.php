<?php

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_COOKIE["user"]) && isset($_COOKIE["pass"]) && isset($_COOKIE["loggedIn"])) {
		echo 1;
	} if ($_SERVER["REQUEST_METHOD"] == "POST"){
    require_once('connect.php');
    require_once('../enums/error.php');

  $sql = 'SELECT EMAIL, PASSWORD,ACCOUNT_ID,USERNAME,SESSION_ID
		  FROM accounts
		  WHERE EMAIL = ?';
  //print_r($_POST);
  $email = $_POST["email-login"];
  $p = $_POST["pass-login"];

  $sql = $conn->prepare($sql);

  $sql->bind_param("s", $email);

  $sql->execute();

  $result = $sql->get_result();

  $objResult = $result->fetch_assoc();

  if(!$objResult){
      echo json_encode(array("SUCCESS"=>"2","MESSAGE"=> "Account does not exist.","ERROR"=>ErrorCodes::inValidEmail));
      http_response_code(401);

      $conn->close();
      //echo $result;
			return;
  }

  $encrypted_pass = sha1($p);

  if(isset($_POST['remem'])){
      $time = time() + 60 * 60 * 24 * 7; // 1 week
  }else{
      $time = time() + 60 * 60;
  }

  if(password_verify($p, $objResult["PASSWORD"])){

    $sessid = $objResult["SESSION_ID"];
    $username = $objResult["USERNAME"];
//  }

    //print "logged in";
    $loggedIn = true;
    setcookie("user",$email,$time, "/");
    setcookie("loggedIn",$loggedIn,$time, "/");
    setcookie("name",$username,$time,"/");
    setcookie("sess_id",$sessid,$time,"/");
    setcookie("ACCID", $objResult["ACCOUNT_ID"], $time, "/");
    //setcookie("chatId",  $chatId, time(), "/");
  echo json_encode(array("SUCCESS"=>"1","ACCID" => $objResult["ACCOUNT_ID"], "NAME" => $objResult["USERNAME"]/*, "SACCOUNT"=> $objResult["STRIPE_ACCOUNT"]*/));
  } else {
		http_response_code(401);
    echo json_encode(array("SUCCESS"=>"2","MESSAGE"=> "Incorrect password.","ERROR"=>ErrorCodes::inValidPassword));
  }

  $conn->close();
}
?>
