<?php
require 'connect.php';

$sql = 'select FILE_ID, FILE_PATH AS PATH, FILE_NAME AS NAME,UPLOAD_DATE AS DATE,FILE_SIZE AS SIZE,USERNAME AS UPLOADER
        FROM uploaded_file AS UF
        join accounts as A on A.account_id = UF.UPLOADER_ID
        where class_id = ?';

$stmt = $conn->prepare($sql);

$class_id = $_GET["CLASS_ID"];

$stmt->bind_param("i",$class_id);

$stmt->execute();

$result = $stmt->get_result();

$files = array();
while ($row = $result->fetch_assoc()) {
   // echo $row["CLASS_NAME"];
    $files[] = $row;

}

$conn->close();

echo json_encode($files);
?>
