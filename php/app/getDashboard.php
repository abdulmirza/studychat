<?php
require('connect.php');

$json = array();

$account_id = $_GET["ACCID"] ?? 0;
$school_id = $_GET["SCHOOL_ID"];

$sql = "(select FILE_ID, FILE_PATH AS PATH, FILE_NAME AS NAME,UPLOAD_DATE AS DATE,FILE_SIZE AS SIZE,USERNAME AS UPLOADER, C.CLASS_NAME
         FROM uploaded_file as UF
         join accounts as A on A.account_id = UF.UPLOADER_ID
         join class as C on C.CLASS_ID = UF.CLASS_ID
         join school as S on S.SCHOOL_ID = C.SCHOOL_ID
         where C.CLASS_ID IN
         (
              select CLASS_ID from class
                 join enrolled_students as ES using (class_id)
                 where ES.account_id = ?
          )
        limit 30
)
UNION
(select FILE_ID, FILE_PATH AS PATH, FILE_NAME AS NAME,UPLOAD_DATE AS DATE,FILE_SIZE AS SIZE,USERNAME AS UPLOADER, C.CLASS_NAME
         FROM uploaded_file as UF
         join accounts as A on A.account_id = UF.UPLOADER_ID
         join class as C on C.CLASS_ID = UF.CLASS_ID
         join school as S on S.SCHOOL_ID = C.SCHOOL_ID
         where S.SCHOOL_ID = 24 AND C.CLASS_NAME NOT LIKE '%abdul%'
        limit ?
)
limit 30
";

$stmt = $conn->prepare($sql);

$stmt->bind_param("ii", $account_id, $school_id);

$stmt->execute();

$result = $stmt->get_result();

$docs = array();

while ($row = $result->fetch_assoc()) {

     $docs[] = $row;
//  echo $row['SCHOOL_NAME'];
}

$json["DOCS"] = $docs;

//print_r($json);

$sql = '(Select AD_ID,NAME_OF_AD,PRICE,TY,IMAGE,DESCRIPTION,PROMOTED,CONTACT
        from ads as A
        left join class as C on C.CLASS_ID = A.CLASS_ID
        join school as S on S.SCHOOL_ID = C.SCHOOL_ID
         where C.CLASS_ID IN
         (
              select CLASS_ID from class
                 join enrolled_students as ES using (class_id)
                 where ES.account_id = ?
          )
          limit 30
)
UNION
(Select AD_ID,NAME_OF_AD,PRICE,TY,IMAGE,DESCRIPTION,PROMOTED,CONTACT
        from ads as A
        join school as S on S.SCHOOL_ID = A.SCHOOL_ID
        WHERE S.SCHOOL_ID = ?
        LIMIt 30)
limit 30
';

$stmt = $conn->prepare($sql);

$stmt->bind_param("ii",$account_id, $school_id);

$stmt->execute();

$result = $stmt->get_result();

$books = array();

while ($row = $result->fetch_assoc()) {

     $books[] = $row;
//  echo $row['SCHOOL_NAME'];
}

$json["BOOKS"] = $books;

$sql = "(select
         account_id AS ID,
         IFNULL((SELECT Count(*) FROM favourited_tutor where tutor_id = account_id),0) FAV,
         IFNULL((SELECT Count(*) FROM tutoring_booking where tutor_id = account_id),0) BOOK,
         TUTOR_IMAGE,
         TUTOR_NAME,
         SOUNDBITE,
         IFNULL(PRICE,AMOUNT) PRICE,
         IFNULL(AVG(RATING),0) AS RATING,
         IFNULL(TEMPORARY_TUTOR,0) AS TEMPORARY_TUTOR
       from tutor_info TI
        left join tutor_rating TR ON TR.Tutor_ID = TI.Account_ID
        left join interval_cost using(COUNTRY)
        join tutor_class as TC on TC.TUTOR_ID = TI.Account_ID
        join class as C on C.CLASS_ID = TC.CLASS_ID
        join school as S on S.SCHOOL_ID = C.SCHOOL_ID
         where C.CLASS_ID IN
         (
              select CLASS_ID from class
                 join enrolled_students as ES using (class_id)
                 where ES.account_id = ?
          )
         AND AVAILABLE_NOW = 1 AND (STRIPE_ACCOUNT IS NOT NULL AND STRIPE_ACCOUNT <> '')
         group by account_id,tutor_image,tutor_name,soundbite,TEMPORARY_TUTOR
         LIMIT 30
)
UNION
(
select
         account_id AS ID,
         IFNULL((SELECT Count(*) FROM favourited_tutor where tutor_id = account_id),0) FAV,
         IFNULL((SELECT Count(*) FROM tutoring_booking where tutor_id = account_id),0) BOOK,
         TUTOR_IMAGE,
         TUTOR_NAME,
         SOUNDBITE,
         IFNULL(PRICE,AMOUNT) PRICE,
         IFNULL(AVG(RATING),0) AS RATING,
         IFNULL(TEMPORARY_TUTOR,0) AS TEMPORARY_TUTOR
       from tutor_info TI
        left join tutor_rating TR ON TR.Tutor_ID = TI.Account_ID
        left join interval_cost using(COUNTRY)
        join tutor_class as TC on TC.TUTOR_ID = TI.Account_ID
        join class as C on C.CLASS_ID = TC.CLASS_ID
        join school as S on S.SCHOOL_ID = C.SCHOOL_ID
         where S.SCHOOL_ID = ?
         AND AVAILABLE_NOW = 1 AND (STRIPE_ACCOUNT IS NOT NULL AND STRIPE_ACCOUNT <> '')
         group by account_id,tutor_image,tutor_name,soundbite,TEMPORARY_TUTOR
         LIMIT 30
)
limit 30;";

$stmt = $conn->prepare($sql);

$stmt->bind_param("ii", $account_id, $school_id);

$stmt->execute();

$result = $stmt->get_result();

$tutors = array();

while ($row = $result->fetch_assoc()) {

     $tutors[] = $row;
//  echo $row['SCHOOL_NAME'];
}

$json["TUTORS"] = $tutors;

$conn->close();

echo json_encode($json);
