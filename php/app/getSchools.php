<?php
require('connect.php');

	$result = $conn->query("SELECT SCHOOL_NAME,SCHOOL_ID, SUBURB_NAME AS CITY, ADDRESS, POSTAL FROM school
JOIN suburb using(SUBURB_ID)
ORDER BY SCHOOL_NAME ASC");

	$results = [];

    // output data of each row
    while($row = $result->fetch_assoc()) {
			$results[] = $row;
    }

  $conn->close();

  echo json_encode($results);

?>
