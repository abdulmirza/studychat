<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
require 'connect.php';
$reporter = $_POST["reporter"];
$offender = $_POST["user"];
$reason = $_POST["reason"];
$message = $_POST["message"];

       $sql = 'SELECT ACCOUNT_ID from accounts where username = ?';

       $stmt = $conn->prepare($sql);

       $stmt->bind_param("s", $offender);

       $stmt->execute();

       $result = $sql->get_result();

       $objResult = $result->fetch_assoc();

       $offender_id = $objResult["ACCOUNT_ID"];


       //insert into reported messages
        $sql = 'INSERT INTO reported_messages VALUES (?, ?, ?, ?)';

       $stmt = $conn->prepare($sql);

       $stmt->bind_param("s", $reporter, $offender_id, $message, $reason);

        $r =   $stmt->execute();  // executes and commits

       $sql = 'Update accounts set banned = 1 where account_id = ?';

        $stmt->bind_param("i", $offender_id);

        $r = $stmt->execute();  // executes and commits

        if ($r) {
            echo json_encode(array("SUCCESS"=>"1","MESSAGE"=>"Message reported"));
        }else{
            echo json_encode(array("SUCCESS"=>"2","MESSAGE"=>"Message has not been reported. Please contact support"));
        }


        $conn->close();
}


?>
