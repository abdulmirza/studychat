<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
   // $stid = oci_parse($con, 'INSERT INTO ARM.ACCOUNTS (Email,Password,DevTokenPush, UserName) VALUES (:E, :P, :D, :U)');
   if(isset($_POST["email"]) && isset($_POST["pass"]) && isset($_POST["username"])) {

       //if(strcmp(strcmp($_POST["password"],$_POST["passwordconfirm"]) === 0){
         require 'connect.php';
            $sql = 'INSERT INTO accounts (Email,Password, UserName) VALUES (?,?,?)';

            $stmt = $conn->prepare($sql);

		      $stmt->bind_param("sss", $email, $encrypted_pass, $u);

            $email = $_POST["email"];
            $pass = $_POST["pass"];
    //$dev = $_POST["devtp"];
            $u = $_POST["username"];

            $encrypted_pass = password_hash($pass, PASSWORD_DEFAULT);

            $r = $stmt->execute();   // executes and commits

            if ($r) {
                               $accid = $conn->insert_id;
              //  echo json_encode(array("SUCCESS"=>"1","MESSAGE"=>"Account successfully created. You can now login."));
              echo json_encode(array("ACCID" => $accid, "NAME" => $u/*, "SACCOUNT"=> $objResult["STRIPE_ACCOUNT"]*/));
            } else {
               //echo $conn->error;
               http_response_code(500);
               if (strpos($conn->error, 'Duplicate entry') !== false) {
                  echo json_encode(array("SUCCESS"=>"2","MESSAGE"=>"You already have an account associated to this email.", "ERROR" => 1));
               } else {
                  echo json_encode(array("SUCCESS"=>"2","MESSAGE"=>"Sorry we we're not able to process your request. Please try again.", "ERROR" => 2));
               }
            }

            $conn->close();

       //} else{
         // echo json_encode(array("SUCCESS"=>"2","MESSAGE"=>"Passwords do not match"));
       //}

   }else{
       echo json_encode(array("SUCCESS"=>"2","MESSAGE"=>"You haven't provided all of the required information", "ERROR" => 3));
   }

}
?>
