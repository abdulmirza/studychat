<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  require_once '../connect.php';
  $stid = "";
  $school = $city = $address = $postal = "";

  if(isset($_POST["cityI"]) && strlen($_POST["cityI"]) !== 0) {
        $suburb = $_POST["cityI"];
        $city = $_POST["city"];
        $school = $_POST["school"];
        $address = $_POST["address"];
        $postal = $_POST["postal"];

        $id = 0;

        if(strcmp($_POST["city"],0) == 0){
            $city = $suburb;

            //Insert City
            $sql = "INSERT INTO city (City_Name) VALUES (?)";

            $stmt = $conn->prepare($sql);

            $stmt->bind_param("s", $city);

            $stmt->execute();  // executes and commits

            $id = $conn->insert_id;
        } else {
          $id = $city;
        }

        //Insert Suburb
        $sql = "INSERT INTO suburb (Suburb_Name,Suburb_Of) VALUES (?, ?)";

        $stmt = $conn->prepare($sql);

        $stmt->bind_param("si", $suburb, $id);

        $stmt->execute();  // executes and commits

        //get susbrub id
        $id = $conn->insert_id;

        //Insert School
        $sql = "INSERT INTO school (School_Name, suburb_ID, Address, Postal) VALUES (?, ?, ?, ?)";

        $stmt = $conn->prepare($sql);

        $stmt->bind_param("siss", $school, $id, $address, $postal);

        $r = $stmt->execute();  // executes and commits

        if ($r) {
          echo json_encode(array("SUCCESS"=>"1","MESSAGE"=>"School successfully added. You can now select this school in settings"));
        }else{
            echo json_encode(array("SUCCESS"=>"2","MESSAGE"=>"Sorry we we're not able to process your request. Please try again."));
        }

  } else {
     $school = $_POST["school"];
     $city = $_POST["city"];
     $address = $_POST["address"];
     $postal = $_POST["postal"];

      $sql = "INSERT INTO school (school_name, suburb_id, Address, Postal) VALUES (?, ?, ?, ?)";

      $stmt = $conn->prepare($sql);

      $stmt->bind_param("siss", $school, $city, $address, $postal);

      $r = $stmt->execute();  // executes and commits

      if ($r) {
          echo json_encode(array("SUCCESS"=>"1","MESSAGE"=>"School successfully added. You can now select this school in settings"));
      }else{
          echo json_encode(array("SUCCESS"=>"2","MESSAGE"=>"Sorry we we're not able to process your request. Please try again."));
      }

  }

  mysqli_close($conn);
}
?>
