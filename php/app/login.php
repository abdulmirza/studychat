<?php
if (isset($_COOKIE["user"]) && isset($_COOKIE["pass"]) && isset($_COOKIE["loggedIn"])) {
		//echo 1;
        header("Location: StudyChat.php");
	}

$userCredentialsDontMatch= "none";
$showInputLeftBlank = "none";
$someThinghasGoneWrong = "none";
$accountMade = "none";
$wrongPassOrEmail = "none";
$duplicateAccount = "none";

if(isset($_GET["result"])){
    $result = $_GET["result"];
    if(strcmp($result , "2") == 0){
          $userCredentialsDontMatch= "block";
    }else if(strcmp($result , "1") == 0){
        $showInputLeftBlank = "block";
    }else if(strcmp($result , "3") == 0){
        $someThinghasGoneWrong = "block";
    }else if(strcmp($result , "4") == 0){
        $accountMade = "block";
    }else if(strcmp($result , "5") == 0){
        $wrongPassOrEmail = "block";
    }else if(strcmp($result , "6") == 0){
        $duplicateAccount = "block";
    }
}
?>
