<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
      require 'connect.php';
      require_once('../enums/error.php');

      $id = 0;

      $school_id = $_POST['SCHOOL_ID'];

      $sql = 'INSERT INTO class (SCHOOL_ID, Class_Name, CLass_Section) VALUES (?, ?, ?)';

      $classname = $_POST["CLASS_NAME"];
      $classsection = $_POST["CLASS_SECTION"] ?? "";

      $stmt = $conn->prepare($sql);

      $stmt->bind_param("iss", $school_id, $classname, $classsection);

      $result = 0;

      $r = $stmt->execute();

      $id = $conn->insert_id;

      if ($r) {
            echo json_encode(array("SUCCESS"=>"1","MESSAGE"=>"You've successfully added your class.", "ID" => $id));
      } else {
            http_response_code(401);
            if(strpos($conn->error, 'Duplicate entry') !== false){
                  echo json_encode(array("SUCCESS"=>"2","MESSAGE"=>"This classes has already been added. Please search again.","ERROR"=>ErrorCodes::courseRepeated));
            }else{
                  //echo json_encode(array("SUCCESS"=>"2","MESSAGE"=>"Something has gone wrong. Please contact support."));


                  echo json_encode(array("SUCCESS"=>"2","MESSAGE"=> "Something has gone wrong. Please contact support.","ERROR"=>ErrorCodes::dbError));
            }
      }

      $conn->close();

}


?>
