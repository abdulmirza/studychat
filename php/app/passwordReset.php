<?php
require '../vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$somethinghasgonewrong = "none";
$success = "none";

if ($_SERVER["REQUEST_METHOD"] == "POST"){
   require '../connect.php';

   $somethinghasgonewrong = "none";
   $successfully = "none";
   $codeNotProvided = "none";
   $passwordNotMatched="none";

   if(strcmp($_POST["code"],"") != 0) {
      $code = $_POST["code"];
      $email = $_POST["email"];
      $sql = 'SELECT PASSWORDRESET_CODE FROM accounts WHERE EMAIL = ? AND PASSWORDRESET_CODE = ?';

      $stmt = $conn->prepare($sql);

      $stmt->bind_param("ss", $email, $code);

      $stmt->execute();  // executes and commits

      $result = $stmt->get_result();

      $objResult = $result->fetch_assoc();

      if($objResult["PASSWORDRESET_CODE"] == $code){
         if(strcmp($_POST["pass"],$_POST["pass2"]) == 0){

            $pass = $_POST["pass"];
            $encrypted_pass = password_hash($pass, PASSWORD_DEFAULT);


            $sql = 'Update accounts set PASSWORDRESET_CODE = -1, PASSWORD= ? WHERE EMAIL = ? AND PASSWORDRESET_CODE = ?';

            $stmt = $conn->prepare($sql);

            $stmt->bind_param("ssi", $encrypted_pass, $email, $code);

            $r = $stmt->execute();  // executes and commits

            if($r){
                $success = "block";
            }else{
                $somethinghasgonewrong = "block";
            }

            $conn->close();
         } else {
            $passwordNotMatched = "block";
         }
      }else{
          $codeNotProvided = "block";
      }
   } else {
      $codeNotProvided = "block";
   }
$conn->close();
}

header("Location: ../../passwordReset.php?error=".$somethinghasgonewrong."&password=".$passwordNotMatched."&success=".$success."&code=".$codeNotProvided);
 ?>
