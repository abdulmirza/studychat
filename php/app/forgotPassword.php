<?php
$somethinghasgonewrong = "none";
$success = "none";
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    require '../connect.php';
    require_once('../email/sendEmail.php');

    $somethinghasgonewrong = "none";
    $successfully = "none";
    $email = $_POST["email"];
    $rand = rand ( 100, 100000);

    $message = "You have requested for your password to be reset. If you did not request for a password reset, there is no need to do anything";

    $subject = "StudyChat password reset";

    $success = sendPasswordResetEmail($email, "No name", $rand, $message);

    if ($success == 1) {

    $sql = 'Update accounts set PASSWORDRESET_CODE = ? WHERE EMAIL = ?';

    $stmt = $conn->prepare($sql);

    $stmt->bind_param("is", $rand, $email);

    $result = $stmt->execute();  // executes and commits

    if($result){
    		$success = "block";
    }else{
    		$somethinghasgonewrong = "block";
    }

	} else {
    $somethinghasgonewrong = "block";
  }

  $conn->close();
}

header("Location: ../../forgotPassword.php?error=".$somethinghasgonewrong."&success=".$success);
?>
