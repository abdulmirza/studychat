<?php
abstract class ErrorCodes /*extends BasicEnum*/ {
    const inValidEmail = 0;
    const inValidPassword = 1;

    const courseRepeated = 2;

    const dbError = 3;
}
