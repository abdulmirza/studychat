<?php
function enrollinClass($conn, $account_id, $class_id) {

  $sql = 'INSERT INTO enrolled_students (ACCOUNT_ID, CLASS_ID) VALUES (?, ?)';

  $stmt = $conn->prepare($sql);

  $stmt->bind_param("ii", $account_id, $class_id);

  $stmt->execute();
}
?>
