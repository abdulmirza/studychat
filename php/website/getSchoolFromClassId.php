<?php
require 'connect.php';

$sql = 'SELECT SCHOOL_ID FROM class
WHERE CLASS_ID = ?';

$stmt = $conn->prepare($sql);

$class_id = $_GET["CLASS_ID"];

$stmt->bind_param("i",$class_id);

$stmt->execute();

$result = $stmt->get_result();

$objResult = $result->fetch_assoc();

$returnData = array();
if($objResult == null){
  $returnData = array("RESULT"=>2,"MESSAGE"=>"School not found.");
} else {
  $returnData = array("RESULT"=>1,"MESSAGE"=>$objResult["SCHOOL_ID"]);
}

$conn->close();
echo json_encode($returnData);
?>
