<?php
require('connect.php');

$school_id = $_GET["SCHOOL_ID"];
$account_id = $_GET["ACCID"];

 $sql = "Select UPPER(CLASS_NAME) CLASS_NAME,CLASS_SECTION, CLASS_ID from class AS cl
                         JOIN school AS sch on cl.school_id = sch.school_id
                         where class_name not like '%abdul%' AND sch.school_id = ?
                         ORDER BY CLASS_NAME";

$stmt = $conn->prepare($sql);

$stmt->bind_param("i", $school_id);

$stmt->execute();

$result = $stmt->get_result();

$class = array();
 while ($row = $result->fetch_assoc()) {

    $class[] = $row;
 }

$conn->close();

echo json_encode($class);
?>
