<?php
require 'connect.php';

$accid = $_GET["CLASS_ID"];

$sql = "SELECT time_start as booking_start ,time_end as booking_end, booker_id as booker_id, booking_id as id, tutor_id
                                                                       FROM tutoring_booking AS TB
                                                                         JOIN booking_data AS BD USING(BOOKING_ID)
                                                                       WHERE (is_canceled = 0 AND Is_Partial_Cancel = 0)
                                                                        AND str_to_date(time_end,'%m/%d/%Y, %r') > str_to_date(?,'%m/%d/%Y, %r')
                                                                        AND TUTOR_ID = ?";

$stmt = $conn->prepare($sql);

$stmt->bind_param("si", $time, $accid);

$stmt->execute();

$result = $stmt->get_result();

$booking = [];
while ($rowForBookings = $result->fetch_assoc()) {
  $booking[] = $rowForBookings;
}

$conn->close();

echo json_encode($booking);
?>
