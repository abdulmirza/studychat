<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
      require 'connect.php';
      require 'enrollInClass.php';
      
      $school_id = $_COOKIE['SCHOOL_ID'] ?? $_POST["SCHOOL_ID"];
      $account_id = $_COOKIE["ACCID"] ?? $_POST["ACCID"];
      $class_name = $_POST["CLASS_NAME"];
      $class_section = $_POST["CLASS_SECTION"] ?? "";

    //  echo $school_id . " " . $account_id . " " . $class_name . " " . $class_section;

      setcookie("SCHOOL_ID",$school_id,time() + 60 * 60 * 7, '/') or die('unable to create cookie');;

      $sql =  'INSERT INTO class (SCHOOL_ID, Class_Name,CLass_Section) VALUES (?, ?, ?)';//'INSERT INTO class (SCHOOL_ID, Class_Name, CLass_Section) VALUES (?, ?, ?)';

      $stmt = $conn->prepare($sql);

      $stmt->bind_param("iss", $school_id, $class_name, $class_section);

      $result = 0;

      $r = $stmt->execute();

      if ($r) {
            $result = 2;

            enrollinClass($conn,$account_id,$conn->insert_id);

              echo json_encode(array("SUCCESS"=>"1","MESSAGE"=>"You've successfully added your class."));
      } else {
          echo $conn->error;
            if(strpos($conn->error, 'Duplicate entry') !== false){
                    echo json_encode(array("SUCCESS"=>"2","MESSAGE"=>"This classes has already been added. Please search again."));
            }else{
                  echo json_encode(array("SUCCESS"=>"2","MESSAGE"=>"Something has gone wrong. Please contact support."));
            }
      }
      $conn->close();
}

?>
