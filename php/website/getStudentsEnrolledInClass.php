<?php
require('connect.php');

$class_id = $_GET["CLASS_ID"];

 $sql = 'Select USERNAME from accounts AS AC
                         JOIN enrolled_students AS ENS on ENS.account_id = AC.account_id
                         where ENS.class_id = ?';

$stmt = $conn->prepare($sql);

$stmt->bind_param("i",$class_id);

$stmt->execute();

$result = $stmt->get_result();

$classes = array();
 while ($row = $result->fetch_assoc()) {

    $classes[] = $row;
 }

$conn->close();

echo json_encode($classes);
?>
