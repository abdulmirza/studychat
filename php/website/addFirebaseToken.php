<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
        require '../connect.php';

        $account_id = $_POST["ACCID"];
        $app_id = 2; // studychat id
        $device_id = 3;

        $pushToken = $_POST["TOKEN"];

        $sql = "SELECT 1 FROM app_push_token WHERE account_id = ? AND app_id = ? AND device_id = ?";

        $stmt = $conn->prepare($sql);

        $stmt->bind_param("iii", $account_id, $app_id, $device_id);

        $stmt->execute();

        $result = $stmt->get_result();

        $row = $result->fetch_assoc();

        if(!$row){
                  $sql = 'INSERT INTO app_push_token VALUES (?,?,?,?)';

                  $stmt = $conn->prepare($sql);

                  $stmt->bind_param("iisi", $account_id, $app_id, $pushToken, $device_id);

                  $r = $stmt->execute();

                  if ($r) {
                          echo json_encode(array("SUCCESS"=>"1","MESSAGE"=>"Push notification token successfully saved."));
                  } else {
                          		http_response_code(401);
                          echo json_encode(array("SUCCESS"=>"2","MESSAGE"=>"Push notification token was not successfully saved.", "ERROR"=>1));
                  }
        } else {
                $sql = "UPDATE app_push_token SET push_notification_id = ? WHERE account_id = ? AND app_id = ? AND device_id = ?";

                $stmt = $conn->prepare($sql);

                $stmt->bind_param("siii", $pushToken, $account_id, $app_id, $device_id);

                $r = $stmt->execute();

                if ($r) {
                        echo json_encode(array("SUCCESS"=>"1","MESSAGE"=>"Push notification token successfully saved."));
                } else {
                        		http_response_code(401);
                        echo json_encode(array("SUCCESS"=>"2","MESSAGE"=>"Push notification token was not successfully saved.","ERROR"=>1));
                }
        }

                          $conn->close();
}

?>
