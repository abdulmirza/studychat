<?php
require "connect.php";
require "enrollInClass.php";

$classes = array();

if(isset($_GET["CLASS"])) {
        $classes = $_GET["CLASS"];
} else if(isset($_POST["CLASS"])) {
        $classes = $_POST["CLASS"];
}

if($classes != null) {
        $account_id = $_COOKIE["ACCID"];

        try {
                if(count($classes) > 0) {
                        foreach((array) $classes as $class_id) {
                                enrollinClass($conn, $account_id, $class_id);
                                $showSuccessAutoEnrollAlert = "block";
                        }
                } else {
                        $showSuccessAutoEnrollAlert = "none";
                }
        } catch (Exception $e) {
                $showSuccessAutoEnrollAlert = "none";
        }
}
?>
