<?php

$show = "none";
if ($_SERVER["REQUEST_METHOD"] == "POST"){
  require_once('connect.php');

  $sql = 'SELECT EMAIL, PASSWORD,AC.ACCOUNT_ID,USERNAME,IF(PUSH_NOTIFICATION_ID IS NOT NULL, 1, 0) AS PUSH_NOTI_GRANTED
		  FROM accounts AC
          LEFT JOIN app_push_token APT ON APT.ACCOUNT_ID = AC.ACCOUNT_ID AND APT.DEVICE_ID = 3
		  WHERE EMAIL = ? OR USERNAME = ?';
  //print_r($_POST);
  $e = $_POST["email-login"];
  $p = $_POST["pass-login"];
  $classes = ["class"];

  $sql = $conn->prepare($sql);

  $sql->bind_param("ss", $e, $e);

  $sql->execute();

  $result = $sql->get_result();

  $objResult = $result->fetch_assoc();

  if(!$objResult){
    $result = "5";
    $conn->close();
    header("Location: ../../signin.php?result=".$result);
  }

  $encrypted_pass = sha1($p);

  if (isset($_POST['remem'])) {
      $time = time() + 60 * 60 * 24 * 7; // 1 week
  } else {
      $time = time() + 60 * 60 * 2; // 3 mins
  }

  if (password_verify($p, $objResult["PASSWORD"])) {
    $sessid = $objResult["SESSION_ID"];
    $username = $objResult["USERNAME"];
//  }

    //print "logged in";
    $loggedIn = true;
    setcookie("user",$e,$time, "/");
    setcookie("loggedIn",$loggedIn,$time, "/");
    setcookie("name",$username,$time,"/");
    setcookie("sess_id",$sessid,$time,"/");
    setcookie("ACCID", $objResult["ACCOUNT_ID"], $time, "/");
    setcookie("PUSH_NOTI_GRANTED", $objResult["PUSH_NOTI_GRANTED"], $time, "/");

    $showSuccessAutoEnrollAlert = "none";
    if(isset($_POST["CLASS"])) {
      $classes=$_POST["CLASS"];
      enrollinClass($conn,$classes,$objResult["ACCOUNT_ID"]);
      $showSuccessAutoEnrollAlert = "block";
    }
    //setcookie("chatId",  $chatId, time(), "/");
    //echo 1;
    header("Location: ../../studychat.php?AUTO_ENROLLED=".$showSuccessAutoEnrollAlert);
    //echo json_encode(array("ID" => $objResult["ACCOUNT_ID"], "NAME" => $objResult["NAME"]/*, "SACCOUNT"=> $objResult["STRIPE_ACCOUNT"]*/));
  } else {
    $show = "5";
    header("Location: ../../signin.php?AUTO_ENROLLED=".$showSuccessAutoEnrollAlert."&RESULT=" . $show);
  }

  $conn->close();
}

function enrollinClass($conn, $enrollArray, $account_id) {

  $sql = 'INSERT INTO enrolled_students (ACCOUNT_ID, CLASS_ID) VALUES (?, ?)';

  $stmt = $conn->prepare($sql);

  $stmt->bind_param("ii", $account_id, $id);

  foreach((array) $enrollArray as $id){
          $stmt->execute();
  }
}
?>
