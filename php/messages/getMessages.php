<?php

   require '../connect.php';

   $messages =  array();

   $sql = "
      (SELECT MESSAGE, TIME_RECEIVED, SENT_BY, MESSAGE_ID FROM message where class_ID = ? AND MESSAGE_ID > ?
    ORDER BY MESSAGE_ID desc  LIMIT 100) order by MESSAGE_ID
        ";

   $course_id = $_GET['COURSE_ID'];

   if(isset($_GET['LAST_MESSAGE_ID']))
      $last_message_id = $_GET['LAST_MESSAGE_ID'];
   else
      $last_message_id = -1;
   //$offset = $_POST["OFFSET"];


   $stmt = $conn->prepare($sql);
   //$session_id = $_POST['sess'];

   $stmt->bind_param("ii",$course_id, $last_message_id);

   $stmt->execute();

   $result = $stmt->get_result();

   //$i = 0;
   while ($row = $result->fetch_assoc()) {
        $messages[] = $row;
    }

    $conn->close();

    echo json_encode($messages);

?>
