<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
        require '../connect.php';
        $sql =  'INSERT INTO message (CLASS_ID, MESSAGE, TIME_RECEIVED, SENT_BY) VALUES (?, ?, ?, ?)';

        $stmt = $conn->prepare($sql);

        $dt = time();

        $class_id = $_POST["COURSE_ID"];
        $sent_by = $_POST["SENT_BY"];
        //$message = $_POST["message"];
        $time = date("Y-m-d h:i:sa",$dt);

        //$json = json_decode(,true);

        // if(isset($json["image"])){
        //         $img = $json["image"];
        //
        //         $img = str_replace('data:image/png;base64,', '', $img);
        //         $img = str_replace(' ', '+', $img);
        //         $data = base64_decode($img);
        //
        //         $file = 'Uploads/' . substr($json["message"],5). uniqid() . '.png';
        //         $success = file_put_contents('../../' . $file, $data);
        //
        //         $json["image"] = $file;
        // }

        $message = $_POST["MESSAGE"];
        //echo $time;

        $id = intval($class_id);

        $stmt->bind_param("issi", $class_id, $message, $time, $sent_by);

        $result = $stmt->execute();  // executes and commits



        if ($result) {
                $last_id = $conn->insert_id;
                $json = array();

                $json["SENT_BY"] = $sent_by;
                $json["MESSAGE_ID"] = $last_id;
                $json["MESSAGE"] = $message;
                $json["TIME_RECEIVED"] = $time;
                        $conn->close();
                echo json_encode($json);


        }else{
                                        $conn->close();
                http_response_code(400);


                echo json_encode(array("Error"=>"1","MESSAGE"=>"Message not saved"));
        }

}
?>
