<?php
$classes = $_GET["CLASS"] ?? array();

if (isset($_COOKIE["user"]) && isset($_COOKIE["loggedIn"]) && isset($_COOKIE["ACCID"])) {
		//echo 1;
    $queryString = "?";

    foreach((array) $classes as $id){
      $queryString .= "CLASS[]=" . $id;
    }

    header("Location: studychat.php". $queryString);
}

if(isset($_COOKIE["ACCID"])) {
          header("Location: studychat.php");
}

$userCredentialsDontMatch= "none";
$showInputLeftBlank = "none";
$someThinghasGoneWrong = "none";
$accountMade = "none";
$wrongPassOrEmail = "none";
$duplicateAccount = "none";

if(isset($_GET["result"])){

    $result = $_GET["result"];
    //echo "Result".$result;
    if(strcmp($result , "2") == 0){
          $userCredentialsDontMatch= "block";
    }else if(strcmp($result , "1") == 0){
        $showInputLeftBlank = "block";
    }else if(strcmp($result , "3") == 0){
        $someThinghasGoneWrong = "block";
    }else if(strcmp($result , "4") == 0){
        $accountMade = "block";
    }else if(strcmp($result , "5") == 0){
        $wrongPassOrEmail = "block";
    }else if(strcmp($result , "6") == 0){
          $duplicateAccount = "block";
      }
}

$user = "";
$email = "";

if(isset($_COOKIE["signup-user"])) {
  $user = $_COOKIE["signup-user"];
}

if(isset($_COOKIE["signup-email"])) {
  $email = $_COOKIE["signup-email"];
}

?>
<?php
  $cssFiles = array('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">',
    '<link href=css/cssv1001.css rel=stylesheet>',
    '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">');
  require "shared/header.php"
?>

<div class="container">
  <div class="row margin_top_10">
    <div class="col">
      <!-- <img src="css/img/studychat-itunes.png" style="width:100px;"/> -->
      <p class="studychat center_text">
        StudyChat
      </p>
    </div>
  </div>

  <div class="row margin_top_5">
    <div class="alert alert-warning">
          <strong>You may already have an account!</strong> If you've have an account on SellBooks that same account will work here! The same email and password will work.
          You can sign in <a href="signin.php">HERE</a>
    </div>

    <div class="alert alert-danger" style="display: <?php echo $userCredentialsDontMatch;?>"> <strong>Passwords dont match!</strong> Please make sure caps lock is off and passwords match.</div>
    <div class="alert alert-danger" style="display: <?php echo $showInputLeftBlank;?>"> <strong>1 or many boxes have been left blank!</strong> Please make sure you have typed your credentials in all of the boxes. Then retry. </div>
    <div class="alert alert-danger" style="display: <?php echo $someThinghasGoneWrong;?>"> <strong>Something has gone wrong!</strong> Please contact support <a href="support.php">Support</a> </div>
    <div class="alert alert-danger" style="display: <?php echo $duplicateAccount;?>">
          <strong>Duplicate Account!</strong> Please try signing in or reset your password. Do you have an account on SellBooks? Because we have a shared database with them.<a href="signin.php">Sign in</a> / <a href="passwordReset.php">Reset Password</a>
    </div>



    <div class="col-lg-6 col-12 d-none d-sm-none d-md-none d-lg-block">
        <?php echo file_get_contents("css/img/undraw_group_chat_v059.svg"); ?>
    </div>

    <div class="col-lg-3 col-md-12 col-12 offset-lg-3">
      <div class="row">
        <div class="col">
          <p class="header_text text_align_right"><?php echo $document[0]->nodeValue ?></p>
        </div>
      </div>

      <form action="php/website/signup.php" method="POST">
        <div class="row margin_top_5">
            <div class="col">
                <input type="text" placeholder="<?php echo $document[1]->nodeValue ?>" name="username" maxlength="30" class="input_text entry" value="<?php echo $user; ?>"/>
            </div>
        </div>
        <div class="row margin_top_5">
            <div class="col">
                <input type="email" placeholder="<?php echo $document[2]->nodeValue ?>" required name="email" maxlength="100" class="input_text entry" value="<?php echo $email?>"/>
            </div>
        </div>
        <div class="row margin_top_5">
            <div class="col">
                <input type="password" placeholder="<?php echo $document[3]->nodeValue ?>" required name="password" maxlength="100" class="input_text entry"/>
            </div>
        </div>
        <div class="row margin_top_5">
            <div class="col">
                <input type="password" placeholder="<?php echo $document[3]->nodeValue ?>" required name="passwordConfirm" maxlength="100" class="input_text entry"/>
            </div>
        </div>
        <?php
           foreach((array) $classes as $id){
              echo '<input type="hidden" name="CLASS[]" value="'.$id.'">';
           }
        ?>
          <!-- <div class="row margin_top_5">
              <div class="col-lg-12">
                <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
      <input type="checkbox" class="custom-control-input">
      <span class="custom-control-indicator"></span>
      <span class="custom-control-description">Remember my preference</span>
    </label>
              </div>
          </div> -->
        <div class="row margin_top_5">
            <div class="col-12 text_align_right">
                <input type="submit" value="Sign Up" class="btn btn-primary sign_btn"/>
            </div>
        </div>
      </form>
      <div class="row margin_top_5">
          <div class="col-12 text-md-right text-center">
              <a href="signin.php"> Already have an account? sign in!</a>
          </div>
      </div>
    </div>


  </div>
</div>
