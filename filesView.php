<?php
  $cssFiles = array('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">',
  '<link href=css/cssv1001.css rel=stylesheet>',
  '<link rel="stylesheet" type="text/css" href="custom-ui/UI-Transition-master/transition.min.css">',
  '<link rel="stylesheet" type="text/css" href="custom-ui/UI-Dropdown-master/dropdown.min.css">',
  '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">',
        '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" integrity="sha256-e47xOkXs1JXFbjjpoRr1/LhVcqSzRmGmPqsrUQeVs+g=" crossorigin="anonymous" />');
    require "shared/header.php";
?>

<div class="container-fluid">
  <div class="row" id="promoted_apps">
      <div class="col promoted_apps_border_left">
        <div class="row" style="height: 60px">
          <div class="col" style="display: flex;justify-content:center;align-content:center;flex-direction:column; " >
            <a class="promoted_apps_name padding margin_0 text-md-right text-center" href="https://sellbooks.today">SellBooks</a>
          </div>
          <div class="col padding" style="display: flex;justify-content:center;align-content:center;flex-direction:column; ">
            <p class="promoted_apps_text margin_0" >
              SellBooks - platform for selling your university/college books
            </p>
          </div>
        </div>
      </div>
      <div class="col promoted_apps_border_right">
        <div class="row" style="height: 60px">
          <div class="col" style="display: flex;justify-content:center;align-content:center;flex-direction:column; " >
            <a class="promoted_apps_name padding margin_0 text-md-right text-center" href="https://stutor.io">Stutor</a>
          </div>
          <div class="col padding" style="display: flex;justify-content:center;align-content:center;flex-direction:column; ">
            <p class="promoted_apps_text margin_0" >
              Stutor - platform for tutoring! Cheap and easy!
            </p>
          </div>
        </div>
      </div>
  </div>

    <h3 class="margin_top_2_5"> Welcome to StudyChat online document viewer!</h3>

    <p>
      Download as many documents as you want! Forever and ever free! make sure to check out the sidebar for your other classes!
    </p>
    <p>
      Make sure you join your respective classes group chat so you can easily communicate with your classmates!
    </p>

    <hr />
    <div class="loader"></div>
  <div class="row margin_top_2_5" style="height: calc(100% - 220px - 2.5%);">
    <div class="col-3" style="height: 100%">
    <div id="class_container" style="overflow: auto; height: 100%;">

    </div>
  </div>
    <div class="col-9"  >
      <h4 style="margin-left: 15px" id="class_name">Class Name</h4>
      <a class="btn btn-lg float_right d-none d-sm-none d-md-block" href="signin.php" title="Get the class share link">
        Go to StudyChat
      </a>
      <!-- <button class="btn btn-outline-primary float_right" data-toggle="modal" data-target="#uploadModal" title="upload a document to your class">
        <?php // echo file_get_contents("css/img/upload.svg"); ?>
      </button> -->

      <div class="dropdown float_right d-none d-sm-none d-md-block" style="margin-right: 2%;">
        <button class="btn btn-lg btn_main_view " id="dropdown-toggle" data-toggle="dropdown" title="Get the class share link">
          <?php echo file_get_contents("css/img/share.svg"); ?>
        </button>

        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <div class="row">
              <div class="col-11">
                  <input type="text" class="input_text" style="height: 32px" disabled id="share_link"/>
              </div>
              <div class="col-1 center_text">
                <?php echo file_get_contents("css/img/copy.svg"); ?>
              </div>
          </div>
        </div>
      </div>

      <p id="no_files" class="hide" style="padding: 2%">
        <span style='font-size:100px;'>&#128546;</span> No files have been uploaded yet. Please check again later! Or sign in and ask your classmates if they have any!
      </p>
      <div class="row" id="files_container" style="padding: 2%; width: 100%">

      </div>
    </div>
  </div>
</div>



<?php
  require "modals/upload.php";
  require "modals/selectSchoolModal.php";


  $jsFiles = array(
    '<script src="custom-ui/UI-Transition-master/transition.min.js"></script>',
    '<script src="custom-ui/UI-Dropdown-master/dropdown.min.js"></script>',
      '<script src="scripts/dropzone.js"></script>',
    '<script src=scripts/filesView.js></script>',
  );
  require "shared/footer.php";
?>
