<?php
  $cssFiles = array('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">',
    '<link href=css/cssv1001.css rel=stylesheet>',
    '<link rel="stylesheet" type="text/css" href="custom-ui/UI-Transition-master/transition.min.css">',
    '<link rel="stylesheet" type="text/css" href="custom-ui/UI-Dropdown-master/dropdown.min.css">',
    /*'<link href=css/chat-css.css rel=stylesheet>',*/
    '<link href=css/chats-dropdown.css rel=stylesheet>',
    '<link href=css/paint.css rel=stylesheet>',
    '<link href=sched/sched.css rel=stylesheet>',
    '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">',
    // '<link href=scripts/lightbox2-master/src/css/lightbox.css rel=stylesheet>',
    '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" integrity="sha256-e47xOkXs1JXFbjjpoRr1/LhVcqSzRmGmPqsrUQeVs+g=" crossorigin="anonymous" />');

    $showSuccessAutoEnrollAlert = $_GET["AUTO_ENROLLED"] ??"none";

    require "php/website/login.php";

    require "php/website/autoEnroll.php";
    require "shared/header.php";
?>


<div class="wrapper">
  <input type="hidden" value="<?php echo $_COOKIE["ACCID"];?>" id="accid"/>
    <!-- Sidebar  -->
    <nav id="sidebar">
        <div class="sidebar-header" id="sidebarCollapse">
            <button class="btn btn-lg btn_main_view float_left hamburger">
              <?php echo file_get_contents("css/img/sidebar_icons/back_icon.svg"); ?>
            </button>
          <button class="btn btn-lg btn_main_view float_left hide hamburger">
                          <img src="css/img/sidebar_icons/hamburger_menu.svg" />
                        </button>

            <p class="logo float_right">
              StudyChat
            </p>
        </div>

        <div class="sidebar_ul_container">
          <ul class="list-unstyled">
              <li class="active">
                  <a>
                      <?php echo file_get_contents("css/img/sidebar_icons/house.svg"); ?>
                      <span>Home</span>
                  </a>
              </li>
              <li>
                  <a href="#" data-toggle="modal" data-target="#settingsModal">
                        <?php echo file_get_contents("css/img/sidebar_icons/settings.svg"); ?>
                      <span>Settings</span>
                  </a>
              </li>
              <li>
                  <a href="#" id="join_chat">
                                              <?php echo file_get_contents("css/img/sidebar_icons/plus.svg"); ?>

                      <span>Join a Chat</span>
                  </a>
              </li>
              <li>
                  <a href="#" data-toggle="modal" data-target="#createChatModal">
                        <?php echo file_get_contents("css/img/sidebar_icons/pencil.svg"); ?>
                      <span>Create a chat</span>
                  </a>
              </li>
              <!-- <li>
                  <a href="#">
                        <?php // echo file_get_contents("css/img/sidebar_icons/faq.svg"); ?>
                      <span>FAQ</span>
                  </a>
              </li> -->
              <!-- <li>
                  <a href="#">
                        <?php // echo file_get_contents("css/img/sidebar_icons/envelope.svg"); ?>
                        <span>Contact Us</span>
                  </a>
              </li> -->
              <li>
                  <!-- <form action="php/website/logout.php">//
                        <?php //echo file_get_contents("css/img/sidebar_icons/logout.svg"); ?>
                      <input type="submit" value="Log Out" />
                  </form> -->
                  <a href="php/website/logout.php">
                    <?php echo file_get_contents("css/img/sidebar_icons/logout.svg"); ?>
                    <span>Log Out</span>
                  </a>
              </li>
          </ul>
        </div>


    </nav>

    <!-- Page Content  -->
    <div id="content">
      <div class="container-fluid">
        <div class="alert alert-success" role="alert" style="display: <?php echo $showSuccessAutoEnrollAlert?>">
          <strong>Class successfully created!</strong> You have Successfully created and enrolled into your class!
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="top_crap">
          <div class="row" id="promoted_apps">
              <div class="col promoted_apps_border_left">
                <div class="row" style="height: 60px">
                  <div class="col" style="display: flex;justify-content:center;align-content:center;flex-direction:column; " >
                    <a class="promoted_apps_name padding margin_0 text-md-right text-center" href="https://sellbooks.today">SellBooks</a>
                  </div>
                  <div class="col padding" style="display: flex;justify-content:center;align-content:center;flex-direction:column; ">
                    <p class="promoted_apps_text margin_0" >
                      SellBooks - platform for selling your university/college books
                    </p>
                  </div>
                </div>
              </div>
              <div class="col promoted_apps_border_right">
                <div class="row" style="height: 60px">
                  <div class="col" style="display: flex;justify-content:center;align-content:center;flex-direction:column; " >
                    <a class="promoted_apps_name padding margin_0 text-md-right text-center" href="https://stutor.io">Stutor</a>
                  </div>
                  <div class="col padding" style="display: flex;justify-content:center;align-content:center;flex-direction:column; ">
                    <p class="promoted_apps_text margin_0" >
                      Stutor - platform for tutoring! Cheap and easy!
                    </p>
                  </div>
                </div>
              </div>
          </div>

        <div class="row" id="main_btns_container">
          <div class="col">
            <div class="row" id="online_students">
              <!-- <div class="col-sm-4 col-md-1">
                <span id="selected_school">

                </span>
              </div> -->
              <div class="col-sm-4 col-md-2">
                <span id="num_of_students_online" class="num_of_students_online">
                    0
                </span>
                <span class="num_of_students_online">&nbsp people online</span>
              </div>
            </div>

            <div class="row">
              <div class="col-12 col-md-10">
                <p id="class_name">
                </p>
                <button id="change_chat" class="btn btn-info btn_solid_main_view pulse" data-toggle="modal" data-target="#changeChatModal" title="Connect to a class!"> change chat</button>
                <button class="btn btn-outline-primary d-md-none d-lg-none" style="margin-left:2%" data-toggle="modal" data-target="#uploadsModal">
                  <?php echo file_get_contents("css/img/upload.svg"); ?>
                </button>
                <a class="btn btn-lg btn_main_view float_right pulse d-md-none d-lg-none school_link" href="https://sellbooks.today" target="_blank" title="Search for books at your school!">
                  <?php echo file_get_contents("css/img/open-book.svg"); ?>
                </a>
                <!-- <button class="btn btn-lg btn_main_view float_right" id="dropdown-toggle" data-toggle="dropdown" title="Get the classes share link">
                  <?php //echo file_get_contents("css/img/share.svg"); ?>
                </button>

                <div class="dropdown-menu container rounded_corners">
                    <div class="row">
                        <div class="col-11">
                            <input type="text" class="input_text" style="height: 32px" disabled id="share_link"/>
                        </div>
                        <div class="col-1 center_text">
                          <?php //echo file_get_contents("css/img/copy.svg"); ?>
                        </div>
                    </div>
                </div> -->
              </div>
              <!-- <div class="col-md-1 d-none d-sm-none d-md-block">
                <button class="btn btn-lg btn_main_view float_right" data-toggle="modal" data-target="#scheduleModal">
                  <?php // echo file_get_contents("css/img/calendar_clock.svg"); ?>
                </button>
              </div> -->
              <div class="col-md-1 d-none d-sm-none d-md-block">
                <a class="btn btn-lg btn_main_view float_right pulse school_link" href="https://sellbooks.today" target="_blank" title="Search for books at your school!">
                  <?php echo file_get_contents("css/img/open-book.svg"); ?>
                </a>
              </div>
              <!-- <div class="col-md-1 d-none d-sm-none d-md-block">
                <button class="btn btn-lg btn_main_view float_right">
                  <?php //echo file_get_contents("css/img/student.svg"); ?>
                </button>
              </div> -->
              <div class="col-md-1 d-none d-sm-none d-md-block">
                <button class="btn btn-lg btn_main_view float_right" id="dropdown-toggle" data-toggle="dropdown" title="Get the classes share link">
                  <?php echo file_get_contents("css/img/share.svg"); ?>
                </button>

                <div class="dropdown-menu container rounded_corners">
                    <div class="row">
                        <div class="col-11">
                            <input type="text" class="input_text" style="height: 32px" disabled id="share_link"/>
                        </div>
                        <div class="col-1 center_text">
                          <?php echo file_get_contents("css/img/copy.svg"); ?>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


        <div class="row" id="chat_container">
          <div class="col-md-3 d-none d-sm-none d-md-block">
            <div class="row margin_right">
              <div class="col-8">
                <div class="center_vertical">
                  <p id="members_list">
                    List of members
                  </p>
                </div>

              </div>
              <div class="col-2 offset-md-2">
                <button class="btn btn-outline-primary" data-toggle="modal" data-target="#uploadsModal" title="upload a document to your class">
                <?php echo file_get_contents("css/img/upload.svg"); ?>
              </button>

              </div>
              <!-- <div class="col-2">
                <button class="btn btn_gold">
                <?php //echo file_get_contents("css/img/plus.svg"); ?>
              </button>
              </div> -->
            </div>
            <div class="row margin_top_10">
              <div id="members_holder" class="col">
              </div>
            </div>
          </div>

          <div class="col-md-9 col-sm-12" style="height: 100%;">
            <div class="alert alert-success alert-dismissible fade show" role="alert" id="alert_first_time">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong>Hey!</strong> We see that you haven't enrolled into a class yet! Would you like a quick run down on how to join a class? Click <a href="#" data-toggle="modal" data-target="#firsttimeModal">here</a> to continue
            </div>
            <div class="alert alert-warning alert-dismissible fade show" role="alert" id="alert_select_school">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong>Oh No!</strong> You haven't selected a school yet! Click <a href="#" data-toggle="modal" data-target="#selectSchoolModal">here</a> to continue
            </div>
            <div class="alert alert-warning alert-dismissible fade show" role="alert" id="alert_grant_notification_access">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong>Oh No!</strong> You haven't granted us permission to send you notifications! We use them to alert users like you to students messages or new course files like past exams!
              <a href="#" onclick="requestPermission()">Click here to give us permission!</a>
            </div>
            <div id="message_ui_container">
              <div id="message_header" class="row">
                <div class="col-1 padding">
                  <div class="center_vertical">
                    <img src="css/img/message_1_line.png"/>

                    <p>
                      Messages
                    </p>
                  </div>

                </div>

              </div>
              <div  id="message_holder">

                  <div id="messages" style="overflow: auto;height: 100%">

                  </div>

              </div>
              <div id="message_footer" class="row" style="padding: 1% 1.5%">
                <div class="col-10 padding">
                  <input type="text" placeholder="Type a message..." class="input_text"/>
                </div>
                <!-- <div class="col-1 padding center_text">
                  <button class="btn btn-lg btn_main_view center_text" data-toggle="modal" data-target="#freeHandDrawerModal">
                    <?php //echo file_get_contents("css/img/freehand_drawing2.svg"); ?>
                  </button>
                </div> -->
              <!--  <div class="col-1 padding">
                  <?php //echo file_get_contents("css/img/microphone.svg"); ?>
                </div> -->
                <div class="col-1 padding d-none d-sm-none d-md-block ">
                  <button class="btn btn-lg btn_main_view" data-toggle="modal" data-target="#uploadModal" id="append_file">
                  <?php echo file_get_contents("css/img/Attachment.svg"); ?>
                </button>
                </div>
                <!-- <div class="col-1 padding">
                  <?php //echo file_get_contents("css/img/Emoji.svg"); ?>
                </div> -->
                <div class="col-1 padding center_text">
                  <button class="btn btn-lg btn_main_view" id="send">
                    <?php echo file_get_contents("css/img/SEND.svg"); ?>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<?php
  require "modals/settings.php";
  require "modals/createChat.php";
  require "modals/addSchool.php";
  require "modals/joinChat.php";
  require "modals/freehanddrawer.php";
  require "modals/changeChat.php";
  require "modals/uploads.php";
  require "modals/upload.php";
  require "modals/firsttime.php";
  require "modals/selectSchoolModal.php";
  require "modals/mobile.php";
  require "modals/scheduleModal.php";

  $jsFiles = array('<script src="https://www.gstatic.com/firebasejs/6.6.0/firebase-app.js"></script>',
  '<script src="https://www.gstatic.com/firebasejs/6.6.0/firebase-messaging.js"></script>',
  '<script>
  var firebaseConfig = {
    apiKey: "AIzaSyCaUnut6s6ZfXbIiflldyciRNSwp5hAkeg",
    authDomain: "healthy-system-152300.firebaseapp.com",
    databaseURL: "https://healthy-system-152300.firebaseio.com",
    projectId: "healthy-system-152300",
    storageBucket: "healthy-system-152300.appspot.com",
    messagingSenderId: "349621773604",
    appId: "1:349621773604:web:181bc364ac6de8bb41ffb1"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
</script>',
  // '<script src="firebase-messaging-sw.js"></script>',
  '<script src=scripts/canvasbrush.js></script>',
  '<script src=scripts/lightbox2-master/src/js/lightbox.js></script>',
  '<script src=scripts/websocket-drawer.js></script>',
  '<script src=scripts/chat-object.js></script>',
  '<script src=scripts/display-image.js></script>',
  '<script src="custom-ui/UI-Transition-master/transition.min.js"></script>',
  '<script src="custom-ui/UI-Dropdown-master/dropdown.min.js"></script>',
  '<script src="sched/sched0.js"></script>',
  //'<script src="uploader/js/jquery.iframe-transport.js"></script>',
  //'<script src="uploader/js/jquery.fileupload.js"></script>',

  '<script src="scripts/dropzone.js"></script>',
    '<script src=scripts/jsv1002.js></script>',
  );
  require "shared/footer.php";
?>
