<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Class Chat - Login</title>
      <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
      <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
      <link rel="stylesheet" href="assets/css/form-elements.css">
      <link rel="stylesheet" href="assets/css/style.css">
      <link rel="stylesheet" href="css/allpages-css.css">
      <!--[if lt IE 9]> <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script> <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
      <link rel="shortcut icon" href="assets/ico/favicon.png">
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
      <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
   </head>
   <body>
      <div class="top-content">
         <div class="inner-bg">
            <div class="container">
               <div class="row">
                  <div class="col-sm-8 col-sm-offset-2 text">
                     <h1><strong>StudyChat</strong> Login &amp; Register Forms</h1>
                     <div class="description">
                        <p> Register or login now and start chatting with your classmates! </p>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-5">
                     <div>
                        <div class="form-box">
                           <div class="form-top">
                              <div class="form-top-left">
                                 <h3>Login to our site</h3>
                                 <p>Enter email and password to log on:</p>
                              </div>
                              <div class="form-top-right"> <i class="fa fa-lock"></i> </div>
                           </div>
                           <div class="form-bottom">
                              <form role="form" action="php/website/signin.php" method="post" class="login-form">
                                 <div class="form-group"> <label class="sr-only" for="form-username">Email</label> <input type="text" name="email-login" required placeholder="Email..." class="form-username form-control" id="form-username"> </div>
                                 <div class="form-group"> <label class="sr-only" for="form-password">Password</label> <input type="password" required name="pass-login" placeholder="Password..." class="form-password form-control" id="form-password"> </div>
                                 <?php
                                    if($class_id != -1) {
                                       echo '<input type="hidden" name="CLASS_ID" value="'.$class_id.'">';
                                    }
                                 ?>
                                 <button type="submit" class="btn">Sign in!</button>
                              </form>
                              <a href="forgotPassword.php" style="float:right">Forgot your password?</a>
                           </div>
                        </div>
                        <div class="alert alert-danger" style="display: <?php echo $wrongPassOrEmail;?>"> <strong>Wrong Email or Password!</strong> Please turn off caps lock and try again </div>
                        <div class="form-box">
                           <div class="form-top">
                              <div class="form-top-left">
                                 <h3>How can we Help?</h3>
                                 <p>What can StudyChat do for you?</p>
                              </div>
                           </div>
                           <div class="form-bottom">
                              <ul>
                                 <li> Never forget about homework or an exam ever again.</li>
                                 <li> Ask your classmates questions about questions you don't understand.</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-1 middle-border"></div>
                  <div class="col-sm-1"></div>
                  <div class="col-sm-5">
                     <div>
                        <div class="form-box">
                           <div class="form-top">
                              <div class="form-top-left">
                                 <h3>Sign up now</h3>
                                 <p>Fill in the form below to get instant access:</p>
                              </div>
                              <div class="form-top-right"> <i class="fa fa-pencil"></i> </div>
                           </div>
                           <div class="form-bottom">
                              <form role="form" action="php/website/signup.php" method="post" class="registration-form">
                                 <div class="form-group"> <label class="sr-only" for="form-first-name">Username</label> <input type="text" required name="username" placeholder="Username" class="form-first-name form-control" id="form-first-name"> </div>
                                 <div class="form-group"> <label class="sr-only" for="form-email-1">Email</label> <input type="text" required name="email" placeholder="Email" class="form-email form-control"> </div>
                                 <div class="form-group"> <label class="sr-only" for="form-email-2">Email</label> <input type="text" required name="emailconfirm" placeholder="Confirm Email" class="form-email form-control"> </div>
                                 <div class="form-group"> <label class="sr-only" for="form-pass-1">Password</label> <input type="password" required name="password" placeholder="Password" class="form-email form-control"> </div>
                                 <div class="form-group"> <label class="sr-only" for="form-pass-2">Password</label> <input type="password" required name="passwordconfirm" placeholder="Confirm Passsword" class="form-email form-control"> </div>
                                 <?php
                                    if($class_id != -1) {
                                       echo '<input type="hidden" name="CLASS_ID" value="'.$class_id.'">';
                                    }
                                 ?>
                                 <button type="submit" class="btn">Sign me up!</button>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <footer>
         <div class="container">
            <div class="row">
               <div class="col-sm-8 col-sm-offset-2">
                  <div class="footer-border"></div>
                  <p>Having Difficulties? <a href="support.php"><strong>Email Us</strong></a> <i class="fa fa-smile-o"></i></p>
               </div>
            </div>
         </div>
      </footer>
      <script src="https://code.jquery.com/jquery-3.1.1.min.js"
         integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
         crossorigin="anonymous"></script><script src="assets/bootstrap/js/bootstrap.min.js"></script><!--[if lt IE 10]> <script src="assets/js/placeholder.js"></script><![endif]-->
   </body>
</html>
