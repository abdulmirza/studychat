<?php
$class_id = $_GET["CLASS_ID"] ?? -1;
if ($class_id != -1) {
        header("Location: login.php?CLASS_ID=" . $class_id);
}

?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>StudyChat - Landing Page</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="css/allpages-css.css" rel="stylesheet">
    <link href="css/landing-page.css" rel="stylesheet">

    <link rel="icon" href="favicon2.png" type="image/png" sizes="16x16">
  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
        <a class="navbar-brand" href="#">StudyChat</a>
        <a class="btn btn-primary" href="login.php" target="_blank">Launch Web App</a>
      </div>
    </nav>
    <!-- Masthead -->
    <header class="masthead text-white text-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h1 class="mb-5">Communicate with your classmates!</h1>
          </div>
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
            <form>
              <div class="form-row">
                <div class="col-12 col-md-12 mb-2 mb-md-0">
                  <h5>A direct communication line with your whole class! Find answers to your questions easily!</h5>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </header>
    <!-- <header class="masthead text-white text-center" style="padding:2%">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
            <img src="img/StudyChatBanner.png"/>
          </div>
        </div>
      </div>
    </header> -->

    <!-- Icons Grid -->
    <section class="features-icons bg-light text-center">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-screen-desktop m-auto text-primary"><img src="img/becomeatutor.png"/></i>
              </div>
              <h3>Ask Questions</h3>
              <p class="lead mb-0">Have a question? Ask your class for an immediate answer instead of waiting for the teacher.</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-layers m-auto text-primary"><img src="img/needhelp.png"/></i>
              </div>
              <h3>Missed a class?</h3>
              <p class="lead mb-0">Ask someone about what was cnvered in class!</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-check m-auto text-primary"><img src="img/quickbookings.png"/></i>
              </div>
              <h3>Study Groups</h3>
              <p class="lead mb-0">Organize study groups to help each other learn!</p>
            </div>
          </div>
        </div>
      </div>
    </section>

	<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#tutee" role="tab">Create a class</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="tutee" role="tabpanel">    <!-- Image Showcases -->
    <section class="showcase">
      <div class="container-fluid p-0">
        <div class="row no-gutters">

          <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-color:#f0f3f6"><img src="img/SelectSchool.PNG" width="50%" height="40%" style="margin:auto;display:block;margin-top:10%;"></div>
          <div class="col-lg-6 order-lg-1 my-auto showcase-text">
            <h2>Select your school</h2>
            <p class="lead mb-0">Start by selecting your school</p>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-lg-6 text-white showcase-img" style="background-color:#f0f3f6"><img src="img/CreateClass.PNG" width="70%" height="60%" style="margin:auto;display:block;margin-top:20%;"></div>
          <div class="col-lg-6 my-auto showcase-text">
            <h2>Create a chat</h2>
            <p class="lead mb-0">Create the class by simply giving us the required info!</p>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-color:#f0f3f6"><img src="img/ViewShareLink.PNG" width="70%" height="40%" style="margin:auto;display:block;margin-top:20%;"></div>
          <div class="col-lg-6 order-lg-1 my-auto showcase-text">
            <h2>Communicate</h2>
            <p class="lead mb-0">Ask your class to join! </p>
          </div>
        </div>
      </div>
    </section>
	</div>
</div>

    <!-- Call to Action -->
    <section class="call-to-action text-white text-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h2 class="mb-4">What are you waiting for? Let's get Started!</h2>
          </div>
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
            <form>
              <div class="form-row">
                <div class="col-12 col-md-12 mb-2 mb-md-0">
                  <a class="btn btn-primary" href="studychat.php" target="_blank">Launch Web App</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a href="#">About</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="#">Contact</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="privacypolicy.html">StudyChat Policy</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">&copy; StudyChat <?php echo date('Y')?>. All Rights Reserved.</p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="#">
                  <i class="fa fa-facebook fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item mr-3">
                <a href="#">
                  <i class="fa fa-twitter fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-instagram fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
  </body>

</html>
