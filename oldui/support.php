<?php
$name = "";
$email = "";
$message = "";
$subject  = "";
$human = 0;
$errEmail = "";
$errHuman = "";
$errMessage = "";
$errName = "";
$errSubject = "";
$result = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){

    if (!$_POST['name']) {
	    $errName = 'Please enter your name';
    }

    if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
	    $errEmail = 'Please enter a valid email address';
    }

    //Check if message has been entered
	if (!$_POST['message']) {
			$errMessage = 'Please enter your message';
    }

       //Check if message has been entered
	if (!$_POST['subject']) {
			$errSubject = 'Please enter a subject';
    }




     $name = $_POST['name'];
		$email = $_POST['email'];
		$message = $_POST['message'];
		$human = intval($_POST['human']);
        $subject = $_POST['subject'];
		$from = 'From: ' . $email;

		$to = 'abdul.rahman.mirza@hotmail.com';
		//$subject = 'Message from Contact Demo ';

		$body = "From: $name\n E-Mail: $email\n Message:\n $message";

            if ($human !== 5) {
	    $errHuman = 'Your anti-spam is incorrect';
    }
// If there are no errors, send the email
if (!$errName && !$errEmail && !$errMessage && !$errHuman && !$errSubject) {
	if (mail ($to, $subject, $body, $from)) {
		$result='<div class="alert alert-success">Thank You! We will be in touch</div>';
	} else {
		$result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later</div>';
	}
}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>StudyChat - Support</title>
          <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <link href="StyleSheet.css" rel="stylesheet">

        <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <script src="jquery-1.12.4.min.js"></script>
           <script src="scripts/stickyFooter.js"></script>
          <link href="css/footer.css" rel="stylesheet">
                <style type="text/css">
            button:hover{
                background-color: rgba(0,0,0,1);
            }

            .f{
                        margin-top: 1%;
                        border-radius: 5px;
                        padding: 2%;
                        border: 1px solid #1A4A7C;
                        background-color: White;
                        margin-bottom: 1%;
                    }
                              .adlabel{
                float: left;
                background-color: #1A4A7C;
            }

        </style>
    </head>
    <body>

         <div class="container">
             <div class="row">
             <div class="col-lg-6 col-lg-offset-1 f"><form class="form-horizontal" role="form" method="post" >
	<div class="form-group">
         <span class="label label-success adlabel">1</span>
                            <p style="float: left">&nbsp Support</p>
                            <p style="float: right" >*Mandatory Fields</p>
                            <hr class="title-divider">
		<label for="name" class="col-sm-2 control-label">Name</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="name" name="name" placeholder="First & Last Name" value="<?php echo htmlspecialchars($name); ?>">
			<?php echo "<p class='text-danger'>$errName</p>";?>
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-sm-2 control-label">Email</label>
		<div class="col-sm-10">
			<input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="<?php echo htmlspecialchars($email); ?>">
			<?php echo "<p class='text-danger'>$errEmail</p>";?>
		</div>
	</div>
                 <div class="form-group">
		<label for="subject" class="col-sm-2 control-label">Subject</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" value="<?php echo htmlspecialchars($subject); ?>">
			<?php echo "<p class='text-danger'>$errSubject</p>";?>
		</div>
	</div>
	<div class="form-group">
		<label for="message" class="col-sm-2 control-label">Message</label>
		<div class="col-sm-10">
			<textarea class="form-control" rows="4" name="message"><?php echo htmlspecialchars($message);?></textarea>
			<?php echo "<p class='text-danger'>$errMessage</p>";?>
		</div>
	</div>
	<div class="form-group">
		<label for="human" class="col-sm-2 control-label">2 + 3 = ?</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="human" name="human" placeholder="Your Answer">
			<?php echo "<p class='text-danger'>$errHuman</p>";?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-10 col-sm-offset-2">
			<input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-10 col-sm-offset-2">
			<?php echo $result; ?>
		</div>
	</div>
</form>
            </div>
              <div class="col-lg-4 " style="border: 1px solid silver;padding: 20px;border-radius: 5px;margin-top: 1%;margin-left: 2%">
                 <h4 class="form-signin-heading"><b>Support</b></h4>
                <hr>
                <p>Have you extensivly read our FAQ?</p>
                <p>Heres a quick checklist to help you resolve any errors that may occur</p>
                <p>Are you currently Signed In?</p>
                <p> Have you selected the school you go to? (If your school is not in the dropdown list <a href="addSchool.php">click here</a> to add it)</p>
                <p>If you have gone through the checklist and want to read the FAQ again, please click the link below.</p>
                <a href="FAQ.html"><button class="btn btn-lg btn-info btn-block">Check out FAQ</button></a>

            </div>
             </div>
             </div>
      <footer class="footer" id="footer" >
            <div style="width: 90%;margin: auto;background-color: #EEEEEE">

            <div class="col-lg-4">
                   <h5>StudyChat</h5>
          <span><a href="about.html">Want to know more about us? Click this link</a></span>

            </div>

            <div class="col-lg-4">
                <h5>StudyChat Support</h5>
          <span>Has Something Gone wrong? Our FAQ page didnt help? Have a suggestion or see something we can improve? <a href="support.php">Email Us</a></span>

            </div>

            <div class="col-lg-4">
                <h5>Frequently Asked Questions</h5>
           <span>Got Any Questions? Check out our <a href="FAQ.html">FAQ Page</a></span>

            </div>
              <hr>
                  <div class="col-lg-14">
                      <a href="https://play.google.com/store/apps/details?id=com.mirza.abdul.StudyChat" target=_blank><img src="google-play-badge.png" alt="google play badge" /></a>
                      <a href="https://itunes.apple.com/us/app/studychat-chat-classmates/id1144788978?ls=1&mt=8" target=_blank><img src="appstore_badge.svg" alt="google play badge" /></a>

            </div>
                </div>
        </footer>
    </body>
</html>
