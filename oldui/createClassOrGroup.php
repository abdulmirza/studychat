<?php

$someThinghasGoneWrong = "none";
$classMade = "none";
$already = "none";
if(isset($_GET["result"])){
   //     $userCredentialsDontMatch= "none";
//$showInputLeftBlank = "none";
//$someThinghasGoneWrong = "none";
//$accountMade = "none";
    $result = $_GET["result"];
    //echo "Result".$result;
    if(strcmp($result , "1") == 0){
        $someThinghasGoneWrong = "block";
    }else if(strcmp($result , "2") == 0){
        $classMade = "block";
    }else if(strcmp($result , "3") == 0){
        $already = "block";
    }
}
?>
<!DOCTYPE html>
<html lang=en>
<meta charset=utf-8>
<meta content="IE=edge" http-equiv=X-UA-Compatible>
<meta content="width=device-width,initial-scale=1" name=viewport>
<title>Class Chat - Create a Class</title>
<link href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500" rel=stylesheet>
<link href=bootstrap-3.3.6-dist/css/bootstrap.min.css rel=stylesheet>
<link href=assets/font-awesome/css/font-awesome.min.css rel=stylesheet>
<link href=assets/css/form-elements.css rel=stylesheet>
<link href=assets/css/style.css rel=stylesheet>
<!--[if lt IE 9]><script src=https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js></script><script src=https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js></script><![endif]-->
<link href=assets/ico/favicon.png rel="shortcut icon">
<link href=assets/ico/apple-touch-icon-144-precomposed.png rel=apple-touch-icon-precomposed sizes=144x144>
<link href=assets/ico/apple-touch-icon-114-precomposed.png rel=apple-touch-icon-precomposed sizes=114x114>
<link href=assets/ico/apple-touch-icon-72-precomposed.png rel=apple-touch-icon-precomposed sizes=72x72>
<link href=assets/ico/apple-touch-icon-57-precomposed.png rel=apple-touch-icon-precomposed>
<div class=top-content>
    <div class=inner-bg>
        <div class=container>
            <div class=row>
                <div class="col-sm-8 col-sm-offset-2 text">
                    <h1><strong>StudyChat</strong> Create Chat Forms</h1>
                    <div class=description>
                        <p>Create a chat so that you can chat with your classmates</div>
                </div>
            </div>
            <!--<div>
                <div class=form-box>
                    <div class=form-top>
                        <div class=form-top-left>
                            <h3>What school do you go to?</h3>
                            <p>Please select the school you currently attend.</div>
                        <div class=form-top-right><i class="fa fa-pencil"></i></div>
                    </div>
                    <div class=form-bottom>
                        <form action="selectschool2.php" class="login-form" method="POST" role="form">
                            <select class=form-control name=school>
                                <?php
                                // require("php/website/getSchools.php");
                             ?>
                            </select>
                            <button class=btn type=submit style=margin-top:1%>Submit!</button>
                        </form>
                    </div>
                </div>
            </div>-->
            <div class=row>
                <div class=col-sm-5>
                    <div>
                        <div class=form-box>
                            <div class="alert alert-danger" style=display:<?php echo $someThinghasGoneWrong;?>><strong>Something has gone wrong!</strong> Please try again, if this problem persists please contact support</div>
                            <div class="alert alert-success" style=display:<?php echo $classMade;?>><strong>Chat created successfully!</strong> You are now enrolled into it and can now share the link with other classmates.</div>
                            <div class="alert alert-warning" style=display:<?php echo $already;?>><strong>Chat already created!</strong> Please go back to settings and carefully look for it</div>
                            <div class=form-top>
                                <div class=form-top-left>
                                    <h3>Add a Chat</h3>
                                    <p>Please enter the relevant information:</div>
                                <div class=form-top-right><i class="fa fa-pencil"></i></div>
                            </div>
                            <div class=form-bottom>
                                <form action="php/website/createClass.php" class="login-form" method="post" role="form">
                                    <div class=form-group>
                                    <select class=form-control name=school>
                                        <?php
                                        require("php/website/getSchools.php");
                                        ?>
                                    </select>
                                    </div>
                                    <div class=form-group>
                                        <label class=sr-only for=form-username>Chat Name</label>
                                        <input class="form-control form-username" id=form-username name=classname placeholder="Class Name, Club Name, etc...">
                                    </div>
                                    <!--<div class=form-group>
                                        <label class=sr-only for=form-password>Class Section</label>
                                        <input class="form-control form-password" id=form-password name=classsection placeholder="Class Section...">
                                    </div>-->
                                    <button class=btn type=submit>Submit!</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1 middle-border"></div>
                <div class=col-sm-1></div>
                <div class=col-sm-5>
                    <div>
                        <div class=form-box>
                            <div class=form-top>
                                <div class=form-top-left>
                                    <h3>Create a Group</h3>
                                    <p><i>Coming Soon</i></div>
                                <div class=form-top-right><i class="fa fa-pencil"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href=studychat.php>
                <button class="btn btn-primary">Go back to StudyChat</button>
            </a>
        </div>
    </div>
</div>
<footer>
    <div class=container>
        <div class=row>
            <div class="col-sm-8 col-sm-offset-2">
                <div class=footer-border></div>
                <p>Having Difficulties? <a href=http://azmind.com target=_blank><strong>Email Us</strong></a> <i class="fa fa-smile-o"></i></div>
        </div>
    </div>
</footer>
<script src=assets/js/jquery-1.11.1.min.js></script>
<script src=assets/bootstrap/js/bootstrap.min.js></script>
<script src=assets/js/jquery.backstretch.min.js></script>
<script src=assets/js/scripts.js></script>
<!--[if lt IE 10]><script src=assets/js/placeholder.js></script><![endif]-->
