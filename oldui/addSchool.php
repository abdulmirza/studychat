<?php
$somethinghasgonewrong = $_GET["error"] ?? "none";
$successfullyPosted = $_GET["posted"] ?? "none";

?>
<!DOCTYPE html>
<html lang=en>
<meta charset=utf-8>
<title>SellBooks - Add School</title>
<meta content="width=device-width,initial-scale=1" name=viewport>
<link href=bootstrap-3.3.6-dist/css/bootstrap.min.css rel=stylesheet>
<script src=https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js></script>
<link href=StyleSheet.css rel=stylesheet>
<script src=bootstrap-3.3.6-dist/js/bootstrap.min.js></script>
<link href=//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css rel=stylesheet>
<script src=jquery-1.12.4.min.js></script>
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel=stylesheet>
<link href=style.css rel=stylesheet>
<link href=css/allpages-css.css rel=stylesheet>
<link href=css/footer.css rel=stylesheet>
<link href=css/addschool-css.css rel=stylesheet>
<style>
    .adlabel {
        float: left;
        background-color: #1A4A7C
    }
</style>
<script>
    $(document).ready(function() {
        $("#idontsee").click(function() {
            $("select option:first").before($("<option selected></option>").val("0").text("Suburb of None")), $("#city_textbox").show(), $("#city_p_tag").show(), $(this).hide()
        })
    })
</script>
<a href=StudyChat.php>
    <button class="btn btn-primary">Go back to StudyChat</button>
</a>
<div class="container">

     <div class="alert alert-danger" style="display: <?php echo $somethinghasgonewrong;?>">
        <strong>Something has gone wrong!</strong> Please contact support <a href="support.php">Support</a>
    </div>
    <div class="alert alert-success" style="display: <?php echo $successfullyPosted;?>">
        <strong>Your School has been successfully added!</strong> <br>You can now select your school and buy/sell on it.
    </div>

    <div style="width: 80%;margin: auto">
        <h4>Add your school</h4>
        <hr >
        <div style="background-color: #DADFE1;border-radius: 5px;overflow: hidden">

            <form method="POST" class="text-center" action="php/website/addSchool.php">
                <div style="margin:2% 2% 2% 2%;background-color: white;border-radius: 5px;overflow: hidden;padding: 2%;">

                    <span class="label label-success adlabel">1</span>
                    <p style="float: left">School Details</p>
                    <p style="float: right" >*Mandatory Fields</p>
                    <hr class="title-divider">

                        <div class="textboxDiv"><p class="p">*School Name:</p>  <input type="text" name="school" required maxlength="50"></div>
                        <div class="textboxDiv"><p class="p">*Address:</p>    <input type="text" name="address" required maxlength="50"></div>
                        <div class="textboxDiv" id ="city_textbox" style="display: none"><p class="p">*City Name:</p>    <input type="text" name="cityI" maxlength="50"></div>
                        <div class="textboxDiv">
                            <h4 id="city_p_tag" style="display: none;">Is it a suburb of one of these citys?</h4>
                            <p class="p">*City:</p>
                             <select id="cities" name="city" style="width: 85%; height: 40px;margin-left: 1%;border-radius: 5px;border: 1px solid silver;">
                                 <?php

                                    require("php/website/getSurburbs.php");
                                  ?>
                             </select>
                             <br>
                             <a id="idontsee" style="float: right;margin-right: 2%">I don't see my city</a>
                         </div>

                        <div class="textboxDiv"><p class="p">*Postal Code:</p>  <input type="text" name="postal" required maxlength="10"></div>
                 </div>
                   <input type="submit" class="btn btn-default" value="Submit">
            </form>

        </div>
    </div>
</div>
<footer class=footer id=footer>
    <div style=width:90%;margin:auto;background-color:#EEE>
        <div class=col-lg-4>
            <h5>SellBooks</h5><span><a href=about.html>Want to know more about us? Click this link</a></span></div>
        <div class=col-lg-4>
            <h5>SellBooks Support</h5><span>Has Something Gone wrong? Our FAQ page didnt help? Have a suggestion or see something we can improve? <a href=support.php>Email Us</a></span></div>
        <div class=col-lg-4>
            <h5>Frequently Asked Questions</h5><span>Got Any Questions? Check out our <a href=FAQ.html>FAQ Page</a></span></div>
        <hr>
        <div class=col-lg-14>
            <a href="https://play.google.com/store/apps/details?id=com.mirza.abdul.sellbooks" target=_blank><img alt="google play badge" src=google-play-badge.png></a>
            <a href="https://itunes.apple.com/us/app/sellbooks/id1066443130?ls=1&mt=8" target=_blank><img alt="google play badge" src=appstore_badge.svg></a>
        </div>
    </div>
</footer>
