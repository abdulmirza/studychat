<?php
  //$cssFiles = array('<link rel="stylesheet" type="text/css" href="sched/sched.css">');
  require "shared/header.php"
?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>Bootstrap Sidebar</h3>
            <strong>BS</strong>
        </div>

        <ul class="list-unstyled components">
            <li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-home"></i>
                    Home
                </a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="#">Home 1</a>
                    </li>
                    <li>
                        <a href="#">Home 2</a>
                    </li>
                    <li>
                        <a href="#">Home 3</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-briefcase"></i>
                    About
                </a>
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-copy"></i>
                    Pages
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li>
                        <a href="#">Page 1</a>
                    </li>
                    <li>
                        <a href="#">Page 2</a>
                    </li>
                    <li>
                        <a href="#">Page 3</a>
                    </li>
                </ul>
            </li>
        </ul>

    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span>Toggle Sidebar</span>
                </button>
            </div>
        </nav>
    </div>
</div>
<?php
  $jsFiles = array('<script src=scripts/canvasbrush.js></script>',
  '<script>
   var arr = [];
   /*$(document).ready(function() {
     $("#menu-toggle").click(function(e) {
         e.preventDefault(), $("#wrapper").toggleClass("toggled")
     }), $("#selectschool").on("change", function() {
         $("#ss").submit()
     });

     resizeCanvas();
   })

   function resizeCanvas() {

     let canvasContainer = $("#canvas_container");
     let canvas = document.getElementById("canvas-display");
     let controlsButton = $("#controls");

     let width = canvasContainer.innerWidth();
     let height = canvasContainer.innerHeight();
     canvas.width = width;
     canvas.height = height;

     console.log("called", canvas.width, height);
   }*/
  </script>',
  '<script src=scripts/lightbox2-master/src/js/lightbox.js></script>',
  '<script src=scripts/websocket-drawer.js></script>',
  '<script src=scripts/chat-object.js></script>',
  '<script src=scripts/display-image.js></script>',
'<script src=scripts/js.js></script>');
  require "shared/footer.php";
?>
