<?php
require 'connect.php';
$somethinghasgonewrong = "none";
$successfully = "none";
if ($_SERVER["REQUEST_METHOD"] == "POST"){
  $somethinghasgonewrong = "none";
  $successfully = "none";
    $stid = oci_parse($con, 'Update ARM.Accounts set unsubscribed = 1 WHERE Email = :C');

    oci_bind_by_name($stid, ':C', $p,256);
    $p = $_POST["email"];
     oci_execute($stid);

      if(oci_num_rows($stid) > 0){
          $successfully = "block";
      }else{
          $somethinghasgonewrong = "block";
      }
  oci_free_statement($stid);
}
oci_close($con);
 ?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
   <title>StudyChat - Unsubscribe</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
             <link rel="stylesheet" href="css/allpages-css.css">
 </head>
 <body>

 <div class="container">
   <h2>StudyChat (StudyChat) unsubscribe from emailing list</h2>
   <form method="post">
     <div class="form-group">
       <label for="email">Email:</label>
       <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
     </div>
     <button type="submit" class="btn btn-default">Submit</button>
   </form>
   <div class="alert alert-danger" style="display: <?php echo $somethinghasgonewrong;?>">
      <strong>Something has gone wrong!</strong> Please contact support <a href="support.php">Support</a> or make sure the email you have entered is correct
  </div>
  <div class="alert alert-success" style="display: <?php echo $successfully;?>">
      <strong>You have successfully unsubscribed!</strong> From now on you wont be able to receive emails about what your classmates need help with. If you would like to resubscribe Please contact support <a href="support.php">Support</a>
  </div>
 </div>

 </body>
 </html>
