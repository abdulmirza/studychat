<?php
  $pageToLoad = "St.php?";

  $cssFiles = array('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">',
    '<link href=css/cssv1001.css rel=stylesheet>',
    '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">');


  $pageToGoBackTo = "home.php";
  $displayBackButton = "none";


  $somethinghasgonewrong = $_GET["error"] ?? "none";

  $successfully = $_GET["success"] ?? "none";
  $codeNotProvided = $_GET["code"] ?? "none";
?>

<?php require "shared/header.php" ?>

 <div class="container">
<div class="row row_padding">
  <div class="col-12">
    <h2>Forgot your Password?</h2>
    <div class="alert alert-success" style="display: <?php echo $successfully;?>">
        <strong>Your password has been successfully reset!</strong> Please try to login in now. If you can not log in, please contact support <a href="support.php">Support</a>
    </div>
    <div class="alert alert-danger" style="display: <?php echo $codeNotProvided;?>">
        <strong>Did you provide us with a valid code?</strong> We couldn't find the code. Please contact <a href="contact.php">Support</a> or try reseting your password again from <a href="forgotPassword.php">here</a>
    </div>

    <form method="post" action="php/app/passwordReset.php">
      <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" required class="form-control" id="email" name="email" placeholder="Enter email">
      </div>
      <div class="form-group">
        <label for="code">Password reset code:</label>
        <input type="text" required class="form-control" id="code" name="code" placeholder="Enter Password Reset Code">
      </div>
      <div class="form-group">
        <label for="pass">Password:</label>
        <input type="password" required class="form-control" id="pass" name="pass" placeholder="Enter Password">
      </div>
 <div class="form-group">
        <label for="pass">Confirm Password:</label>
        <input type="password" required class="form-control" id="pass" name="pass2" placeholder="Confirm Password">
      </div>
      <button type="submit" class="btn btn-primary width_auto">Submit</button>
    </form>
    <div class="alert alert-danger" style="display: <?php echo $somethinghasgonewrong;?>">
       <strong>Something has gone wrong!</strong> Please contact support <a href="support.php">Support</a> or make sure the email and code you have entered is correct
   </div>

  </div>
  </div>
</div>
</div>
 <?php
   $jsFiles = null;
   require "shared/footer.php"
 ?>
