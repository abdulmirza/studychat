<?php
$name = "";
$email = "";
$message = "";
$subject  = "";
$human = 0;
$errEmail = "";
$errHuman = "";
$errMessage = "";
$errName = "";
$errSubject = "";
$result = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
	require('php/email/Mailin.php');
	if (!$_POST['name']) {
		$errName = 'Please enter your name';
	}

	if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
		$errEmail = 'Please enter a valid email address';
	}

	//Check if message has been entered
	if (!$_POST['message']) {
		$errMessage = 'Please enter your message';
	}

	//Check if message has been entered
	if (!$_POST['subject']) {
		$errSubject = 'Please enter a subject';
	}


	$name = $_POST['name'];
	$email = $_POST['email'];
	$message = $_POST['message'];
	$human = intval($_POST['human']);
	$subject = $_POST['subject'];
	$from = 'From: ' . $email;

	$to = 'admin@StudyChat.io';
	//$subject = 'Message from Contact Demo ';

	$body = "From: $name\n E-Mail: $email\n Message:\n $message";

	if ($human !== 5) {
		$errHuman = 'Your anti-spam is incorrect';
	}
	// If there are no errors, send the email
	if (!$errName && !$errEmail && !$errMessage && !$errHuman && !$errSubject) {
		$r = sendSupportEmail($email,$to, $name, $subject, $body);
		if (strcmp($r["code"],"success") == 0) {
			sendSupportEmail($to, $email, $name, $subject, $message);
			$result='<div class="alert alert-success">Thank You! We will be in touch</div>';
		} else {
			$result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later</div>';
		}
	}
}

function sendsupportEmail($from,$email, $name, $subject, $content) {
		$html = file_get_contents('php/email/templates/contact-support.html');

		$html = str_replace("#USER_MESSAGE#",$content, $html);

		$mailinThing = new Mailin("https://api.sendinblue.com/v2.0","1p38HfvnMsATkwWm");
		$data = array( "to" => array($email=>$name),
				"from" => array('admin@studychat.online', "StudyChat Support"),
				"subject" => $subject,
				"html" => $html//,
				//"attachment" => array("https://domain.com/path-to-file/filename1.pdf", "https://domain.com/path-to-file/filename2.jpg")
	);
		$thingy = $mailinThing->send_email($data);
	   // var_dump($thingy);
			return $thingy;
}
?>
<?php
$cssFiles = array('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">',
'<link href=css/cssv1001.css rel=stylesheet>',
'<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">');

require "shared/header.php";
?>

<div class="container" style="padding: 3%">
	<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
		<div class="container-fluid">


			<a class="navbar-brand js-scroll-trigger" style="font-weight: bold" href="index.php">StudyChat</a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				Menu
				<i class="fas fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class=" nav-link btn btn-outline-light js-scroll-trigger index_page_button" style="color: white;" href="home.php">Launch web app</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="row">
		<div class="col-12 col-md-6">
			<form class="form-horizontal" role="form" method="post" >
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="name" name="name" placeholder="First & Last Name" value="<?php echo htmlspecialchars($name); ?>">
					<?php echo "<p class='text-danger'>$errName</p>";?>
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="col-sm-2 control-label">Email</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="<?php echo htmlspecialchars($email); ?>">
					<?php echo "<p class='text-danger'>$errEmail</p>";?>
				</div>
			</div>
			<div class="form-group">
				<label for="subject" class="col-sm-2 control-label">Subject</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" value="<?php echo htmlspecialchars($subject); ?>">
					<?php echo "<p class='text-danger'>$errSubject</p>";?>
				</div>
			</div>
			<div class="form-group">
				<label for="message" class="col-sm-2 control-label">Message</label>
				<div class="col-sm-10">
					<textarea class="form-control" rows="4" name="message"><?php echo htmlspecialchars($message);?></textarea>
					<?php echo "<p class='text-danger'>$errMessage</p>";?>
				</div>
			</div>
			<div class="form-group">
				<label for="human" class="col-sm-2 control-label">2 + 3 = ?</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="human" name="human" placeholder="Your Answer">
					<?php echo "<p class='text-danger'>$errHuman</p>";?>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">
					<input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">
					<?php echo $result; ?>
				</div>
			</div>
		</form>
	</div>
	<div class="col-lg-4 d-md-none d-lg-block" style="border: 1px solid silver;padding: 20px;border-radius: 5px;margin-top: 1%;margin-left: 2%">
		<h4 class="form-signin-heading"><b>Support</b></h4>
		<hr>
		<p>If theres an issue with the website please email us directly at <strong>admin@StudyChat.io</strong> and please include fullpage screenshots</p>
		<p>Heres a quick checklist to help you resolve any errors that may occur</p>
		<p>Are you currently Signed In?</p>
		<!-- <p> Have you selected the school you go to? (If your school is not in the dropdown list <a href="addSchool.php">click here</a> to add it)</p> -->
		<!-- <p>If creating an Ad have you used any type of special characters? (/, ' , " , [] , {} , non-english letters, etc...)</p> -->
		<p>If you have gone through the checklist and want to read the FAQ again, please click the link below.</p>
		<a href="FAQ.html"><button class="btn btn-lg btn-info btn-block width_100">Check out FAQ</button></a>

	</div>
</div>
</div>
<footer id="footeR_bg">
	<div class="row center_text">
		<div class="col">
			<a class="footer_text" href="#">
				<?php echo $footer[0]->nodeValue ?>
			</a>
		</div>
		<div class="col">
			<a class="footer_text" href="support.php">
				<?php echo $footer[1]->nodeValue ?>
			</a>
		</div>
		<div class="col">
			<a class="footer_text" href="policy.php">
				<?php echo $footer[2]->nodeValue ?>
			</a>
		</div>
	</div>
	<hr style="background-color: #fff;margin: 2.5% 0;border: solid 1px #ffffff;"/>
	<div class="row">
		<div class="col">
			<p class="center_text" style="color: #b2bec3;opacity:0.7">
				Copyright 2018 <?php echo date("Y.m.d") ?>
			</p>
		</div>
	</div>
</footer>

</body>
</html>
