
<?php
require("php/language/languageSupport.php");
//$_currentSessionId = "";
$show = "none";
$time = 0;
$isLogged= false;

//echo $_SERVER['QUERY_STRING'];
if (isset($_COOKIE["ACCID"])) {
    $isLogged = true;

}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset=utf-8>
  <meta content="IE=edge"http-equiv=X-UA-Compatible>
  <meta content="width=device-width,shrink-to-fit=no,initial-scale=1"name=viewport>
  <meta content="Chat with your classmates"name=description>
  <meta content="Abdul-Rahman Mirza"name=author>
  <title>StudyChat - Chat with your classmates</title>

  <?php
    if(isset($cssFiles)) {
        foreach($cssFiles as $file) {
            echo $file;
        }
    }
?>
  <!--[if lt IE 9]><script src=https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js></script><script src=https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js></script><![endif]-->
</head>

<body>
