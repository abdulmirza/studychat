
<?php
$cssFiles = array('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">',
  '<link href=css/cssv1001.css rel=stylesheet>',
  '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">');


require "shared/header.php";
$somethinghasgonewrong = $_GET["error"] ?? "none";
$successfully = $_GET["success"] ?? "none";
?>

 <div class="container">
   <div class="row row_padding">
     <div class="col-12">
       <h2>Reset your password</h2>
       <div class="alert alert-danger" style="display: <?php echo $somethinghasgonewrong;?>">
          <strong>Something has gone wrong!</strong> Please contact support <a href="support.php">Support</a> or make sure the email you have entered is correct
      </div>
      <div class="alert alert-success" style="display: <?php echo $successfully;?>">
          <strong>You have successfully requested for a password reset!</strong> Please check your email. If you do no recieve an email, please contact support <a href="support.php">Support</a>
      </div>

       <form method="post" action="php/app/forgotPassword.php">
         <div class="form-group">
           <label for="email">Email:</label>
           <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
         </div>
         <button type="submit" class="btn btn-primary width_auto">Submit</button>
       </form>

     </div>
     </div>
   </div>
 </div>

<?php
$jsFiles = null;
require "shared/footer.php"
?>
