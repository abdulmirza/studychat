<?php
$cssFiles = array('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">',
'<link href=css/cssv1001.css rel=stylesheet>',
'<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">');

require "shared/header.php" ?>

        <div class="alert alert-info">
                <strong>Didn't find your answer?</strong> <a href="contact.php" class="btn">Contact us!</a>
        </div>


        <div class="container container_max_width">
            <div class="row">
                <div class="col-lg-12">
                  <p class="header_text text_align_left">
                      <?php echo $document[0]->nodeValue ?>
                  </p>
                </div>
            </div>
            <div class="row row_spacing_2_5">
                <div class="col-lg-12">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><?php echo  $document[1]->nodeValue ?></a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                   <?php echo  $document[2]->nodeValue ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><?php echo  $document[3]->nodeValue ?></a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                        <?php echo  $document[4]->nodeValue ?>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><?php echo  $document[5]->nodeValue ?></a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                   <?php echo  $document[6]->nodeValue ?>
                                </div>
                            </div>
                        </div>
                          <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse9"><?php echo  $document[7]->nodeValue ?></a>
                                </h4>
                            </div>
                            <div id="collapse9" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <?php echo  $document[8]->nodeValue ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse30"> <?php echo  $document[9]->nodeValue ?></a>
                                </h4>
                            </div>
                            <div id="collapse30" class="panel-collapse collapse">
                                <div class="panel-body">
                                     <?php echo  $document[10]->nodeValue ?>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <?php $jsFiles = null;
        require "shared/footer.php" ?>
